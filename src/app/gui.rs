mod state_machine;

use std::collections::BTreeSet;
use std::ops::Deref;
use std::path::Path;
use egui::{ScrollArea, Ui};
use itertools::Itertools;
use log::info;
use map_data::{FilesEnum, OtherFile, Script, ScriptValidationResult, IssueCounter, Validate, BUILDINGS_TABLE_ID, MapTable, MONUMENTS_TABLE_ID};
use map_data::entity::Param;
use simulator::Simulator;

#[derive(Default)]
pub struct GUI {
    map_tables: BTreeSet<u32>,
    files: BTreeSet<usize>,
    explain_issues: bool,
    show_debug_table_list: bool,
    script_theme_editor: bool,
    //show_orbs_and_chests: bool,
    //orbs_and_chests: String,
    simulation: Option<Simulator>,
    issues: IssueCounter,
}

impl GUI {
    pub fn new_map(&mut self, map: &super::map::Map) {
        self.map_tables.clear();
        self.files.clear();
        self.script_theme_editor = false;
        /*self.orbs_and_chests.clear();
        if let Some((table, _, _, _)) = map.map.get(&BUILDINGS_TABLE_ID) {
            let t : &MapTable = &table.borrow();
            match t {
                MapTable::Buildings(b, start) => {
                    let mut id = *start;
                    for b in &b.0 {
                        if b.b.b.b.params.iter().any(|p| matches!(p, Param::Chest(_))) {
                            self.orbs_and_chests.push_str(&format!(
                                "chest: {id} at: {:?}\n", b.b.b.collision_pos));
                        }
                        id += 1;
                    }
                }
                _ => unreachable!(),
            }
        }
        if let Some((table, _, _, _)) = map.map.get(&MONUMENTS_TABLE_ID) {
            let t : &MapTable = &table.borrow();
            match t {
                MapTable::Monuments(m, start) => {
                    let mut id = *start;
                    for m in &m.0 {
                        self.orbs_and_chests.push_str(&format!(
                            "monument: {id} at: {:?}\n", m.b.b.collision_pos));
                        id += 1;
                    }
                }
                _ => unreachable!(),
            }
        }*/
        info!("validating map");
        self.issues = map.validate(map);
    }
    pub fn show_mut(&mut self, map: &mut super::map::Map, ctx: &egui::Context) {
        egui::SidePanel::left(egui::Id::new(42)).show(ctx, |ui| { ui.label(&map.map_name);
        //egui::Window::new(&map.map_name).show(ctx, |ui| {
            ScrollArea::vertical().show(ui, |ui| {
                if self.issues.bugs != 0 {
                    ui.colored_label(ui.style().visuals.error_fg_color, format!("bugs: {}", self.issues.bugs));
                }
                if self.issues.warnings != 0 {
                    ui.colored_label(ui.style().visuals.warn_fg_color, format!("warnings: {}", self.issues.warnings));
                }
                if self.issues.bugs != 0 || self.issues.warnings != 0 {
                    if ui.button("FIX").clicked() {
                        map.fix();
                        self.new_map(map);
                    }
                    ui.checkbox(&mut self.explain_issues, "explain issues");
                }
                // TODO how to show `map.files`?
                // there is no longer the option to do a collapsing per "category"
                ui.collapsing("map tables:", |ui| {
                    for (id, (table, _, _, _)) in map.map.iter() {
                        let mut b = self.map_tables.contains(id);
                        if (if let Some(name) = table.borrow().name() {
                            //ui.checkbox(&mut b, name)
                            ui.checkbox(&mut b, &format!("{name} ({})", table.borrow().base_id()))
                        } else {
                            ui.checkbox(&mut b, &format!("{id}"))
                        }).changed() {
                            if b {
                                self.map_tables.insert(*id);
                            } else {
                                self.map_tables.remove(id);
                            }
                        }
                    }
                });
                for (key, group) in &map.files.iter().enumerate()
                    .group_by(|(_, f)| f.borrow().path().parent().map(Path::to_path_buf)) {
                    let key = match key {
                        None => "top level files".to_owned(),
                        Some(key) => format!("{}", key.to_str().unwrap()),
                    };
                    ui.collapsing(format!("{key}"), |ui|{
                        for (i, f) in group {
                            let mut b = self.files.contains(&i);
                            if ui.checkbox(&mut b, format!("{}", f.borrow().path().file_name().unwrap().to_str().unwrap())).changed() {
                                if b {
                                    self.files.insert(i);
                                } else {
                                    self.files.remove(&i);
                                }
                            }
                        }
                    });
                }
                ui.checkbox(&mut self.show_debug_table_list, "dbg table list");
                //ui.checkbox(&mut self.show_orbs_and_chests, "show orb and chest list");
                if self.simulation.is_none() {
                    if ui.button("simulate").clicked() {
                        self.simulation = Some(simulator::init(&map))
                    }
                }
            });
        });

        if self.explain_issues && (self.issues.bugs != 0 || self.issues.warnings != 0) {
            egui::Window::new("Issues").show(ctx, |ui| {
                ScrollArea::vertical().show(ui, |ui|{
                    for explain in &self.issues.error_text {
                        ui.colored_label(ui.style().visuals.error_fg_color, explain);
                    }
                    for explain in &self.issues.warning_text {
                        ui.colored_label(ui.style().visuals.warn_fg_color, explain);
                    }
                });
            });
        }

        self.maps(map, ctx);
        self.files.retain(|index|{
            let mut file = map.files[*index].borrow_mut();
            let mut retain = true;
            if show_file_mut(&mut file, &mut retain, map, ctx) {
                match &mut *file {
                    FilesEnum::GlobalVariables(s) |
                    FilesEnum::ScriptList(s) |
                    FilesEnum::ScriptGroups(s) |
                    FilesEnum::SpawnGroups(s) |
                    FilesEnum::Script(s) => {
                        let gv = map.files.iter()
                            .find_map(|f| {
                                if let FilesEnum::GlobalVariables(gv) = f.try_borrow().ok()?.deref() {
                                    Some((gv.name.clone(), gv.content.clone()))
                                } else {
                                    None
                                }
                            });
                        let gv = gv.as_ref()
                            .map(|(name, content)| (name.as_str(), content.as_str()));
                        s.revalidate(gv);
                    }
                    FilesEnum::AtlasCol(_) |
                    FilesEnum::AtlasNor(_) |
                    FilesEnum::Descriptions(_) |
                    FilesEnum::OtherFiles(_) => {},
                }
            }
            retain
        });
        self.script_theme_editor(ctx);
        if self.show_debug_table_list {
            egui::Window::new("table IDs debug").show(ctx, |ui|{
                ScrollArea::vertical().show(ui, |ui|{
                    for (table_id, (table,_ , s1,s2)) in map.map.iter() {
                        ui.label(format!("{table_id} {s1} {s2} {:?}", table.borrow().name()));
                    }
                });
            });
        }
        /*if self.show_orbs_and_chests {
            egui::Window::new("Orbs and chest").show(ctx, |ui|{
                /*if ui.button("copy").clicked() {
                    clipp
                }*/
                ScrollArea::vertical().show(ui, |ui|{
                    //ui.label(&self.orbs_and_chests);
                    let mut text = self.orbs_and_chests.clone();
                    ui.text_edit_multiline(&mut text);
                });
            });
        }*/
        if let Some(simulation) = &mut &mut self.simulation {
            simulation.gui(ctx);
        }
    }
}

fn show_file_mut(file: &mut FilesEnum, open: &mut bool, map: &super::map::Map, ctx: &egui::Context) -> bool {
    if *open == false {
        return false;
    }
    match file {
        FilesEnum::GlobalVariables(s) |
        FilesEnum::ScriptList(s) |
        FilesEnum::ScriptGroups(s) |
        FilesEnum::SpawnGroups(s) |
        FilesEnum::Script(s) => show_script_mut(Some(s), open, ctx),
        FilesEnum::AtlasCol(a) => show_other_file_mut(&mut a.col, open, map, ctx),
        FilesEnum::AtlasNor(a) => show_other_file_mut(&mut a.nor, open, map, ctx),
        FilesEnum::Descriptions(f) => show_other_file_mut(f, open, map, ctx),
        FilesEnum::OtherFiles(f) => show_other_file_mut(f, open, map, ctx),
    }
}

fn show_script_mut(script: Option<&mut Script>, open: &mut bool, ctx: &egui::Context) -> bool {
    if *open == false || script.is_none() {
        return false;
    }
    let script = script.unwrap();
    egui::Window::new(&script.name).open(open).show(ctx, |ui| {
        let theme = egui_extras::syntax_highlighting::CodeTheme::from_memory(ui.ctx(), ui.style());
        const LANGUAGE: &str = "lua";
        let mut layouter = |ui: &Ui, string: &str, wrap_width: f32| {
            let mut layout_job =
                egui_extras::syntax_highlighting::highlight(ui.ctx(), ui.style(), &theme, string, LANGUAGE);
            layout_job.wrap.max_width = wrap_width;
            ui.fonts(|f| f.layout_job(layout_job))
        };

        ScrollArea::vertical().show(ui, |ui| {
            fn show_validation (validation_result: &script_validator::ValidationResult, ui: &mut Ui) {
                match validation_result {
                    Ok((_, Ok(states))) => {
                        ui.collapsing("dbg state machine", |ui|{
                            state_machine::show_state_machine(states, ui);
                        });
                    }
                    Ok((_, Err(e))) |
                    Err(e) => {
                        ui.colored_label(ui.style().visuals.error_fg_color, format!("{e:?}"));
                    }
                }
            }
            match &script.validation_result {
                ScriptValidationResult::GlobalVariables => {}
                ScriptValidationResult::ScriptList(r) => {
                    match r {
                        Ok(_) => {}
                        Err(e) => {
                            ui.colored_label(ui.style().visuals.error_fg_color, format!("{e}"));
                        }
                    }
                }
                ScriptValidationResult::ScriptGroups(r) => {
                    match r {
                        Ok(groups) => {
                            ui.collapsing("goups", |ui|{
                                ui.label(format!("{:#?}", groups));
                            });
                        }
                        Err(e) => {
                            ui.colored_label(ui.style().visuals.error_fg_color, format!("{e}"));
                        }
                    }
                }
                ScriptValidationResult::NotPartOfHierarchy => {
                    ui.colored_label(ui.style().visuals.error_fg_color, "Script is not part of an hierarchy");
                }
                ScriptValidationResult::SpawnGroupsValidated(_r) => {
                    /*match _r {
                        Ok((_, Ok(emitters))) => {
                            for emitter in emitters {
                                ui.label(emitter);
                            }
                        }
                        _ => {
                            ui.label(&format!("{r:#?}"));
                        }
                    }*/
                    // TODO
                }
                ScriptValidationResult::Validated(r, _) => show_validation(r, ui),
                ScriptValidationResult::IncludedMultipleTimes(results) => {
                    ui.colored_label(ui.style().visuals.error_fg_color, "Script included multiple times!");
                    for result in results {
                        show_validation(result, ui);
                    }
                }
            }
            let r = ui.add(
                egui::TextEdit::multiline(&mut script.content)
                    .font(egui::TextStyle::Monospace) // for cursor height
                    .code_editor()
                    .desired_rows(10)
                    .lock_focus(true)
                    .desired_width(f32::INFINITY)
                    .layouter(&mut layouter),
            );
            r.changed()
        }).inner
    }).and_then(|r| r.inner).unwrap_or_default()
}


fn show_other_file_mut(file: &mut OtherFile, open: &mut bool, _map: &super::map::Map, ctx: &egui::Context) -> bool {
    let ext = path_extension(&file.relative_path);
    egui::Window::new(&file.relative_path).open(open).show(ctx, |ui|{
        match extension_type(ext) {
            FileType::Text => {
                match std::str::from_utf8(&file.content) {
                    Ok(text) => {
                        let theme = egui_extras::syntax_highlighting::CodeTheme::from_memory(ui.ctx(), ui.style());
                        let mut layouter = |ui: &Ui, string: &str, wrap_width: f32| {
                            let mut layout_job =
                                egui_extras::syntax_highlighting::highlight(ui.ctx(), ui.style(), &theme, string, &file.relative_path);
                            layout_job.wrap.max_width = wrap_width;
                            ui.fonts(|f| f.layout_job(layout_job))
                        };
                        let mut text = text.to_string();
                        ScrollArea::vertical().show(ui, |ui|{
                            let r = ui.add(
                                egui::TextEdit::multiline(&mut text)
                                    .font(egui::TextStyle::Monospace) // for cursor height
                                    .code_editor()
                                    .desired_rows(10)
                                    .lock_focus(true)
                                    .desired_width(f32::INFINITY)
                                    .layouter(&mut layouter),
                            );
                            if r.changed() {
                                file.content = text.into();
                            }
                            r.changed()
                        }).inner
                    }
                    Err(err) => {
                        ui.colored_label(ui.style().visuals.error_fg_color, format!("{}", err));
                        false
                    }
                }
            }
            FileType::Image => {
                ui.image(egui::ImageSource::Bytes {uri: file.relative_path.clone().into(), bytes: file.content.clone().into()});
                false
            }
            FileType::Unknown => {
                false
            }
        }
    }).map(|r| r.inner.unwrap_or_default()).unwrap_or_default()

}

impl GUI {
    fn maps(&mut self, map: &mut super::map::Map, ctx: &egui::Context) {
        self.map_tables.retain(|map_table|{
            let mut retain = true;
            map.map.get(map_table).unwrap().0.borrow_mut().show_mut(*map_table, &mut retain, map, ctx);
            retain
        });
    }

    fn script_theme_editor(&mut self, ctx: &egui::Context) {
        if self.script_theme_editor {
            egui::Window::new("script theme editor").show(ctx, |ui| {
                let mut theme = egui_extras::syntax_highlighting::CodeTheme::from_memory(ui.ctx(), ui.style());
                ui.collapsing("Theme", |ui| {
                    ui.group(|ui| {
                        theme.ui(ui);
                        theme.clone().store_in_memory(ui.ctx());
                    });
                });
            });
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq)]
enum FileType { Text, Image, Unknown }

fn path_extension(path: &str) -> Option<&str> {
    /*std::path::Path::try_from(path).ok()?
        .extension()?
        .to_str()*/
    let dot = path.rfind(".")? + 1;
    Some(&path[dot..])
}
fn extension_type(ext: Option<&str>) -> FileType {
    fn extension_type(ext: Option<&str>) -> Option<FileType> {
        let ext = ext?;
        if ext.eq_ignore_ascii_case("xml") ||
            ext.eq_ignore_ascii_case("md") ||
            ext.eq_ignore_ascii_case("json") {
            return Some(FileType::Text)
        }
        if ext.eq_ignore_ascii_case("dds") ||
            ext.eq_ignore_ascii_case("png") ||
            ext.eq_ignore_ascii_case("jpg") ||
            ext.eq_ignore_ascii_case("tga") ||
            ext.eq_ignore_ascii_case("bmp") {
            return Some(FileType::Image)
        }
        Some(FileType::Unknown)
    }
    extension_type(ext).unwrap_or(FileType::Unknown)
}
