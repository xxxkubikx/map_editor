use egui::{Context, Ui};
use sr_libs::cff::GameDataCffFile;
use crate::entities::EntityType;
use crate::Simulator;
use crate::world::chunks::{EntityWithBB, WorldChunks};

impl Simulator {
    pub fn gui(&mut self, ctx: &Context) {
        let mut replace = None;
        match self {
            Simulator::FakeEmtyToMakeCompilerHappy => unreachable!(),
            Simulator::MapEntitiesLoaded { data, path_to_cff, error } => {
                egui::Window::new("simulation").show(ctx, |ui|{
                    if let Some(error) = error {
                        ui.colored_label(ui.style().visuals.error_fg_color, error);
                    }
                    ui.horizontal(|ui|{
                        ui.text_edit_singleline(path_to_cff);
                        if ui.button("Load game files").clicked() {
                            *error = None;
                            let try_cff = std::fs::read(&path_to_cff)
                                .map_err(|err| format!("{err:?}"))
                                .and_then(|bytes| GameDataCffFile::from_bytes(&bytes)
                                    .map_err(|err| format!("{err:?}")));
                            match try_cff {
                                Ok(cff) => {
                                    replace = Some(cff);
                                }
                                Err(err) => {
                                    *error = Some(err);
                                    return;
                                }
                            };
                        }
                    });
                    ui.collapsing("all", |ui|{
                        chunks(&Cfg::default(), &data.world_chunks, (1, 0, 0), ui);
                    });
                });
            }
            Simulator::MapInitialized { data, cff: _cff } => {
                egui::Window::new("simulation").show(ctx, |ui|{
                    ui.collapsing("all", |ui|{
                        chunks(&Cfg::default(), &data.world_chunks, (1, 0, 0), ui);
                    });
                });
            }
        }
        if let Some(cff) = replace {
            replace_with::replace_with_or_abort(
                self,
                |self_| match self_ {
                    Simulator::MapEntitiesLoaded { data, .. } => {
                        let mut data = data;
                        data.id_admin.init_after_map_load(&cff);
                        Simulator::MapInitialized { data, cff }
                    },
                    _ => unreachable!()
                });
        }
    }
}


#[derive(Copy, Clone)]
struct Cfg {
    entity_type_filter: u32,
    show_empty_chunks: bool,
    show_empty_lists: bool,
    as_tree: bool,
}

impl Cfg {
    pub const fn default() -> Self {
        Self{
            entity_type_filter: 0x7ffffffe,
            show_empty_chunks: true,
            show_empty_lists: false,
            as_tree: true,
        }
    }
}

fn chunks(cfg: &Cfg, grid: &WorldChunks, part: (u32, u32, u32), ui: &mut Ui) {
    if part.0 > grid._40.splits_a8 {
        return;
    }
    draw_chunk(cfg, grid, part, ui, chunks_inner);

    if !cfg.as_tree {
        chunks_inner(cfg, grid, part, ui);
    }
    fn chunks_inner(cfg: &Cfg, grid: &WorldChunks, part: (u32, u32, u32), ui: &mut Ui) {

        if part.0 + 1 > grid._40.splits_a8 {
            return;
        }

        chunks(cfg, grid, (part.0 + 1, part.1 * 2, part.2 * 2), ui);
        chunks(cfg, grid, (part.0 + 1, part.1 * 2 + 1, part.2 * 2), ui);
        chunks(cfg, grid, (part.0 + 1, part.1 * 2, part.2 * 2 + 1), ui);
        chunks(cfg, grid, (part.0 + 1, part.1 * 2 + 1, part.2 * 2 + 1), ui);
    }
}


fn draw_chunk(cfg: &Cfg, grid: &WorldChunks, part: (u32, u32, u32), ui: &mut Ui, inner:fn(&Cfg, &WorldChunks, (u32, u32, u32), &mut Ui)) {
    let i = part.1 + grid._10[part.0 as usize] + (part.2 << (part.0 - 1));
    let chunk = &grid.chunks.as_slice()[i as usize];
    if chunk.is_none() {
        return;
    }
    let chunk = chunk.as_ref().unwrap();
    if chunk.is_occupied_0x208 == 0 {
        if cfg.show_empty_chunks {
            ui.label(format!("{part:?} occupied: {} bb: {:?}", chunk.is_occupied_0x208, chunk.bounding_box_1f0));
        }
        return;
    }
    ui.collapsing(format!("{part:?} bb: {:?}", chunk.bounding_box_1f0), |ui|{
        ui.label(format!("occupied: {}", chunk.is_occupied_0x208));
        for (i, lists) in chunk.lists_by_entity_types.iter().enumerate() {
            if cfg.entity_type_filter & (1 << i) == 0 {
                continue;
            }
            let et = EntityType::try_from(i as u32);
            if let Ok(et) = et {
                if !lists.is_empty() {
                    entities(&format!("{et:?}s"), lists.iter(), ui);
                } else if cfg.show_empty_lists {
                    ui.label(format!("No {et:?}s"));
                }
            } else if !lists.is_empty() {
                entities(&format!("Entities of unknown type {i}"), lists.iter(), ui);
            }
        }
        if cfg.as_tree {
            inner(cfg, grid, part, ui)
        }
    });
}

fn entities<'a>(name: &str, entities: impl Iterator<Item = &'a EntityWithBB>, ui: &mut Ui) {
    ui.collapsing(name, |ui|{
        for entity in entities {
            ui.collapsing(format!("{:?} {:?} at: {:?}", entity.entity_type, entity.entity, entity.bounding_box), |_ui|{
                //super::ids::entity_info(&entity.entity.b, ui);
            });
        }
    });
}
