use std::ops::Mul;
use map_data::entity::Position;
use crate::entities::{EntityPositioned, EntityType};
use crate::id_admin::EntityId;

impl WorldChunks {
    pub fn new(x: u16, y: u16) -> Self {
        let bb = BoundingBox {
            min: Position {
                x: -32.0,
                z: 0.0,
                y: -32.0,
            },
            max: Position {
                x: x as f32 * 1.4 + 32.,
                z: 655.35,
                y: y as f32 * 1.4 + 32.,
            },
        };
        Self::init(6, bb)
    }
    fn init(splits: u32, bb: BoundingBox) -> Self {
        let v2 = (1 << (splits as u8) - 1) as f32;
        let scaling_x = (bb.max.x - bb.min.x) / v2;
        let scaling_y = (bb.max.y - bb.min.y) / v2;
        let mut v6 = 1;
        let mut i = 0;
        let mut _10 = [0;7];
        loop {
            if v6 >= splits + 1 {
                break
            }
            if v6 == 1 {
                _10[1] = 0;
            } else {
                _10[v6 as usize] = _10[v6 as usize - 1] + (1 << (2 * v6 - 4));
            }

            v6 += 1;
            i += 1 << (2 * v6 - 2);
        }
        Self {
            chunks: vec![None;i],
            _08: 0,
            _10,
            _2c: 0,
            _30: 0,
            _34: 0,
            _38: 0,
            _3c: 0,
            _40: Struct168d7e080 {
                _struct_40: 0,
                gap_44: 0,
                gap_48: [0;18],
                bounding_box_90: bb,
                splits_a8: splits,
                scaling_x,
                scaling_y,
                inv_scaling_x: 1. / scaling_x,
                inv_scaling_y: 1. / scaling_y,
                bounding_box_bc: bb,
            },
        }
    }
    pub fn add_entity(&mut self, e: &EntityPositioned) {
        let mwp = MatrixWithPosition {
            m: if e.rotation.order == 1 {
                let x = Matrix3x3::rotation_matrix(Axis::X, e.rotation.x);
                let y = Matrix3x3::rotation_matrix(Axis::Y, e.rotation.y);
                let z = Matrix3x3::rotation_matrix(Axis::Z, e.rotation.z);
                x*y*z
            } else {
                todo!() // defined on entities
            },
            pos: e.collision_pos,
        };
        let bb = self.add_bb(mwp, BoundingBox::default());
        if bb.min.x.abs() >= f32::EPSILON
            || bb.min.z.abs() >= f32::EPSILON
            || bb.min.y.abs() >= f32::EPSILON
            || bb.max.x.abs() >= f32::EPSILON
            || bb.max.z.abs() >= f32::EPSILON
            || bb.max.y.abs() >= f32::EPSILON {
            self.add_entity_to_bb(e.b.id, e.b.entity_type, bb);
        }
    }
    fn add_entity_to_bb(&mut self, e: EntityId, et: EntityType, bb: BoundingBox) {
        let split_dir = self.split_dir(bb);
        let i = split_dir.z + self._10[split_dir.x as usize] + (split_dir.y << (split_dir.x as u8 - 1));
        match &mut self.chunks[i as usize] {
            None => {
                let v7 = self._40.splits_a8 - split_dir.x;
                let mut chunk = WordChunk{
                    lists_by_entity_types: std::array::from_fn(|_|Vec::new()),
                    bounding_box_1f0: BoundingBox{
                        min: Position {
                            x: (split_dir.z << v7) as f32 * self._40.scaling_x + self._40.bounding_box_90.min.x,
                            z: 0.0,
                            y: (split_dir.y << v7) as f32 * self._40.scaling_y + self._40.bounding_box_90.min.y,
                        },
                        max: Position {
                            x: ((split_dir.z + 1) << v7) as f32 * self._40.scaling_x + self._40.bounding_box_90.min.x,
                            z: bb.max.y, // to eliminate super small values: ((bb.max.y - 0.0).abs() + (bb.max.y + 0.0)) * 0.5,
                            y: ((split_dir.y + 1) << v7) as f32 * self._40.scaling_y + self._40.bounding_box_90.min.y,
                        },
                    },
                    is_occupied_0x208: 0,
                };
                chunk.increase_occupied(e, bb, et);
                self.chunks[i as usize] = Some(chunk);
            }
            Some(chunk) => {
                chunk.increase_occupied(e, bb, et);
            }
        }
        // todo!("sub::0x15AE400") // maybe just preallocate chuks?
    }
    fn split_dir(&self, bb: BoundingBox) -> Vec3u {
        let v4 = ((bb.min.x - self._40.bounding_box_90.min.x) * self._40.inv_scaling_x) as u32;
        let v5 = ((bb.min.y - self._40.bounding_box_90.min.y) * self._40.inv_scaling_y) as u32;
        let v6 = ((bb.max.x - self._40.bounding_box_90.min.x) * self._40.inv_scaling_x) as u32;
        let v7 = ((bb.max.y - self._40.bounding_box_90.min.y) * self._40.inv_scaling_y) as u32;
        if (v4 | v5 | v6 | v7) >> (self._40.splits_a8 - 1) != 0 {
            Vec3u{
                x: 1,
                z: 0,
                y: 0,
            }
        } else {
            let i = ((2 * (v4 ^ v5 | v6 ^ v7)) | 1).ilog2();
            Vec3u{
                x: self._40.splits_a8 - i,
                z: v4 >> i,
                y: v5 >> i,
            }
        }
    }
    fn add_bb(&mut self, m: MatrixWithPosition, bb: BoundingBox) -> BoundingBox {
        let bb = bb.sub_16f1660(m);
        let min = bb.min;
        let max = bb.max;
        if self._40.bounding_box_bc.max.x < min.x
            || self._40.bounding_box_bc.max.z < min.z
            || self._40.bounding_box_bc.max.y < min.y
            || self._40.bounding_box_bc.min.x > max.x
            || self._40.bounding_box_bc.min.z > max.z
            || self._40.bounding_box_bc.min.y > max.y {
            BoundingBox {
                min: Position{
                    x: 0.0,
                    z: 0.0,
                    y: 0.0,
                },
                max: Position{
                    x: 0.0,
                    z: 0.0,
                    y: 0.0,
                },
            }
        } else {
            BoundingBox {
                min: Position{
                    x: bb.min.x.max(min.x),
                    z: bb.min.z.max(min.z),
                    y: bb.min.y.max(min.y),
                },
                max: Position{
                    x: bb.max.x.min(max.x),
                    z: bb.max.z.min(max.z),
                    y: bb.max.y.min(max.y),
                },
            }
        }
    }
}

pub struct WorldChunks
{
    pub chunks: Vec<Option<WordChunk>>,
    _08: u32,
    pub _10: [u32;7],
    _2c: u32,
    _30: u32,
    _34: u32,
    _38: u32,
    _3c: u32,
    pub _40: Struct168d7e080,
}

pub struct Struct168d7e080
{
    _struct_40: usize,
    gap_44: u32,
    gap_48: [u32;18],
    pub bounding_box_90: BoundingBox,
    pub splits_a8: u32,
    scaling_x: f32,
    scaling_y: f32,
    inv_scaling_x: f32,
    inv_scaling_y: f32,
    pub bounding_box_bc: BoundingBox,
}

#[derive(Clone)]
pub struct WordChunk
{
    pub lists_by_entity_types: [Vec<EntityWithBB>; 31],
    pub bounding_box_1f0: BoundingBox,
    pub is_occupied_0x208: u32,
}

impl WordChunk {
    pub fn increase_occupied(&mut self, entity: EntityId, bounding_box: BoundingBox, entity_type: EntityType) {
        if bounding_box.max.y > self.bounding_box_1f0.max.y {
            self.bounding_box_1f0.max.y = bounding_box.max.y;
        }
        self.lists_by_entity_types[entity_type as u32 as usize].push(EntityWithBB{
            entity,
            entity_type,
            bounding_box,
        });
        self.is_occupied_0x208 += 1;
    }
}

#[derive(Copy, Clone)]
pub struct EntityWithBB
{
    pub entity: EntityId,
    pub entity_type: EntityType,
    pub bounding_box: BoundingBox,
}

#[derive(Copy, Clone)]
pub struct BoundingBox4X {
    pub a : BoundingBox,
    pub b : BoundingBox,
    pub c : BoundingBox,
    pub d : BoundingBox,
}

#[derive(Debug, Copy, Clone)]
pub struct BoundingBox {
    pub min : Position,
    pub max : Position,
}
impl Default for BoundingBox {
    fn default() -> Self {
        Self{
            min: Position {
                x: -0.5,
                z: -0.5,
                y: -0.5,
            },
            max: Position {
                x: 0.5,
                z: 0.5,
                y: 0.5,
            },
        }
    }
}
impl BoundingBox {
    pub fn contains(&self, pos: &Position) -> bool {
        self.min.x <= pos.x && pos.x <= self.max.x &&
            self.min.y <= pos.y && pos.y <= self.max.y &&
            self.min.z <= pos.z && pos.z <= self.max.z
    }
    pub fn sub_16f1660(&self, m: MatrixWithPosition) -> Self {
        let mut v15 = BoundingBox4X {
            a: Default::default(),
            b: Default::default(),
            c: Default::default(),
            d: Default::default(),
        };
        let mut i = 8;
        loop {
            i -= 1;
            let v = match i {
                7 => &mut v15.a.min,
                6 => &mut v15.a.max,
                5 => &mut v15.b.min,
                4 => &mut v15.b.max,
                3 => &mut v15.c.min,
                2 => &mut v15.c.max,
                1 => &mut v15.d.min,
                0 => &mut v15.d.max,
                _ => unreachable!(),
            };
            let x = m.m.m10 * v.z + m.m.m00 * v.z + m.m.m20 * v.y;
            let z = m.m.m11 * v.z + m.m.m01 * v.z + m.m.m21 * v.y;
            let y = m.m.m12 * v.z + m.m.m02 * v.z + m.m.m22 * v.y;
            v.x = m.pos.x + x;
            v.z = m.pos.z + z;
            v.y = m.pos.y + y;

            if i <= 0 {
                break;
            }
        }
        let mut ret = BoundingBox {
            min: v15.a.min,
            max: v15.a.min,
        };
        let mut i = 7;
        loop {
            let v = match i {
                7 => &v15.a.max,
                6 => &v15.b.min,
                5 => &v15.b.max,
                4 => &v15.c.min,
                3 => &v15.c.max,
                2 => &v15.d.min,
                1 => &v15.d.max,
                _ => unreachable!(),
            };
            if ret.min.x > v.x {
                ret.min.x = v.x;
            }
            if ret.max.x < v.x {
                ret.max.x = v.x;
            }
            if ret.min.z > v.z {
                ret.min.z = v.z;
            }
            if ret.max.z < v.z {
                ret.max.z = v.z;
            }
            if ret.min.y > v.y {
                ret.min.y = v.y;
            }
            if ret.max.y < v.y {
                ret.max.y = v.y;
            }

            i -= 1;
            if i <= 0 {
                break;
            }
        }

        ret
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Matrix3x3
{
    pub m00: f32,
    pub m01: f32,
    pub m02: f32,
    pub m10: f32,
    pub m11: f32,
    pub m12: f32,
    pub m20: f32,
    pub m21: f32,
    pub m22: f32,
}
pub enum Axis
{
    X = 0x0,
    Z = 0x1,
    Y = 0x2,
}
impl Mul for Matrix3x3 {
    type Output = Matrix3x3;

    fn mul(self, o: Self) -> Self::Output {
        Self::Output{
            m00: self.m00 * o.m00 + self.m01 * o.m10 + self.m02 * o.m20,
            m01: self.m00 * o.m01 + self.m01 * o.m11 + self.m02 * o.m21,
            m02: self.m00 * o.m02 + self.m02 * o.m12 + self.m02 * o.m22,
            m10: self.m10 * o.m00 + self.m11 * o.m10 + self.m12 * o.m20,
            m11: self.m10 * o.m01 + self.m11 * o.m11 + self.m12 * o.m22,
            m12: self.m10 * o.m02 + self.m11 * o.m12 + self.m12 * o.m22,
            m20: self.m20 * o.m00 + self.m21 * o.m10 + self.m22 * o.m20,
            m21: self.m20 * o.m01 + self.m21 * o.m11 + self.m22 * o.m21,
            m22: self.m20 * o.m02 + self.m21 * o.m12 + self.m22 * o.m22,
        }
    }
}
impl Matrix3x3 {
    pub fn rotation_matrix(axis: Axis, phi: f32) -> Self {
        let cos_phi = phi.cos();
        let sin_phi = phi.sin();
        match axis {
            Axis::X =>
                Self {
                    m00: 1.0,
                    m01: 0.0,
                    m02: 0.0,
                    m10: 0.0,
                    m11: cos_phi,
                    m12: -sin_phi,
                    m20: 0.0,
                    m21: sin_phi,
                    m22: cos_phi,
                },
            Axis::Y =>
                Self{
                    m00: cos_phi,
                    m01: 0.0,
                    m02: sin_phi,
                    m10: 0.0,
                    m11: 1.0,
                    m12: 0.0,
                    m20: -sin_phi,
                    m21: 0.0,
                    m22: cos_phi,
                },
            Axis::Z =>
                Self{
                    m00: cos_phi,
                    m01: -sin_phi,
                    m02: 0.0,
                    m10: sin_phi,
                    m11: cos_phi,
                    m12: 0.0,
                    m20: 0.0,
                    m21: 0.0,
                    m22: 1.0,
                },
        }
    }
}
#[derive(Debug, Copy, Clone)]
pub struct MatrixWithPosition
{
    pub m: Matrix3x3,
    pub pos: Position,
}

#[derive(Debug, Copy, Clone)]
pub struct Vec3u {
    pub x: u32,
    pub z: u32,
    pub y: u32,
}
