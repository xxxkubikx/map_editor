use std::ops::Deref;
use sr_libs::cff::GameDataCffFile;
use map_data::{MapTable, ATMO_ZONES_TABLE_ID, BARRIERS_TABLE_ID, BARRIER_SETS_TABLE_ID, BUILDINGS_TABLE_ID, EFFECT_ZONES_TABLE_ID, OBJECTS_TABLE_ID, SQUADS_TABLE_ID, TEAM_MANAGER_TABLE_ID, TERRAIN_TABLE_ID, WAYPOINT_GRID_MANAGER_TABLE_ID};
use map_data::terrain::MapVersion;
use crate::entities::atmo_zones_manager::AtmoZonesManager;
use crate::entities::barrier::Barrier;
use crate::entities::barrier_set::BarrierSet;
use crate::entities::building::Building;
use crate::entities::effect_zones_manager::EffectZonesManager;
use crate::entities::EntityByType;
use crate::entities::object::Object;
use crate::entities::squad::Squad;
use crate::entities::world::World;
use crate::id_admin::IdAdministration;
use crate::team_manager::TeamManager;
use crate::waypoint_grid_manager::WaypointGridManager;
use crate::world::chunks::WorldChunks;

mod id_admin;
pub mod entities;
mod waypoint_grid_manager;
mod team_manager;
mod world;
pub mod gui;

pub enum Simulator {
    FakeEmtyToMakeCompilerHappy,
    MapEntitiesLoaded{
        data: SimulatorData,
        path_to_cff: String,
        error: Option<String>,
    },
    MapInitialized{
        data: SimulatorData,
        cff: GameDataCffFile,
    },
}

pub struct SimulatorData {
    id_admin: IdAdministration,
    world: World,
    waypoint_grid_manager: WaypointGridManager,
    team_manager: TeamManager,
    atmo_zones_manager: AtmoZonesManager,
    effect_zones_manager: EffectZonesManager,
    world_chunks: WorldChunks,
}

pub fn init(map: &map_data::Map) -> Simulator {
    let mut id_admin = IdAdministration::default();

    let (mut world_chunks, world) = match map.map.get(&TERRAIN_TABLE_ID).unwrap().0.borrow().deref() {
        MapTable::Terrain(terrain) => {
            let MapVersion::Type23 {
                y, x, ..
            } = &terrain.terrain.terrain;
            let world_chunks = WorldChunks::new(*x, *y);
            (world_chunks, World::new(&mut id_admin, terrain.terrain.clone()))
        },
        _ => unreachable!()
    };
    let waypoint_grid_manager = match map.map.get(&WAYPOINT_GRID_MANAGER_TABLE_ID).unwrap().0.borrow().deref() {
        MapTable::Unknown(bytes) => WaypointGridManager::new(bytes),
        _ => unreachable!()
    };
    let team_manager = match map.map.get(&TEAM_MANAGER_TABLE_ID).unwrap().0.borrow().deref() {
        MapTable::Unknown(bytes) => TeamManager::new(bytes),
        _ => unreachable!()
    };
    let atmo_zones_manager = match map.map.get(&ATMO_ZONES_TABLE_ID).unwrap().0.borrow().deref() {
        MapTable::AtmoZones(am) => AtmoZonesManager::new(&mut id_admin, am.clone()),
        _ => unreachable!()
    };
    let effect_zones_manager = match map.map.get(&EFFECT_ZONES_TABLE_ID).unwrap().0.borrow().deref() {
        MapTable::EffectZones(ez) => EffectZonesManager::new(&mut id_admin, ez.clone()),
        _ => unreachable!()
    };
    if map.map.get(&1013).is_some() {
        panic!("Table 1013 is unknown")
    }
    if let Some((t, _, _, _)) = map.map.get(&SQUADS_TABLE_ID) {
        match t.borrow().deref() {
            MapTable::Squads(entities, _) => {
                for e in &entities.0 {
                    let e = Squad::new(&mut id_admin, e);
                    world_chunks.add_entity(&e.entity_base);
                    id_admin.add_entity(EntityByType::Squad(e));
                }
            },
            _ => unreachable!()
        };
    }
    if let Some((t, _, _, _)) = map.map.get(&OBJECTS_TABLE_ID) {
        match t.borrow().deref() {
            MapTable::Objects(entities, _) => {
                for e in &entities.0 {
                    let e = Object::new(&mut id_admin, e);
                    world_chunks.add_entity(&e.entity_base);
                    id_admin.add_entity(EntityByType::Object(e));
                }
            },
            _ => unreachable!()
        };
    }
    if let Some((t, _, _, _)) = map.map.get(&BARRIER_SETS_TABLE_ID) {
        match t.borrow().deref() {
            MapTable::BarrierSets(entities, _) => {
                for e in &entities.0 {
                    let e = BarrierSet::new(&mut id_admin, e);
                    world_chunks.add_entity(&e.entity_base.b);
                    id_admin.add_entity(EntityByType::BarrierSet(e));
                }
            },
            _ => unreachable!()
        };
    }
    if let Some((t, _, _, _)) = map.map.get(&BARRIERS_TABLE_ID) {
        match t.borrow().deref() {
            MapTable::Barriers(entities, _) => {
                for e in &entities.0 {
                    let e = Barrier::new(&mut id_admin, e);
                    world_chunks.add_entity(&e.entity_base.b);
                    id_admin.add_entity(EntityByType::Barrier(e));
                }
            },
            _ => unreachable!()
        };
    }
    if let Some((t, _, _, _)) = map.map.get(&BUILDINGS_TABLE_ID) {
        match t.borrow().deref() {
            MapTable::Buildings(entities, _) => {
                for e in &entities.0 {
                    let e = Building::new(&mut id_admin, e);
                    world_chunks.add_entity(&e.entity_base.b);
                    id_admin.add_entity(EntityByType::Building(e));
                }
            },
            _ => unreachable!()
        };
    }

    // todo load more
    //id_admin.create_figures_for_squads();
    Simulator::MapEntitiesLoaded {
        data: SimulatorData {
            id_admin,
            world,
            waypoint_grid_manager,
            team_manager,
            atmo_zones_manager,
            effect_zones_manager,
            world_chunks,
        },
        path_to_cff: String::new(),
        error: None,
    }
}