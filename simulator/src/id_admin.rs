use std::collections::BTreeMap;
use std::num::NonZeroU32;
use sr_libs::cff::GameDataCffFile;
use crate::entities::EntityByType;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct AbilityId(NonZeroU32);
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct EntityId(NonZeroU32);
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct JobChainId(NonZeroU32);
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct JHobId(NonZeroU32);

pub struct IdAdministration {
    next_ability_id: AbilityId,
    next_entity_id: EntityId,
    next_job_chain_id: JobChainId,
    next_job_id: JHobId,
    entities: BTreeMap<EntityId, EntityByType>,
    script_tags: BTreeMap<String, EntityId>,
}

impl Default for IdAdministration {
    fn default() -> Self {
        Self {
            next_ability_id: AbilityId(NonZeroU32::new(1).unwrap()),
            next_entity_id: EntityId(NonZeroU32::new(1).unwrap()),
            next_job_chain_id: JobChainId(NonZeroU32::new(1).unwrap()),
            next_job_id: JHobId(NonZeroU32::new(1).unwrap()),
            entities: Default::default(),
            script_tags: Default::default(),
        }
    }
}

impl IdAdministration {
    pub fn init_after_map_load(&mut self, cff: &GameDataCffFile) {
        let squads : Vec<_> = self.entities.iter().filter_map(|(id, e)| {
            match e {
                EntityByType::Squad(s) => {
                    Some((*id, cff.squads.entities.get(&s.entity_id.0).unwrap()))
                }
                _ => None,
            }
        }).collect();
        for (eid, res_squad) in squads {
            let squad = self.entities.get_mut(&eid).unwrap();
            //squad.entity_base().job_manager.unwrap()
            // todo CGdJobChainIdle add to job_manager
            // todo add res_squad.class
        }
    }
    pub fn next_entity_id(&mut self) -> EntityId {
        let id = self.next_entity_id;
        self.next_entity_id = EntityId(id.0.checked_add(1).expect("no more ID's left..."));
        id
    }
    pub fn add_entity(&mut self, e: EntityByType) {
        if let Some(old_entity) = self.entities.insert(e.id(), e) {
            panic!("Entity with ID {:?} already exist: {:?}", old_entity.id(), old_entity);
        }
    }
    pub fn add_script_tag(&mut self, tag: String, entity_id: EntityId) {
        if let Some(old_entity_id) = self.script_tags.insert(tag, entity_id) {
            panic!("Tag already exist for entity: {:?}", old_entity_id);
        }
    }
}
