use crate::entities::{AcquirableEntity, EntityType};
use crate::id_admin::IdAdministration;

#[derive(Debug)]
pub struct Building{
    pub entity_base: AcquirableEntity,
}

impl Building {
    pub fn new(id_admin: &mut IdAdministration, data: &map_data::buildings::Building) -> Self {
        let entity_base = AcquirableEntity::new(id_admin, &data.b, EntityType::Building);
        Self{
            entity_base,
        }
    }
}