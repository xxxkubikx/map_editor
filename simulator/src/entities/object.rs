use crate::entities::aspect::{Aspect, AspectSpecific};
use crate::entities::{EntityPositioned, EntityType};
use crate::id_admin::IdAdministration;

#[derive(Debug)]
pub struct Object{
    pub entity_base: EntityPositioned,
}

impl Object {
    pub fn new(id_admin: &mut IdAdministration, data: &map_data::object::Object) -> Self {
        let mut entity_base = EntityPositioned::new(id_admin, &data.b, EntityType::Object);
        entity_base.b.aspects.push(Aspect{
            owner: entity_base.b.id,
            specific: AspectSpecific::Attackable {},
        });
        Self{
            entity_base,
        }
    }
}