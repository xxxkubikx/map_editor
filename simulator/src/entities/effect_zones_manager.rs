use map_data::effect_zones::EffectZones;
use crate::entities::{EntityBase, EntityType};
use crate::id_admin::IdAdministration;

pub struct EffectZonesManager {
    pub entity_base: EntityBase,
    data: EffectZones,
}

impl EffectZonesManager {
    pub fn new(id_admin: &mut IdAdministration, data: EffectZones) -> Self {
        Self{
            entity_base: EntityBase::new(id_admin, data.unknown as u8, EntityType::EffectZoneManager),
            data
        }
    }
}