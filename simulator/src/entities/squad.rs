use sr_libs::cff::ResSquadId;
use crate::entities::{EntityPositioned, EntityType};
use crate::id_admin::IdAdministration;

#[derive(Debug)]
pub struct Squad{
    pub entity_base: EntityPositioned,
    pub entity_id: ResSquadId,
    pub team: u8,
}

impl Squad {
    pub fn new(id_admin: &mut IdAdministration, data: &map_data::squad::Squad) -> Self {
        Self{
            entity_base: EntityPositioned::new(id_admin, &data.b, EntityType::Squad),
            entity_id: ResSquadId(data.entity_id),
            team: data.team,
        }
    }
}