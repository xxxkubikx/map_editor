use map_data::atmo_zones::AtmoZones;
use crate::entities::{EntityBase, EntityType};
use crate::id_admin::IdAdministration;

pub struct AtmoZonesManager {
    pub entity_base: EntityBase,
    data: AtmoZones,
}

impl AtmoZonesManager {
    pub fn new(id_admin: &mut IdAdministration, data: AtmoZones) -> Self {
        Self{
            entity_base: EntityBase::new(id_admin, data.unknown_byte, EntityType::AtmoZoneManager),
            data
        }
    }
}