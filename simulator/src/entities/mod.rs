use map_data::entity::{IdEntity, Position, Rotation};
use crate::entities::aspect::Aspect;
use crate::id_admin::{EntityId, IdAdministration};

pub mod aspect;
pub mod atmo_zones_manager;
pub mod barrier;
pub mod barrier_set;
pub mod building;
pub mod effect_zones_manager;
pub mod object;
pub mod squad;
pub mod world;

#[derive(Debug, Copy, Clone)]
#[repr(u32)]
pub enum EntityType {
    None = 0x0,
    Figure = 0x1,
    Building = 0x2,
    Object = 0x3,
    ScriptGroup = 0x4,
    Turret = 0x5,
    Squad = 0x6,
    Player = 0x7,
    World = 0x8,
    _9 = 0x9,
    PlayerKit = 0xA,
    _B = 0xB,
    EntityAbilityWorldObject = 0xC,
    _D = 0xD,
    BarrierModule = 0xE,
    Projectile = 0xF,
    BarrierSet = 0x10,
    AtmoZoneManager = 0x11,
    EffectZoneManager = 0x12,
    PowerSlot = 0x13,
    _14 = 0x14,
    _15 = 0x15,
    _16 = 0x16,
    _17 = 0x17,
    _18 = 0x18,
    StartingPoint = 0x19,
    SoundEmitter = 0x1A,
    TokenSlot = 0x1B,
    EntityEffect = 0x1C,
    ScriptingArea = 0x1D,
    ScriptingPath = 0x1E,
}

impl EntityType {
    pub fn try_from(v: u32) -> Result<Self, u32> {
        match v {
            0x0 => Ok(Self::None),
            0x1 => Ok(Self::Figure),
            0x2 => Ok(Self::Building),
            0x3 => Ok(Self::Object),
            0x4 => Ok(Self::ScriptGroup),
            0x5 => Ok(Self::Turret),
            0x6 => Ok(Self::Squad),
            0x7 => Ok(Self::Player),
            0x8 => Ok(Self::World),
            0x9 => Ok(Self::_9),
            0xA => Ok(Self::PlayerKit),
            0xB => Ok(Self::_B),
            0xC => Ok(Self::EntityAbilityWorldObject),
            0xD => Ok(Self::_D),
            0xE => Ok(Self::BarrierModule),
            0xF => Ok(Self::Projectile),
            0x10 => Ok(Self::BarrierSet),
            0x11 => Ok(Self::AtmoZoneManager),
            0x12 => Ok(Self::EffectZoneManager),
            0x13 => Ok(Self::PowerSlot),
            0x14 => Ok(Self::_14),
            0x15 => Ok(Self::_15),
            0x16 => Ok(Self::_16),
            0x17 => Ok(Self::_17),
            0x18 => Ok(Self::_18),
            0x19 => Ok(Self::StartingPoint),
            0x1A => Ok(Self::SoundEmitter),
            0x1B => Ok(Self::TokenSlot),
            0x1C => Ok(Self::EntityEffect),
            0x1D => Ok(Self::ScriptingArea),
            0x1E => Ok(Self::ScriptingPath),
            u => Err(u),
        }
    }
}

#[derive(Debug)]
pub struct EntityBase {
    pub id: EntityId,
    pub entity_type: EntityType,
    pub map_layer: u8,
    pub abilities: Vec<()>,
    pub aspects: Vec<Aspect>,
    pub job_manager: Option<()>,
}

impl EntityBase {
    pub fn new(id_admin: &mut IdAdministration, map_layer: u8, et: EntityType) -> Self {
        Self {
            id: id_admin.next_entity_id(),
            entity_type: et,
            map_layer,
            abilities: vec![],
            aspects: vec![],
            job_manager: None,
        }
    }
    pub fn new_with_params(id_admin: &mut IdAdministration, map_layer: u8, id_entity: &IdEntity, et: EntityType) -> Self {
        let id = id_admin.next_entity_id();
        let aspects = id_entity.params.iter().map(|p|{
            Aspect::new(id_admin, id, p)
        }).collect();
        Self {
            id,
            entity_type: et,
            map_layer,
            abilities: vec![],
            aspects,
            job_manager: None,
        }
    }
}

#[derive(Debug)]
pub struct EntityPositioned {
    pub b: EntityBase,
    pub collision_pos: Position,
    pub vis_pos: Position,
    pub rotation: Rotation,
}

impl EntityPositioned {
    pub fn new(id_admin: &mut IdAdministration, ep: &map_data::entity::PositionedEntity, et: EntityType) -> Self {
        Self {
            b: EntityBase::new_with_params(id_admin, ep.unknown, &ep.b, et),
            collision_pos: ep.collision_pos.clone(),
            vis_pos: ep.vis_pos.clone(),
            rotation: ep.rotation,
        }
    }
}
#[derive(Debug)]
pub struct AcquirableEntity {
    pub b: EntityPositioned,
    pub no_player_acquire: bool,
}

impl AcquirableEntity {
    pub fn new(id_admin: &mut IdAdministration, e: &map_data::entity::AcquirableEntity, et: EntityType) -> Self {
        Self {
            b: EntityPositioned::new(id_admin, &e.b, et),
            no_player_acquire: e.no_player_acquire,
        }
    }
}

#[derive(Debug)]
pub enum EntityByType {
    Squad(squad::Squad),
    Object(object::Object),
    Barrier(barrier::Barrier),
    BarrierSet(barrier_set::BarrierSet),
    Building(building::Building),
}
impl EntityByType {
    pub fn entity_base(&self) -> &EntityBase {
        match self {
            EntityByType::Squad(e) => &e.entity_base.b,
            EntityByType::Object(e) => &e.entity_base.b,
            EntityByType::Barrier(e) => &e.entity_base.b.b,
            EntityByType::BarrierSet(e) => &e.entity_base.b.b,
            EntityByType::Building(e) => &e.entity_base.b.b,
        }
    }
    pub fn id(&self) -> EntityId {
        self.entity_base().id
    }
}