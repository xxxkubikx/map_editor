use map_data::entity::Param;
use crate::id_admin::{EntityId, IdAdministration};

#[derive(Debug)]
pub struct Aspect{
    pub owner: EntityId,
    pub specific: AspectSpecific,
}

#[derive(Debug)]
pub enum AspectSpecific {
    EditorUniqueID{
        unique_id: u32,
    },
    Tag{
        tag: String,
    },
    PlayerKit{
        pkit: String,
    },
    Loot{
        loot_id: u32,
    },
    Attackable{

    },
}

impl Aspect {
    pub fn new(id_admin: &mut IdAdministration, owner: EntityId, p: &Param) -> Self {
        let specific = match p {
            Param::Id(id) => {
                AspectSpecific::EditorUniqueID {
                    unique_id: *id,
                }
            }
            Param::Tag(tag) => {
                id_admin.add_script_tag(tag.clone(), owner);
                AspectSpecific::Tag {
                    tag: tag.clone(),
                }
            }
            Param::PKit(pkit) => {
                AspectSpecific::PlayerKit {
                    pkit: pkit.clone(),
                }
            }
            Param::Chest(chest) => {
                AspectSpecific::Loot {
                    loot_id: *chest,
                }
            }
            Param::BUG(_) => {
                todo!()
            }
        };
        Self {
            owner,
            specific,
        }
    }
}