use crate::entities::{AcquirableEntity, EntityType};
use crate::id_admin::IdAdministration;

#[derive(Debug)]
pub struct Barrier{
    pub entity_base: AcquirableEntity,
}

impl Barrier {
    pub fn new(id_admin: &mut IdAdministration, data: &map_data::barrier::Barrier) -> Self {
        let entity_base = AcquirableEntity::new(id_admin, &data.b, EntityType::BarrierModule);
        Self{
            entity_base,
        }
    }
}