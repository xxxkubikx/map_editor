use crate::entities::{AcquirableEntity, EntityType};
use crate::id_admin::IdAdministration;

#[derive(Debug)]
pub struct BarrierSet{
    pub entity_base: AcquirableEntity,
}

impl BarrierSet {
    pub fn new(id_admin: &mut IdAdministration, data: &map_data::barrier::BarrierSet) -> Self {
        let entity_base = AcquirableEntity::new(id_admin, &data.b, EntityType::BarrierSet);
        Self{
            entity_base,
        }
    }
}