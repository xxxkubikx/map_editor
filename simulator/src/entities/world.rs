use map_data::terrain::{Blocking, CGdCliffs, CGdWaterMap, Heights, TextureMasks};
use crate::entities::{EntityBase, EntityType};
use crate::id_admin::IdAdministration;

pub struct World {
    pub entity_base: EntityBase,
    pub size_x: u16,
    pub size_y: u16,
    pub real_size_x: f32,
    pub real_size_y: f32,
    pub blocking: Blocking,
    pub heights_map: Heights,
    pub aviatic_heights_map: Heights,
    pub texture_masks: TextureMasks,
    pub water: CGdWaterMap,
    pub cliffs: CGdCliffs,
}

impl World {
    pub fn new(id_admin: &mut IdAdministration, data: map_data::terrain::TerrainData) -> Self {
        let map_data::terrain::MapVersion::Type23 {
            y, x, blocking, heights_map, aviatic_heights_map, texture_masks, water, cliffs
        } = data.terrain;
        Self {
            entity_base: EntityBase::new(id_admin, data.map_layer, EntityType::World),
            size_x: x,
            size_y: y,
            real_size_x: 0.0,
            real_size_y: 0.0,
            blocking,
            heights_map,
            aviatic_heights_map,
            texture_masks,
            water,
            cliffs,
        }
    }
}