DefineEvent({
  sName = "OnEvent",
  sDesc = "Event that is being executed continuously as long as the conditions are true.",
  ktEventNameParam,
  ktConditionsParam,
  ktActionsParam,
  ktGotoStateParam
})
DefineEvent({
  sName = "OnOneTimeEvent",
  sDesc = "Event that is only executed once, when the conditions become true for the first time.",
  ktEventNameParam,
  ktConditionsParam,
  ktActionsParam,
  ktGotoStateParam
})
DefineEvent({
  sName = "OnKeyPressEvent",
  sDesc = "Event that is being executed when a key combo is pressed.",
  ktScriptTagPlayerEventParam,
  ktKeyComboEventParam,
  ktKeyShiftEventParam,
  ktKeyCtrlEventParam,
  ktKeyAltEventParam,
  ktConditionsParam,
  ktActionsParam,
  ktGotoStateParam
})
DefineEvent({
  sName = "OnOneTimeKeyPressEvent",
  sDesc = "Event that is being executed once when a key combo is pressed.",
  ktScriptTagPlayerEventParam,
  ktKeyComboEventParam,
  ktKeyShiftEventParam,
  ktKeyCtrlEventParam,
  ktKeyAltEventParam,
  ktConditionsParam,
  ktActionsParam,
  ktGotoStateParam
})
DefineEvent({
  sName = "OnToggleEvent",
  sDesc = "Event that toggles between two states, which are On and Off. Has nothing to do with script states!",
  ktEventNameParam,
  ktOnConditionsParam,
  ktOnActionsParam,
  ktOffConditionsParam,
  ktOffActionsParam
})
DefineEvent({
  sName = "OnTimerEvent",
  sDesc = "Event that starts anytime and ends after some time has elapsed, then restarts the timer.",
  ktEventNameParam,
  ktTimerEventMinutesParam,
  ktTimerEventSecondsParam,
  ktTimerStartConditionsParam,
  ktTimerStartActionsParam,
  ktTimerStopConditionsParam,
  ktTimerStopActionsParam,
  ktTimerEventMultipleTimeConstant
})
DefineEvent({
  sName = "OnIntervalEvent",
  sDesc = "Event that is checked/executed only every x seconds. Useful for Filtered Actions or time-consuming conditions.",
  ktEventNameParam,
  ktTimerEventSecondsParam,
  ktConditionsParam,
  ktActionsParam
})
DefineEvent({
  sName = "OnOneTimeTimerEvent",
  sDesc = "Event that starts anytime and ends after some time has elapsed and only once.",
  ktEventNameParam,
  ktTimerEventMinutesParam,
  ktTimerEventSecondsParam,
  ktTimerStartConditionsParam,
  ktTimerStartActionsParam,
  ktTimerStopConditionsParam,
  ktTimerStopActionsParam,
  ktTimerEventOneTimeConstant
})
DefineEvent({
  sName = "OnRespawnEvent",
  sDesc = "Respawns an entity or group and executes Actions on every respawn.",
  ktStartDespawnedEventParam,
  ktScriptTagSpawnTargetEventParam,
  ktRespawnDelayMinutesEventParam,
  ktRespawnDelaySecondsEventParam,
  ktHealthPercentEventParam,
  ktConditionsParam,
  ktActionsParam,
  ktDeathActionsParam,
  ktGotoStateParam
})
DefineEvent({
  sName = "OnIdleGoHomeEvent",
  sDesc = "If the Squad is Idle it will return to a specified location.",
  ktEventNameParam,
  ktScriptTagEventParam,
  ktScriptTagTargetEventParam,
  ktRangeEventParam,
  ktWalkModeRunEventParam,
  ktDirectionEventParam,
  ktConditionsParam,
  ktGoActionsParam,
  ktHomeActionsParam,
  ktOneTimeHomeActionsParam
})
DefineEvent({
  sName = "OnIdleContinueAttackEvent",
  sDesc = "If the Squad is Idle it will attack the next valid Target from the TargetTag ScriptGroup.",
  ktEventNameParam,
  ktScriptTagEventParam,
  ktScriptTagTargetEventParam,
  ktAttackGroupSizeEventParam,
  ktConditionsParam,
  ktActionsParam
})
DefineEvent({
  sName = "OnDeadEvent",
  sDesc = "This event is only active as long as the entity is dead.",
  ktEventNameParam,
  ktConditionsParam,
  ktActionsParam
})
DefineEvent({
  sName = "OnOneTimeDeadEvent",
  sDesc = "This event is only active as long as the entity is dead. It will only be executed once.",
  ktEventNameParam,
  ktConditionsParam,
  ktActionsParam
})
DefineEvent({
  sName = "OnBuildingNotUnderAttackEvent",
  sDesc = "Event that only executes if the Building is not under attack.",
  ktEventNameParam,
  ktConditionsParam,
  ktActionsParam,
  ktGotoStateParam,
  ktScriptTagEventParam
})
