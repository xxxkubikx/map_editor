DefineWarfarePattern({
  sName = sPatternName,
  sDesc = "Spawns Waves of Squads based on _SpawnGroups definitions.",
  iCreateDate = GetDays(2008, 2, 12),
  ktScriptTagTargetParam,
  ktDirectionParam,
  ktTeamParam,
  ktScriptTagPlayerNoQuantorParam,
  "AfterSpawnGotoTargetTag",
  "SpawnWaveTemplate",
  "InitialSpawnDelaySeconds",
  "InitialSpawnDelayMinutes",
  "SpawnBuilding",
  Pattern = function(_ARG_0_)
    if type(_ARG_0_.SpawnBuilding) == "string" and GetEntityType(_ARG_0_.SpawnBuilding) == TypeBuilding then
    else
    end
    if _ARG_0_.TargetTag then
      table.insert(_ARG_0_.StartConditions, BuildingIsAlive({
        For = "ALL",
        Tag = _ARG_0_.TargetTag
      }))
      table.insert(_ARG_0_.ExitConditions, BuildingIsDestroyed({
        For = "ALL",
        Tag = _ARG_0_.TargetTag
      }))
    end
    WP_AddControlStateNoDeathEvents(_ARG_0_)
    if GetWaveParams().InitialSpawnDelaySeconds <= 0 and 0 >= GetWaveParams().InitialSpawnDelayMinutes then
      for _FORV_7_, _FORV_8_ in ipairs(_ARG_0_.FinishActions) do
        table.insert(CreateSpawnAllActions(_ARG_0_), _FORV_8_)
      end
      table.insert(CreateSpawnAllActions(_ARG_0_), EntityFlagSetTrue({
        Name = "ef_WaveAlreadySpawnedAtOnceBefore"
      }))
      OnOneTimeEvent({
        EventName = "StartWaveSpawned",
        Conditions = {
          EntityFlagIsFalse({
            Name = "ef_WaveAlreadySpawnedAtOnceBefore"
          })
        },
        Actions = CreateSpawnAllActions(_ARG_0_),
        GotoState = _ARG_0_.FinishState
      })
    elseif GetWaveParams().InitialSpawnDelaySeconds > 0 or 0 < GetWaveParams().InitialSpawnDelayMinutes then
      if GetWaveParams().RespawnDelayMinutes == 0 and GetWaveParams().RespawnDelaySeconds == 0 then
        GetWaveParams().RespawnDelaySeconds = 1
      end
      OnOneTimeEvent({
        EventName = "WaitForInitialSpawn",
        Conditions = {},
        Actions = {},
        GotoState = WP_GetStartStateName() .. "WaitForInitialSpawn"
      })
    end
    State({
      StateName = WP_GetStartStateName(),
      OnEvent({
        EventName = "WaitForAllDeadAndRespawnTimerElapsed",
        Conditions = {
          EntityTimerIsElapsed({
            Name = "et_WP_UpdateInterval",
            Minutes = GetWaveParams().RespawnDelayMinutes,
            Seconds = GetWaveParams().RespawnDelaySeconds
          }),
          SquadIsDead({
            For = "ALL",
            Tag = GetScriptTag()
          })
        },
        Actions = {
          ScriptGroupClear({
            Group = GetScriptTag()
          })
        },
        GotoState = WP_GetStartStateName() .. "WaitForNextSquadSpawn"
      }),
      WP_AddStandardEvents(_ARG_0_)
    })
    if GetWaveParams().InitialSpawnDelaySeconds > 0 or 0 < GetWaveParams().InitialSpawnDelayMinutes then
      State({
        StateName = WP_GetStartStateName() .. "WaitForInitialSpawn",
        OnEvent({
          EventName = "InitialSpawnDelayElapsed",
          Conditions = {
            EntityTimerIsElapsed({
              Name = "et_WP_UpdateInterval",
              Minutes = GetWaveParams().InitialSpawnDelayMinutes,
              Seconds = GetWaveParams().InitialSpawnDelaySeconds
            })
          },
          Actions = {
            ScriptGroupClear({
              Group = GetScriptTag()
            })
          },
          GotoState = WP_GetStartStateName() .. "WaitForNextSquadSpawn"
        }),
        WP_AddStandardEvents(_ARG_0_)
      })
    end
    if _ARG_0_.TargetTag then
      State({
        StateName = WP_GetStartStateName() .. "BuildingHitTimeOut",
        OnEvent({
          EventName = "BuildingGotHitAgain",
          Conditions = {
            EntityTimerIsElapsed({
              Name = "et_WP_SpawnWave_BuildingHitTimeOut",
              Seconds = 3
            }),
            BuildingWasJustHit({
              For = "ANY",
              Tag = _ARG_0_.TargetTag
            })
          },
          Actions = {
            EntityTimerStart({
              Name = "et_WP_SpawnWave_BuildingHitTimeOut"
            })
          }
        }),
        OnEvent({
          EventName = "BuildingHitTimeOutOver",
          Conditions = {
            EntityTimerIsElapsed({
              Name = "et_WP_SpawnWave_BuildingHitTimeOut",
              Seconds = 3
            })
          },
          Actions = {
            EntityTimerStop({
              Name = "et_WP_SpawnWave_BuildingHitTimeOut"
            })
          },
          GotoState = WP_GetStartStateName() .. "WaitForNextSquadSpawn"
        }),
        WP_AddStandardEvents(_ARG_0_)
      })
      OnEvent({
        EventName = "BuildingGotHit",
        Conditions = {
          BuildingWasJustHit({
            For = "ANY",
            Tag = _ARG_0_.TargetTag
          })
        },
        Actions = {
          EntityTimerStart({
            Name = "et_WP_SpawnWave_BuildingHitTimeOut"
          })
        },
        GotoState = WP_GetStartStateName() .. "BuildingHitTimeOut"
      })
    end
    State({
      StateName = WP_GetStartStateName() .. "WaitForNextSquadSpawn",
      OnEvent({
        EventName = "FINISH -> whole Group is Alive!",
        Conditions = {
          SquadIsAlive({
            For = "ALL",
            Tag = GetScriptTag()
          })
        },
        Actions = _ARG_0_.FinishActions,
        GotoState = _ARG_0_.FinishState
      }),
      OnEvent({
        EventName = "Spawn-A-Squad",
        Conditions = {
          EntityTimerIsElapsed({
            Name = "et_WP_UpdateInterval",
            Seconds = GetWaveParams().SpawnInterval
          }),
          EntityIsAlive({
            Tag = _ARG_0_.TargetTag
          })
        },
        Actions = {
          EntityTimerStart({
            Name = "et_WP_UpdateInterval"
          })
        },
        GotoState = WP_GetStartStateName() .. "SpawnOneOfThemSquads"
      }),
      WP_AddStandardEvents(_ARG_0_)
    })
    State({
      StateName = WP_GetStartStateName() .. "SpawnOneOfThemSquads",
      CreateSpawnEvents(_ARG_0_),
      WP_AddStandardEvents(_ARG_0_)
    })
  end,
  WP_AddStandardParams()
})
function CreateSpawnEvents(_ARG_0_)
  if _ARG_0_.SpawnWaveTemplate and _ARG_0_.SpawnWaveTemplate ~= "" then
  else
  end
    --[[ TODO to which table is this trying to insert?
  for _FORV_6_, _FORV_7_ in pairs((GetSpawnWaveTags())) do
    table.insert({}, ScriptGroupTagAdd({
      Group = GetScriptTag(),
      Tag = _FORV_6_
    }))
  end ]]--
  if table.getn({}) > 0 then
    OnEvent({
      EventName = "Add Tags to ScriptGroup",
      Conditions = {
        ScriptGroupIsEmpty({
          Group = GetScriptTag()
        })
      },
      Actions = {}
    })
    OnEvent({
      EventName = "Abort Spawn: SpawnBuilding DEAD!",
      Conditions = {
        EntityIsDead({
          Tag = _ARG_0_.TargetTag
        })
      },
      Actions = {},
      GotoState = WP_GetStartStateName() .. "WaitForNextSquadSpawn"
    })
  end
  for _FORV_7_, _FORV_8_ in pairs((GetSpawnWaveTags())) do
    table.insert({}, SquadSpawnIntoGroup({
      Tag = _FORV_7_,
      Group = GetScriptTag(),
      TargetTag = _ARG_0_.TargetTag,
      Team = _ARG_0_.Team,
      SquadId = _FORV_8_,
      Direction = _ARG_0_.Direction
    }))
    if type(_ARG_0_.Player) == "string" then
      table.insert({}, EntityPlayerSet({
        Tag = _FORV_7_,
        Player = _ARG_0_.Player
      }))
    end
    if type(_ARG_0_.AfterSpawnGotoTargetTag) == "string" and _ARG_0_.AfterSpawnGotoTargetTag ~= _ARG_0_.TargetTag then
      table.insert({}, SquadGoto({
        Tag = _FORV_7_,
        TargetTag = _ARG_0_.AfterSpawnGotoTargetTag
      }))
    end
    OnEvent({
      Conditions = {
        SquadIsDead({Tag = _FORV_7_}),
        EntityIsAlive({
          Tag = _ARG_0_.TargetTag
        })
      },
      Actions = {},
      GotoState = WP_GetStartStateName() .. "WaitForNextSquadSpawn"
    })
  end
end
function CreateSpawnAllActions(_ARG_0_)
  if _ARG_0_.SpawnWaveTemplate and _ARG_0_.SpawnWaveTemplate ~= "" then
  else
  end
  table.insert({}, ScriptGroupClear({
    Group = GetScriptTag()
  }))
  for _FORV_6_, _FORV_7_ in pairs((GetSpawnWaveTags())) do
    table.insert({}, ScriptGroupTagAdd({
      Group = GetScriptTag(),
      Tag = _FORV_6_
    }))
  end
  for _FORV_6_, _FORV_7_ in pairs((GetSpawnWaveTags())) do
    table.insert({}, SquadSpawnIntoGroup({
      Tag = _FORV_6_,
      Group = GetScriptTag(),
      TargetTag = _ARG_0_.TargetTag,
      Team = _ARG_0_.Team,
      SquadId = _FORV_7_,
      Direction = _ARG_0_.Direction
    }))
    if type(_ARG_0_.Player) == "string" then
      table.insert({}, EntityPlayerSet({
        Tag = _FORV_6_,
        Player = _ARG_0_.Player
      }))
    end
  end
  if type(_ARG_0_.AfterSpawnGotoTargetTag) == "string" and _ARG_0_.AfterSpawnGotoTargetTag ~= _ARG_0_.TargetTag then
    table.insert({}, SquadGoto({
      Tag = GetScriptTag(),
      TargetTag = _ARG_0_.AfterSpawnGotoTargetTag
    }))
  end
  return {}
end
