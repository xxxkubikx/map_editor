DefineWarfarePattern({
  sName = sPatternName,
  sDesc = "Respawns the SquadGroup (all at once) after a specified delay.",
  iCreateDate = GetDays(2008, 1, 16),
  ktScriptTagTargetParam,
  ktRespawnDelayMinutesEventParam,
  ktRespawnDelaySecondsEventParam,
  Pattern = function(_ARG_0_)
    WP_AddControlStateNoDeathEvents(_ARG_0_)
    State({
      StateName = WP_GetStartStateName(),
      OnEvent({
        EventName = "Squads go to TargetTag",
        Conditions = {
          EntityTimerIsElapsed({
            Name = "et_WP_UpdateInterval",
            Minutes = _ARG_0_.RespawnDelayMinutes,
            Seconds = _ARG_0_.RespawnDelaySeconds
          }),
          SquadIsDead({
            For = "ALL",
            Tag = GetScriptTag()
          })
        },
        Actions = {
          SquadRespawn({
            Tag = GetScriptTag(),
            TargetTag = _ARG_0_.TargetTag,
            HealthPercent = default
          })
        },
        GotoState = _ARG_0_.FinishState
      }),
      WP_AddStandardEvents(_ARG_0_)
    })
  end,
  WP_AddStandardParams()
})
