function tprint (tbl, indent)
  if not indent then indent = 0 end
  local toprint = string.rep(" ", indent) .. "{\r\n"
  indent = indent + 2 
  for k, v in pairs(tbl) do
    toprint = toprint .. string.rep(" ", indent)
    if (type(k) == "number") then
      toprint = toprint .. "[" .. k .. "] = "
    elseif (type(k) == "string") then
      toprint = toprint  .. k ..  "= "   
    end
    if (type(v) == "number") then
      toprint = toprint .. v .. ",\r\n"
    elseif (type(v) == "string") then
      toprint = toprint .. "\"" .. v .. "\",\r\n"
    elseif (type(v) == "table") then
      toprint = toprint .. "\r\n" .. tprint(v, indent + 2) .. ",\r\n"
    else
      toprint = toprint .. "\"" .. tostring(v) .. "\",\r\n"
    end
  end
  toprint = toprint .. string.rep(" ", indent-2) .. "}"
  return toprint
end

-- print(debug.traceback())

function DumpCommand(_ARG_0_)
  appendfile(GetScriptFile() .. GetScriptTag() .. ".luc", _ARG_0_ .. "\n")
end
if FINAL == false then
  DEBUG = true
end
old_dofile = dofile
function dofile(_ARG_0_)
  if SCRIPTSYSTEM then
    if pcall(old_dofile, _ARG_0_) then
      return pcall(old_dofile, _ARG_0_)
    end
    if DEBUG then
      CScriptMain.ShowMessage("dofile ERROR: " .. tostring(pcall(old_dofile, _ARG_0_)) .. [[


]] .. debug.traceback() .. [[


in file: ]] .. tostring(_ARG_0_))
    end
  else
    if not findfile(_ARG_0_) then
      assert(nil, "ERROR: dofile(" .. tostring(_ARG_0_) .. [[
): file not found!

]] .. debug.traceback() .. [[


in file: ]] .. tostring(_ARG_0_))
    end
    return old_dofile(_ARG_0_)
  end
end
function InitScriptSystem()
  SCRIPTSYSTEM = true
  if DEBUG then
    os.remove((CScriptMain.BuildDiagFileName("GdsLog.txt")))
    fGdsLogFile = io.open(CScriptMain.BuildDiagFileName("GdsLog.txt"), "a")
  end
  ksToolBinPath = "..\\tool\\bin\\"
  ksDotPath = ksToolBinPath .. "dot\\"
  ksMapPath = "bf1\\map\\"
  ksScriptPath = "\\scripts\\"
  if not ksToolLuaPath then
    ksToolLuaPath = "bf1\\script\\"
  end
  if not ksGdsPath then
    ksGdsPath = "bf1\\script\\gds\\"
  end
  if not ksGdsDefinePath then
    ksGdsDefinePath = "bf1\\script\\gds\\definitions\\"
  end
  if not ksGdsWarfarePatternPath then
    ksGdsWarfarePatternPath = "bf1\\script\\gds\\warfarepatterns\\"
  end
  --dofile(ksToolLuaPath .. "tool_lua5.lua")
  tStateSpecs = {}
  tEventSpecs = {}
  tActionConditionSpecs = {}
  tConfig = {}
end
if not SCRIPTCHECK then
  InitScriptSystem()
end