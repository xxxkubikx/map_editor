function DefineWarfarePattern(_ARG_0_)
  _G[_ARG_0_.sName] = _ARG_0_.Pattern
end
local WP_Name = ""
local WP_Start_Name = ""
local WP_End_Name = ""
function WP_GetName()
  return WP_Name
end
function WP_GetStartStateName()
  return WP_Start_Name
end
function WP_GetExitStateName()
  return WP_End_Name
end
function WP_GetDeathStateName()
  return sDeathStateName
end
function WP_CanExit(_ARG_0_)
  return #_ARG_0_.ExitConditions > 0
end
function WP_IsDeadState(_ARG_0_)
  return _ARG_0_.bIsDeadState
end
function WP_AddControlStateNoDeathEvents(_ARG_0_)
  _ARG_0_.bIsDeadState = true
  WP_AddControlState(_ARG_0_)
end
function WP_AddControlState(_ARG_0_)
  if DEBUG then
    assert(type(_ARG_0_.FinishState) == "string" and _ARG_0_.FinishState ~= "self", "WP: FinishState not supplied (can not be 'self')!")
  end
  SetAsWarfarePatternScript()
  WP_Name = _ARG_0_.StateName
  WP_Start_Name = "WP_" .. _ARG_0_.StateName .. "_START"
  WP_End_Name = _ARG_0_.ExitState
  sDeathStateName = _ARG_0_.DeathState
  if not sDeathStateName or sDeathStateName == "" or sDeathStateName == "self" then
    sDeathStateName = "DEADEND"
  end
  if not _ARG_0_.StartConditions then
    _ARG_0_.StartConditions = {}
  end
  if not _ARG_0_.StartActions then
    _ARG_0_.StartActions = {}
  end
  if not _ARG_0_.ExitConditions then
    _ARG_0_.ExitConditions = {}
  end
  if not _ARG_0_.ExitActions then
    _ARG_0_.ExitActions = {}
  end
  table.insert(_ARG_0_.StartActions, EntityTimerStart({
    Name = "et_WP_UpdateInterval"
  }))
  if _ARG_0_.GotoState == "self" or _ARG_0_.GotoState == "" and WP_CanExit(_ARG_0_) then
    _ARG_0_.GotoState = WP_GetName() .. "_END_SCRIPT"
    State({StateName = _ARG_0_.GotoState})
  end
  State({
    StateName = WP_GetName(),
    OnEvent({
      EventName = WP_GetName() .. " - WP - StartConditions satisfied, executing StartActions",
      Conditions = _ARG_0_.StartConditions,
      Actions = _ARG_0_.StartActions,
      GotoState = WP_GetStartStateName()
    }),
    WP_AddStandardEvents(_ARG_0_)
  })
end
function WP_AddStandardEvents(_ARG_0_)
  if not WP_IsDeadState(_ARG_0_) then
    OnEvent({
      EventName = WP_GetName() .. " - WP - SquadGroup is dead!",
      Conditions = {
        SquadIsDead({
          For = "ALL",
          Tag = GetScriptTag()
        })
      },
      Actions = {},
      GotoState = WP_GetDeathStateName()
    })
  end
  if WP_CanExit(_ARG_0_) then
    if _ARG_0_.AllowExitBeforeStart == true then
      OnEvent({
        EventName = WP_GetName() .. " - WP - ExitConditions satisfied!",
        Conditions = _ARG_0_.ExitConditions,
        Actions = _ARG_0_.ExitActions,
        GotoState = WP_GetExitStateName()
      })
    end
    _ARG_0_.AllowExitBeforeStart = true
  end
end
