-- Decompiled using luadec 2.0.2 by sztupy (http://winmo.sztupy.hu)
-- Command line was: D:\lua\Decompiler+DecompiledScript\gdsspawngroupsystem.lub 

local l_0_0 = 0
AllSpawnWaveTemplates = {}
AllSpawnWaveEmitters = {}
Remove = -1111
function SpawnWaveTemplate (l_1_0)
  if DEBUG then
    local l_1_1 = assert
    l_1_1(type(l_1_0.Name) == "string", "SpawnWaveTemplate: missing or invalid parameter 'Name'!")
    l_1_1 = assert
    l_1_1(not AllSpawnWaveTemplates[l_1_0.Name], string.format("SpawnWaveTemplate: a template with Name '%s' is already defined!", l_1_0.Name))
  end
  AllSpawnWaveTemplates[l_1_0.Name] = l_1_0
  local l_1_4 = {}
  for l_1_8,l_1_9 in pairs(l_1_0) do
    if type(l_1_8) == "string" and type(l_1_9) == "table" then
      table.insert(l_1_4, l_1_8)
      local l_1_10 = l_1_9
      local l_1_11 = {}
      for l_1_15 = DifficultyEasy, GetDifficulty() do
        if not l_1_0[l_1_15] then
          l_1_0[l_1_15] = {}
        end
        l_1_11[l_1_15] = table.copy(l_1_0[l_1_15])
        if l_1_10[l_1_15] then
          if l_1_10[l_1_15].Replace then
            l_1_11[l_1_15] = l_1_10[l_1_15]
          else
            for l_1_19,l_1_20 in pairs(l_1_10[l_1_15]) do
              if not l_1_11[l_1_15][l_1_19] then
                l_1_11[l_1_15][l_1_19] = 0
              end
              l_1_11[l_1_15][l_1_19] = l_1_11[l_1_15][l_1_19] + l_1_20
              if l_1_11[l_1_15][l_1_19] < 1 then
                l_1_11[l_1_15][l_1_19] = nil
              end
            end
          end
        end
      end
      CollateSpawnWaveData(l_1_11)
      AllSpawnWaveTemplates[l_1_0.Name .. l_1_8] = l_1_11
    end
  end
  for l_1_24,l_1_25 in ipairs(l_1_4) do
    l_1_0[l_1_25] = nil
  end
  CollateSpawnWaveData(l_1_0)
  AllSpawnWaveTemplates[l_1_0.Name] = l_1_0
end

function CollateSpawnWaveData (l_2_0)
  local l_2_1 = l_0_0
  local l_2_2 = {}
  l_2_2.Difficulty = GetDifficulty()
  l_2_0[l_2_1] = l_2_2
  l_2_1 = DifficultyEasy
  l_2_2 = GetDifficulty
  l_2_2 = l_2_2()
  for i = l_2_1, l_2_2 do
    if not l_2_0[i] then
      l_2_0[i] = {}
    end
    for l_2_8,l_2_9 in pairs(l_2_0[i]) do
      if type(l_2_8) == "number" then
        if not l_2_0[l_0_0][l_2_8] then
          l_2_0[l_0_0][l_2_8] = 0
        end
        l_2_0[l_0_0][l_2_8] = l_2_0[l_0_0][l_2_8] + l_2_9
        if l_2_0[l_0_0][l_2_8] < 1 then
          l_2_0[l_0_0][l_2_8] = nil
        end
      end
    end
  end
   -- DECOMPILER ERROR: Confused about usage of registers for local variables.

end

function GetSpawnWaveTemplate (l_3_0)
  if DEBUG then
    local l_3_1 = assert
    l_3_1(type(l_3_0) == "string", string.format("GetSpawnWaveTemplate - Name is not a string!"))
    l_3_1 = assert
    l_3_1(AllSpawnWaveTemplates[l_3_0], string.format("GetSpawnWaveTemplate - template with Name '%s' does not exist!", l_3_0))
    l_3_1 = assert
    l_3_1(#AllSpawnWaveTemplates[l_3_0] > 0, string.format("GetSpawnWaveTemplate - template with Name '%s' is empty!", l_3_0))
  end
  return AllSpawnWaveTemplates[l_3_0]
end

GetSpawnWaveData = function(l_4_0)
  return GetSpawnWaveTemplate(l_4_0)[l_0_0]
end

function AddDebugInfoToSpawnWaveTemplates ()
  if DEBUG then
    for l_5_3,l_5_4 in pairs(AllSpawnWaveTemplates) do
      for l_5_8,l_5_9 in ipairs(l_5_4) do
        for l_5_13,l_5_14 in pairs(l_5_9) do
          if type(l_5_13) == "number" then
            for l_5_18,l_5_19 in pairs(Squad) do
              if l_5_13 == l_5_19 then
                l_5_9[l_5_18] = l_5_19
                for l_5_13,l_5_14 in l_5_10 do
                end
              end
            end
          end
        end
      end
    end
     -- Warning: missing end command somewhere! Added here
  end
end

function CreateSpawnGroups()
  local l_6_0 = GetScriptPath() .. "_SpawnGroups.lua"
  if FileExists(l_6_0) then
    dofile(l_6_0)
  end
end

function SpawnWaveEmitter (l_7_0)
  if DEBUG then
    local l_7_1 = assert
    l_7_1(type(AllSpawnWaveEmitters) == "table")
    l_7_1 = assert
    l_7_1(type(l_7_0) == "table")
    l_7_1 = assert
    l_7_1(type(l_7_0.GroupName) == "string", "SpawnWaveEmitter - GroupName must be a string!")
    l_7_1 = assert
    l_7_1(string.find(l_7_0.GroupName, "sp_", 1, 1) == 1, "SpawnWaveEmitter - GroupName does not start with 'sp_'!")
    l_7_1 = assert
    l_7_1(type(l_7_0.Waves) == "table", string.format("SpawnWaveEmitter - 'Waves' parameter missing or invalid! (GroupName = '%s')", l_7_0.GroupName))
  end
  l_7_0.GroupName = string.upper(l_7_0.GroupName)
  AllSpawnWaveEmitters[l_7_0.GroupName] = l_7_0
  local l_7_12 = #l_7_0.Waves
  for l_7_16 = 1, l_7_12 do
    l_7_0.Waves[l_7_16].WaveNumber = l_7_16
    if l_7_16 == 1 then
      local l_7_17 = GdsCreateScriptGroup
      local l_7_18 = {}
      l_7_18.Name = l_7_0.GroupName
      l_7_17(l_7_18)
    end
    local l_7_19 = string.upper(string.format("%s_W%02i", l_7_0.GroupName, l_7_16))
    local l_7_21 = {}
    l_7_21.Name = l_7_19
    GdsCreateScriptGroup(l_7_21)
    AddScriptGroupScriptTags(l_7_19, l_7_0, l_7_16)
    GdsAddToScriptGroup(l_7_0.GroupName, l_7_19)
    for i_1,i_2 in pairs(l_7_0) do
      if type(i_2) ~= "table" and l_7_0.Waves[l_7_16][i_1] == nil then
        l_7_0.Waves[l_7_16][i_1] = i_2
      end
    end
    if not l_7_0.Waves[l_7_16].InitialSpawnDelaySeconds then
      l_7_0.Waves[l_7_16].InitialSpawnDelaySeconds = 0
    end
    if not l_7_0.Waves[l_7_16].InitialSpawnDelayMinutes then
      l_7_0.Waves[l_7_16].InitialSpawnDelayMinutes = 0
    end
    -- AllSpawnWaveEmitters[l_7_19] = l_7_0.GroupName
     -- DECOMPILER ERROR: Confused about usage of registers for local variables.

  end
   -- DECOMPILER ERROR: Confused about usage of registers for local variables.

end

function CreateSpawnWaveTags(l_8_0, l_8_1)
  local l_8_2 = GetSpawnWaveData(l_8_0)
  local l_8_3 = {}
  for l_8_7,l_8_8 in pairs(l_8_2) do
    if type(l_8_7) == "number" and l_8_8 > 0 then
      local l_8_9 = l_8_7
      local l_8_10 = l_8_8
      for l_8_14 = 1, l_8_10 do
        l_8_3[string.upper(string.format("%s_ID%i_S%02i", l_8_1, l_8_9, l_8_14))] = l_8_9
      end
    end
  end
  return l_8_3
end

function AddScriptGroupScriptTags(l_9_0, l_9_1, l_9_2)
  local l_9_3 = CreateSpawnWaveTags(l_9_1.Waves[l_9_2].SpawnWaveTemplate, l_9_0)
  if not l_9_1.Tags then
    l_9_1.Tags = {}
  end
  l_9_1.Tags[l_9_0] = {}
  if not l_9_1.WavesByTag then
    l_9_1.WavesByTag = {}
  end
  l_9_1.WavesByTag[l_9_0] = l_9_1.Waves[l_9_2]
  for l_9_7,l_9_8 in pairs(l_9_3) do
    GdsAddToScriptGroup(l_9_0, l_9_7)
    l_9_1.Tags[l_9_0][l_9_7] = l_9_8
  end
end

function GetSpawnGroup()
  local l_10_0 = AllSpawnWaveEmitters[GetScriptTag()]
  if type(l_10_0) == "string" then
    l_10_0 = AllSpawnWaveEmitters[l_10_0]
  end
  return l_10_0
end

function GetSpawnWaveTags()
  for _i, tags in pairs(GetSpawnGroup().Tags) do
    return tags -- TODO this is almost certainly wrong!
  end
  --return GetSpawnGroup().Tags[GetScriptTag()]
end

function GetWaveParams()
  for _i, wave in pairs(GetSpawnGroup().WavesByTag) do
    return wave -- TODO this is almost certainly wrong!
  end
  return GetSpawnGroup().WavesByTag[GetScriptTag()]
end

function GetCurrentWaveNumber()
  return GetSpawnGroup().WavesByTag[GetScriptTag()].WaveNumber
end

function GetTotalWaveNumber()
  return #GetSpawnGroup().Waves
end


