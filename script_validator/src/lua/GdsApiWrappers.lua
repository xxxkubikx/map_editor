--if SCRIPTCHECK then
--  return
--end
if CScriptMain then
  function math.random(_ARG_0_, _ARG_1_)
    if not _ARG_0_ and not _ARG_1_ then
      _ARG_0_ = 0
      _ARG_1_ = 1
    elseif not _ARG_1_ then
      _ARG_1_ = _ARG_0_
      _ARG_0_ = 1
    end
    return CScriptMain.GdRandom(_ARG_0_, _ARG_1_)
  end
end
function ArenaDummyRandom(_ARG_0_, _ARG_1_)
  if not _ARG_0_ and not _ARG_1_ then
    _ARG_0_ = 0
    _ARG_1_ = 1
  elseif not _ARG_1_ then
    _ARG_1_ = _ARG_0_
    _ARG_0_ = 1
  end
  return (CScriptMain.MatRandom(_ARG_0_, _ARG_1_))
end
function GdsGetConfigBoolean(_ARG_0_)
end
function GdsGetConfigInteger(_ARG_0_)
end
function GdsGetConfigFloat(_ARG_0_)
end
function GdsGetConfigString(_ARG_0_)
end
function GdsCreateStateMachine(_ARG_0_)
  if DEBUG then
    assert(type(_ARG_0_) == "string", "GdsCreateStateMachine()" .. "(): _sStateMachineName is not a string!\n")
    LogSM(string.format("%s(\"%s\")", "GdsCreateStateMachine()", _ARG_0_))
    DotDumpStateMachine(_ARG_0_)
  end
  if tConfig.bNoCode then
  else
    if DEBUG then
    end
    if DUMP2DISK then
      os.remove(GetScriptFile() .. ".luc")
      DumpCommand("uSM = CScriptMain.CreateStateMachine([=[" .. string.upper(_ARG_0_) .. "]=], 0, " .. GetScriptUpdateInterval() .. ", " .. GetScriptStartStep() .. ")")
    end
  end
  if DEBUG and not tConfig.bNoCode then
    assert(type((CScriptMain.CreateStateMachine(string.upper(_ARG_0_), 1, GetScriptUpdateInterval(), GetScriptStartStep()))) == "userdata", "GdsCreateStateMachine()" .. "(): _uStateMachine is not userdata!\n")
  end
  return (CScriptMain.CreateStateMachine(string.upper(_ARG_0_), 1, GetScriptUpdateInterval(), GetScriptStartStep()))
end
function GdsCreateState(_ARG_0_, _ARG_1_)
  --if DEBUG then
    --if not tConfig.bNoCode then
    --  assert(type(_ARG_0_) == "userdata", "GdsCreateState()" .. "(): _uStateMachine is not userdata!\n")
    --end
    assert(type(_ARG_1_) == "string", "GdsCreateState()" .. "(): _sStateName is not a string!\n")
    LogSM(string.format("%s(%s, \"%s\")", "GdsCreateState()", tostring(_ARG_0_), _ARG_1_))
    DotDumpState(_ARG_1_)
  --end
  if tConfig.bNoCode then
  elseif DUMP2DISK then
    DumpCommand("uState" .. _ARG_1_ .. " = uSM:AddState([=[" .. _ARG_1_ .. "]=])")
  end
  if DEBUG and not tConfig.bNoCode then
    assert(type((_ARG_0_:AddState(_ARG_1_))) == "userdata", "GdsCreateState()" .. "(): _uState is not userdata!\n")
  end
  return (_ARG_0_:AddState(_ARG_1_))
end
function CreateTransitionName(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_)
  if _ARG_0_ == _ARG_1_ then
  else
  end
  return (string.format("[%s] to [%s] (%s) %s", _ARG_0_, _ARG_1_, tostring(_ARG_2_), tostring(_ARG_3_)))
end
function GdsCreateTransition(_uFromState, _uToState, _sFromStateName, _sToStateName, _sEventName, _ARG_5_)
  if _sToStateName == nil or _sFromStateName == nil then
    return nil
  end
  local DEBUG = true
  _uFromState.GotoState = _sToStateName
  --print("GdsCreateTransition: " .. tprint(_uFromState))
  if DEBUG then
    if not tConfig.bNoCode then
      --assert(type(_uFromState) == "userdata", "GdsCreateTransition()" .. "(): _uFromState is not userdata! State = " .. _sFromStateName .. [[type:]] .. type(_uToState) .. "\n")
      --assert(type(_uToState) == "userdata", "GdsCreateTransition()" .. "(): _uToState is not userdata! State = " .. _sToStateName .. [[type:]] .. type(_uToState) .. "\n")
    end
    assert(type(_sFromStateName) == "string", "GdsCreateTransition()" .. "(): _sFromStateName is not a string!\n")
    assert(type(_sToStateName) == "string", "GdsCreateTransition()" .. "(): _sToStateName is not a string!\n")
    assert(type(_sEventName) == "string", "GdsCreateTransition()" .. "(): _sEventName is not a string!\n")
    LogSM(string.format("%s(%s, %s, \"%s\" >> \"%s\")", "GdsCreateTransition()", tostring(_uFromState), tostring(_uToState), _sFromStateName, _sToStateName))
    DotDumpTransition(_sFromStateName, _sToStateName)
  end
  if tConfig.bNoCode then
  else
    if DUMP2DISK then
      DumpCommand("uTransition = uState" .. _sFromStateName .. ":AddTransition([=[" .. CreateTransitionName(_sFromStateName, _sToStateName, _sEventName, _ARG_5_) .. "]=], uState" .. _sToStateName .. ")")
    end
    if tConfig.bEventDebug then
      _uFromState:AddTransition(CreateTransitionName(_sFromStateName, _sToStateName, _sEventName, _ARG_5_), _uToState):EnableTransitionDebug(1)
    end
  end
  if not DEBUG or not tConfig.bNoCode then
  end
  return (_uFromState:AddTransition(CreateTransitionName(_sFromStateName, _sToStateName, _sEventName, _ARG_5_), _uToState))
end

function GdsAddCondition(_ARG_0_, _ARG_1_)
    -- TODO check conditions
  --[[if DEBUG then
    if not tConfig.bNoCode then
      assert(type(_ARG_0_) == "userdata", "GdsAddCondition()" .. "(): _uTransition is not userdata!\n")
    end
    DotDumpCondition(_ARG_1_.sParamString)
  end
  if not tConfig.bNoCode then
    LogSM("Adding Condition: " .. tostring(_ARG_1_.uCondition) .. ", type: " .. type(_ARG_1_.uCondition) .. " TO " .. tostring(_ARG_0_) .. ", type: " .. type(_ARG_0_))
    _ARG_0_:AddCondition(_ARG_1_.uCondition)
    if DUMP2DISK then
      DumpCommand("uTransition:AddCondition(uCommand)")
    end
  end]]
end
function GdsAddAction(_ARG_0_, _ARG_1_)
  if DEBUG then
    if not tConfig.bNoCode then
      assert(type(_ARG_0_) == "userdata", "GdsAddAction()" .. "(): _uTransition is not userdata!\n")
    end
    DotDumpAction(_ARG_1_.sParamString)
  end
  if not tConfig.bNoCode then
    _ARG_0_:AddAction(_ARG_1_.uAction)
    if DUMP2DISK then
      DumpCommand("uTransition:AddAction(uCommand)")
    end
  end
end
function GdsNegateCondition(_ARG_0_)
  if not tConfig.bNoCode then
    _ARG_0_:SetNegated(1)
  end
  return _ARG_0_
end
function GdsAddFilter(_ARG_0_, _ARG_1_, _ARG_2_)
  if DEBUG and not tConfig.bNoCode then
    assert(type(_ARG_0_) == "userdata", "GdsAddFilter()" .. "(): _uCommand is not userdata! Type is: " .. type(_ARG_0_) .. "\n")
  end
  if not tConfig.bNoCode then
    if not _ARG_2_ then
      --LogSM("\t\tAdding Filter: " .. tostring((ExecuteCommandString(_ARG_1_))) .. " TO " .. tostring(_ARG_0_) .. " --- " .. tostring(_ARG_0_.AddFilter))
      --_ARG_0_:AddFilter((ExecuteCommandString(_ARG_1_)))
      if DUMP2DISK then
        DumpCommand("\tuCommand:AddFilter(uFilter)")
      end
    else
      --LogSM("\t\tAdding Target Filter: " .. tostring((ExecuteCommandString(_ARG_1_))) .. " TO " .. tostring(_ARG_0_) .. " --- " .. tostring(_ARG_0_.AddFilterTarget))
      --_ARG_0_:AddFilterTarget((ExecuteCommandString(_ARG_1_)))
      if DUMP2DISK then
        DumpCommand("uCommand:AddFilterTarget(uFilter)")
      end
    end
  end
end
function GdsCreateScriptGroup(_ARG_0_)
  if DEBUG then
    assert(type(_ARG_0_) == "table", "CreateScriptGroup() param is not a table!")
    assert(_ARG_0_.Name and type(_ARG_0_.Name) == "string" and string.len(_ARG_0_.Name) > 0, "CreateScriptGroup() - group has no Name!")
    assert(string.find(_ARG_0_.Name, "sg_", 1, 1) == 1 or string.find(_ARG_0_.Name, "SP_", 1, 1) == 1, "CreateScriptGroup() - group name must start with 'sg_' but was named '" .. _ARG_0_.Name .. "'")
  end
  CScriptMain.CreateScriptGroup(_ARG_0_.Name)
  for _FORV_5_, _FORV_6_ in ipairs(_ARG_0_) do
    CScriptMain.AddEntityToScriptGroup(_ARG_0_.Name, _FORV_6_)
  end
  if not tScriptGroups then
    tScriptGroups = {}
  end
  tScriptGroups[_ARG_0_.Name] = true
  return _ARG_0_.Name
end
function GdsAddToScriptGroup(_ARG_0_, _ARG_1_)
  CScriptMain.AddEntityToScriptGroup(_ARG_0_, _ARG_1_)
end
function GdsDeleteScriptGroups()
  for _FORV_3_, _FORV_4_ in pairs(tScriptGroups) do
    CScriptMain.DeleteScriptGroup(_FORV_3_)
  end
  tScriptGroups = nil
  collectgarbage()
end
function GdsRegisterSquadSpawnDeathCounter(_ARG_0_, _ARG_1_)
  if DEBUG then
    assert(type(_ARG_0_) == "table", "RegisterSquadSpawn/DeathCounter" .. " - params is not a table!")
    assert(type(_ARG_0_.Group) == "string", "RegisterSquadSpawn/DeathCounter" .. " - 'Group' parameter is not a string!")
  end
  if _ARG_0_.Group == "default" then
    _ARG_0_.Group = GetScriptTag()
  end
  CScriptMain.RegisterSquadSpawnDeathCounter(_ARG_0_.Group, _ARG_1_)
end
function RegisterSquadSpawnCounter(_ARG_0_)
  GdsRegisterSquadSpawnDeathCounter(_ARG_0_, 1)
end
function RegisterSquadDeathCounter(_ARG_0_)
  GdsRegisterSquadSpawnDeathCounter(_ARG_0_, 0)
end
function FileExists(_ARG_0_)
  if CScriptMain.FileExists(_ARG_0_) ~= 0 then
    return true
  end
  return false
end
function ShowMessage(_ARG_0_)
  if DEBUG and SCRIPTSYSTEM then
    CScriptMain.ShowMessage(_ARG_0_)
  end
end
function GdsAddReadOnlyNetworkVariable(_ARG_0_)
  CScriptMain.AddVariableNetReadOnly(_ARG_0_)
end
function GdsAddWriteableNetworkVariable(_ARG_0_)
  CScriptMain.AddVariableNetWriteable(_ARG_0_)
end
function GdsGetMapName()
  return ScriptMain:GetMapName()
end
