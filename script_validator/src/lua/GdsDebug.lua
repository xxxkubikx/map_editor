--[[if not SCRIPTCHECK then
  function print(_ARG_0_)
    if DEBUG and fGdsLogFile then
      fGdsLogFile:write(string.format("[%s] %s\n", tostring(os.date()), tostring(_ARG_0_)))
      fGdsLogFile:flush()
    end
  end
end]]
function LogSM(_ARG_0_)
  if DEBUG and tConfig.bLogCreateStateMachine then
    print("LUA::LogSM: " .. _ARG_0_)
  end
end
function MessageBox(_ARG_0_)
  if DEBUG then
    print((tostring(_ARG_0_)))
    CScriptMain.ShowMessage((tostring(_ARG_0_)))
  else
    print((tostring(_ARG_0_)))
  end
end
function MB(_ARG_0_)
  MessageBox(_ARG_0_)
end
function ViewTable(_ARG_0_)
  if DEBUG then
    WriteTableToFile(_ARG_0_, "tabledump.txt", true)
    os.execute("tabledump.txt")
    MessageBox([[
ViewTable: Script execution suspended... click OK to continue.

This is *NOT* an error!]])
  end
end
function VT(_ARG_0_)
  ViewTable(_ARG_0_)
end
--if not SCRIPTCHECK then
--  old_assert = assert
--  function assert(_ARG_0_, _ARG_1_)
--    if DEBUG and not _ARG_0_ then
--      _ARG_1_ = tostring(_ARG_1_)
--      print("ASSERTION FAILED:")
--      print(_ARG_1_)
--      if DEBUG and fGdsLogFile then
--        fGdsLogFile:flush()
--        io.close(fGdsLogFile)
--        fGdsLogFile = nil
--      end
--      error([[
--
--
--MESSAGE: ]] .. debug.traceback(_ARG_1_), 2)
--    end
--  end
--end
function DotDumpStateMachine(_ARG_0_)
  if tConfig.bDumpGif then
    tDot.sHeader = "digraph " .. _ARG_0_ .. " {\n"
  end
end
function DotDumpState(_ARG_0_)
  if tConfig.bDumpGif then
    table.insert(tDot.tStates, _ARG_0_)
    tDot.tStateIds[_ARG_0_] = table.getn(tDot.tStates)
  end
end
function DotDumpTransition(_ARG_0_, _ARG_1_)
  if tConfig.bDumpGif then
    table.insert(tDot.tTransitions, {
      sFromStateName = _ARG_0_,
      sToStateName = _ARG_1_,
      sTransitionName = _sTransitionName
    })
    if not tDot.tConditions[table.getn(tDot.tTransitions)] then
      tDot.tConditions[table.getn(tDot.tTransitions)] = {}
    end
    if not tDot.tActions[table.getn(tDot.tTransitions)] then
      tDot.tActions[table.getn(tDot.tTransitions)] = {}
    end
  end
end
function DotDumpCondition(_ARG_0_)
  if tConfig.bDumpGif then
    table.insert(tDot.tConditions[table.getn(tDot.tTransitions)], _ARG_0_ .. "\\n")
  end
end
function DotDumpAction(_ARG_0_)
  if tConfig.bDumpGif then
    table.insert(tDot.tActions[table.getn(tDot.tTransitions)], _ARG_0_ .. "\\n")
  end
end
function DotDumpToFile(_ARG_0_)
  if tConfig.bDumpGif then
    assert(io.open(_ARG_0_, "w+"))
    io.open(_ARG_0_, "w+"):write(tDot.sHeader)
    table.foreachi(tDot.tStates, function(_ARG_0_, _ARG_1_)
      _UPVALUE0_:write(string.format("%i [peripheries=3,color=red,style=filled,label=\"%s\"];\n", _ARG_0_ - 1, _ARG_1_))
      table.foreachi(tDot.tTransitions, function(_ARG_0_, _ARG_1_)
        if _ARG_1_.sFromStateName == _UPVALUE0_ and tDot.tStateIds[_ARG_1_.sFromStateName] and tDot.tStateIds[_ARG_1_.sToStateName] then
          _UPVALUE1_:write(string.format("%i -> t%i\n", tDot.tStateIds[_ARG_1_.sFromStateName] - 1, _ARG_0_))
          _UPVALUE1_:write(string.format("t%i -> %i\n", _ARG_0_, tDot.tStateIds[_ARG_1_.sToStateName] - 1))
          table.foreachi(tDot.tConditions[_ARG_0_], function(_ARG_0_, _ARG_1_)
            _UPVALUE0_ = _UPVALUE0_ .. _ARG_1_
          end)
          table.foreachi(tDot.tActions[_ARG_0_], function(_ARG_0_, _ARG_1_)
            _UPVALUE0_ = _UPVALUE0_ .. _ARG_1_
          end)
          _UPVALUE1_:write(string.format("t%i [shape=record,label=\"{%s|%s}\"];\n", _ARG_0_, "", ""))
        end
      end)
    end)
    io.open(_ARG_0_, "w+"):write(tDot.sFinish)
    io.open(_ARG_0_, "w+"):close()
  end
end
function LogUsageStatistics()
  table.sort(tActionConditionSpecs, function(_ARG_0_, _ARG_1_)
    if _ARG_0_.iCount == nil then
    elseif _ARG_1_.iCount == nil then
    else
    end
    return _ARG_0_.iCount > _ARG_1_.iCount
  end)
  print("------------------------------------------------------------------------------")
  print("Usage Statistics for Map '" .. GetPlatform() .. "' (sorted by GDS functions):")
  print("CONDITIONS:")
  table.foreachi(tActionConditionSpecs, function(_ARG_0_, _ARG_1_)
    if _ARG_1_.sType == "condition" and _ARG_1_.iCount then
      print(_ARG_1_.sName .. " (" .. _ARG_1_.sFunction .. "): " .. _ARG_1_.iCount)
      if not table.foreachi(_UPVALUE0_, function(_ARG_0_, _ARG_1_)
        if _ARG_1_.sFunction == _UPVALUE0_.sFunction then
          return _ARG_0_
        end
      end) then
        table.insert(_UPVALUE0_, {
          sFunction = _ARG_1_.sFunction,
          iCount = _ARG_1_.iCount,
          tNames = {
            _ARG_1_.sName
          }
        })
      else
        _UPVALUE0_[table.foreachi(_UPVALUE0_, function(_ARG_0_, _ARG_1_)
          if _ARG_1_.sFunction == _UPVALUE0_.sFunction then
            return _ARG_0_
          end
        end)].iCount = _UPVALUE0_[table.foreachi(_UPVALUE0_, function(_ARG_0_, _ARG_1_)
          if _ARG_1_.sFunction == _UPVALUE0_.sFunction then
            return _ARG_0_
          end
        end)].iCount + _ARG_1_.iCount
        table.insert(_UPVALUE0_[table.foreachi(_UPVALUE0_, function(_ARG_0_, _ARG_1_)
          if _ARG_1_.sFunction == _UPVALUE0_.sFunction then
            return _ARG_0_
          end
        end)].tNames, _ARG_1_.sName)
      end
    end
  end)
  print("ACTIONS:")
  table.foreachi(tActionConditionSpecs, function(_ARG_0_, _ARG_1_)
    if _ARG_1_.sType == "action" and _ARG_1_.iCount then
      print(_ARG_1_.sName .. " (Code Function: " .. _ARG_1_.sFunction .. "): " .. _ARG_1_.iCount)
      if not table.foreachi(_UPVALUE0_, function(_ARG_0_, _ARG_1_)
        if _ARG_1_.sFunction == _UPVALUE0_.sFunction then
          return _ARG_0_
        end
      end) then
        table.insert(_UPVALUE0_, {
          sFunction = _ARG_1_.sFunction,
          iCount = _ARG_1_.iCount,
          tNames = {
            _ARG_1_.sName
          }
        })
      else
        _UPVALUE0_[table.foreachi(_UPVALUE0_, function(_ARG_0_, _ARG_1_)
          if _ARG_1_.sFunction == _UPVALUE0_.sFunction then
            return _ARG_0_
          end
        end)].iCount = _UPVALUE0_[table.foreachi(_UPVALUE0_, function(_ARG_0_, _ARG_1_)
          if _ARG_1_.sFunction == _UPVALUE0_.sFunction then
            return _ARG_0_
          end
        end)].iCount + _ARG_1_.iCount
        table.insert(_UPVALUE0_[table.foreachi(_UPVALUE0_, function(_ARG_0_, _ARG_1_)
          if _ARG_1_.sFunction == _UPVALUE0_.sFunction then
            return _ARG_0_
          end
        end)].tNames, _ARG_1_.sName)
      end
    end
  end)
  table.sort({}, function(_ARG_0_, _ARG_1_)
    if _ARG_0_.iCount == nil then
    elseif _ARG_1_.iCount == nil then
    else
    end
    return _ARG_0_.iCount > _ARG_1_.iCount
  end)
  table.sort({}, function(_ARG_0_, _ARG_1_)
    if _ARG_0_.iCount == nil then
    elseif _ARG_1_.iCount == nil then
    else
    end
    return _ARG_0_.iCount > _ARG_1_.iCount
  end)
  print("------------------------------------------------------------------------------")
  print("Usage Statistics for Map '" .. GetPlatform() .. "' (sorted by Code functions):")
  print("CONDITIONS:")
  table.foreach({}, function(_ARG_0_, _ARG_1_)
    print(_ARG_1_.sFunction .. ": " .. _ARG_1_.iCount)
  end)
  print("ACTIONS:")
  table.foreach({}, function(_ARG_0_, _ARG_1_)
    print(_ARG_1_.sFunction .. ": " .. _ARG_1_.iCount)
  end)
  print("------------------------------------------------------------------------------")
end
