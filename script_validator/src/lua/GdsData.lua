sPlatform = nil
SCRIPTCHECK = false
function InitGlobalVars(_ARG_0_)
  tStateMachine = {}
  tStateMachine.tStates = {}
  assert(type(_ARG_0_) == "table", "DoScript() parameter is not a table!")
  if _ARG_0_.iX then
    _ARG_0_.fX = _ARG_0_.iX
    _ARG_0_.fY = _ARG_0_.iY
  end
  if _ARG_0_.fX then
    _ARG_0_.iX = math.floor(_ARG_0_.fX)
    _ARG_0_.iY = math.floor(_ARG_0_.fY)
  end
  if _ARG_0_.fAngle then
    _ARG_0_.iDirection = math.floor(_ARG_0_.fAngle * (180 / math.pi))
  end
  _ARG_0_.sScriptFile = string.lower(_ARG_0_.sScriptFile)
  _ARG_0_.sScriptTag = string.upper(string.gsub(_ARG_0_.sScriptTag, ".lua", ""))
  tStateMachine.tParams = _ARG_0_
  --[[
  repeat
    _ = string.find(GetScriptFile(), "/", 0 + 1, 1)
    if string.find(GetScriptFile(), "/", 0 + 1, 1) then
      _sScriptTag = string.sub(GetScriptFile(), string.find(GetScriptFile(), "/", 0 + 1, 1) + 1)
    end
  until string.find(GetScriptFile(), "/", 0 + 1, 1) == nil ]]
  _sScriptTag = GetScriptFile()
  tStateMachine.tParams.sScriptPath = string.gsub(GetScriptFile(), _sScriptTag, "")
  tStateMachine.tParams.sScriptPath = string.gsub(tStateMachine.tParams.sScriptPath, "%.lua", "")
  tStateMachine.tParams.tScriptPath = {}
  for _FORV_6_ = 1, 3 do
    table.insert(tStateMachine.tParams.tScriptPath, string.format("%s/script%i/", string.gsub(string.gsub(string.gsub(string.gsub(tStateMachine.tParams.sScriptPath, "/script3/", ""), "/script2/", ""), "/script1/", ""), "/script/", ""), _FORV_6_))
  end
  tStateMachine.tParams.sCameraPath = string.gsub(string.gsub(string.gsub(string.gsub(tStateMachine.tParams.sScriptPath, "/script3/", ""), "/script2/", ""), "/script1/", ""), "/script/", "") .. "/camera/"
  if DEBUG and tConfig.bLogCreateStateMachine and not SCRIPTCHECK then
    LogSM("DoScript() params:")
    table.foreach(tStateMachine.tParams, function(_ARG_0_, _ARG_1_)
      LogSM(string.format("\t%s = %s", _ARG_0_, tostring(_ARG_1_)))
    end)
  end
  tStateMachine.iNumEvents = 0
  tStateMachine.iNumOneTimeEvents = 0
  tStateMachine.iNumDeadEvents = 0
  tStateMachine.iNumOneTimeDeadEvents = 0
  tStateMachine.iNumToggleEvents = 0
  tStateMachine.iNumTimerEvents = 0
  tStateMachine.iNumIdleGoHomeEvents = 0
  tStateMachine.iNumPathStates = 0
  tStateMachine.iNumScriptGroups = tStateMachine.iNumScriptGroups or 0
  tStateMachine.iTempTableCount = 0
  tStateMachine._TempTables = {}
  ClearStateEventTables()
  if tConfig.bDumpGif then
    tDot = {}
    tDot.sHeader = ""
    tDot.sFinish = "}"
    tDot.tStates = {}
    tDot.tStateIds = {}
    tDot.tTransitions = {}
    tDot.tConditions = {}
    tDot.tActions = {}
  end
end
function ClearStateEventTables()
  tEvents = {}
  tDeadEvents = {}
  tDeathEvents = {}
  tRespawnEvents = {}
end
function GetNumEvents()
  if tStateMachine then
    return tStateMachine.iNumEvents
  end
end
function IncreaseNumEvents()
  if tStateMachine then
    tStateMachine.iNumEvents = tStateMachine.iNumEvents + 1
  end
end
function GetNumDeadEvents()
  if tStateMachine then
    return tStateMachine.iNumDeadEvents
  end
end
function IncreaseNumDeadEvents()
  if tStateMachine then
    tStateMachine.iNumDeadEvents = tStateMachine.iNumDeadEvents + 1
  end
end
function GetNumOneTimeEvents()
  if tStateMachine then
    return tStateMachine.iNumOneTimeEvents
  end
end
function IncreaseNumOneTimeEvents()
  if tStateMachine then
    tStateMachine.iNumOneTimeEvents = tStateMachine.iNumOneTimeEvents + 1
  end
end
function GetNumOneTimeDeadEvents()
  if tStateMachine then
    return tStateMachine.iNumOneTimeDeadEvents
  end
end
function IncreaseNumOneTimeDeadEvents()
  if tStateMachine then
    tStateMachine.iNumOneTimeDeadEvents = tStateMachine.iNumOneTimeDeadEvents + 1
  end
end
function GetNumToggleEvents()
  if tStateMachine then
    return tStateMachine.iNumToggleEvents
  end
end
function IncreaseNumToggleEvents()
  if tStateMachine then
    tStateMachine.iNumToggleEvents = tStateMachine.iNumToggleEvents + 1
  end
end
function GetNumTimerEvents()
  if tStateMachine then
    return tStateMachine.iNumTimerEvents
  end
end
function IncreaseNumTimerEvents()
  if tStateMachine then
    tStateMachine.iNumTimerEvents = tStateMachine.iNumTimerEvents + 1
  end
end
function GetNumIdleGoHomeEvents()
  if tStateMachine then
    return tStateMachine.iNumIdleGoHomeEvents
  end
end
function IncreaseNumIdleGoHomeEvents()
  if tStateMachine then
    tStateMachine.iNumIdleGoHomeEvents = tStateMachine.iNumIdleGoHomeEvents + 1
  end
end
function GetNumPathStates()
  if tStateMachine then
    return tStateMachine.iNumPathStates
  end
end
function IncreaseNumPathStates()
  if tStateMachine then
    tStateMachine.iNumPathStates = tStateMachine.iNumPathStates + 1
  end
end
function GetNumScriptGroups()
  if tStateMachine then
    return tStateMachine.iNumScriptGroups
  end
end
function IncreaseNumScriptGroups()
  if tStateMachine then
    tStateMachine.iNumScriptGroups = tStateMachine.iNumScriptGroups + 1
  end
end
function GetScriptFile()
  if tStateMachine then
    return tStateMachine.tParams.sScriptFile
  end
end
--if not SCRIPTCHECK then
  function GetCameraPath()
    if tStateMachine then
      return tStateMachine.tParams.sCameraPath
    end
  end
  function GetScriptPath(_ARG_0_)
    if tStateMachine then
      if type(_ARG_0_) == "number" then
        return tStateMachine.tParams.tScriptPath[_ARG_0_]
      else
        return tStateMachine.tParams.sScriptPath
      end
    end
  end
  function GetScriptTag()
    if tStateMachine then
      return tStateMachine.tParams.sScriptTag
    else
      return "No tag outside of game"
    end
  end
  function IsGameVersionGreaterThanSavegameVersion()
    if tStateMachine then
      if not tStateMachine.tParams.iAppVersionMajor then
        MessageBox("IsGameVersionNewerThanSavegameVersion(): not a savegame patch script!")
        return false
      else
        return GetGameVersion() > GetSavegameVersion()
      end
    end
  end
  function GetGameVersion()
    if tStateMachine then
      if not tStateMachine.tParams.iAppVersionMajor then
        MessageBox("GetGameVersion(): not a savegame patch script!")
        return -1
      else
        return tStateMachine.tParams.iAppVersionMajor * 100 + tStateMachine.tParams.iAppVersionMinor
      end
    end
  end
  function GetSavegameVersion()
    if tStateMachine then
      if not tStateMachine.tParams.iSavegameVersionMajor then
        MessageBox("GetSavegameVersion(): not a savegame patch script!")
        return -1
      else
        return tStateMachine.tParams.iSavegameVersionMajor * 100 + tStateMachine.tParams.iSavegameVersionMinor
      end
    end
  end
--end
function IsPlatformScript()
  if tStateMachine then
    return tStateMachine.tParams.iEntityType == CScriptMain.TypeWorld
  end
end
function IsPlayerScript()
  if tStateMachine then
    return tStateMachine.tParams.iEntityType == CScriptMain.TypePlayer
  end
end
function IsScriptGroupScript()
  if tStateMachine then
    return tStateMachine.tParams.iEntityType == CScriptMain.TypeScriptGroup
  end
end
function IsWarfarePatternScript()
  if tStateMachine then
    return tStateMachine.tParams.bWarfarePatternScript
  end
end
function DoNotCreateDeadStates()
  SetAsWarfarePatternScript()
end
function SetAsWarfarePatternScript()
  if tStateMachine then
    tStateMachine.tParams.bWarfarePatternScript = true
  end
end
function IsSquadScript()
  if tStateMachine then
    return tStateMachine.tParams.iEntityType == CScriptMain.TypeSquad
  end
end
function IsBuildingScript()
  if tStateMachine then
    return tStateMachine.tParams.iEntityType == CScriptMain.TypeBuilding
  end
end
function IsObjectScript()
  if tStateMachine then
    return tStateMachine.tParams.iEntityType == CScriptMain.TypeObject
  end
end
function GetDays(year, month, day)
  -- EA's version
  -- return (year - 2000) * 365 + month * 30 + day
  -- it should be for ducumentation purposes only, so changing type should not be an issue
  return tostring(year) .. "." .. tostring(month) .. "." .. tostring(day)
end
function GetNumPlayers()
  if SCRIPTSYSTEM then
    if not tStateMachine.iNumPlayers then
      tStateMachine.iNumPlayers = CScriptMain.GetNumPlayers()
    end
    return tStateMachine.iNumPlayers
  end
  return 0
end
function GetLastCardID()
  if SCRIPTSYSTEM then
    return CScriptMain.GetLastCardID()
  end
  return 0
end
function GetEntityType(_ARG_0_)
  if SCRIPTSYSTEM then
    assert(type(_ARG_0_) == "string", "GetEntityType - tag is not a string!")
    return CScriptMain.GetEntityType(_ARG_0_)
  end
  return 0
end
function InitAllPlayersGroup()
  GdsCreateScriptGroup({
    Name = "sg_AllPlayers"
  })
  GdsCreateScriptGroup({
    Name = "sg_AllPlayersAndComputer"
  })
end
function CreateScriptGroups()
  if FileExists(GetScriptPath() .. "_ScriptGroups.lua") then
    assert(type((dofile(GetScriptPath() .. "_ScriptGroups.lua"))) == "table", "Error loading " .. GetScriptPath() .. "_ScriptGroups.lua")
  elseif FileExists(GetScriptPath() .. GetScriptTag() .. "_ScriptGroups.lua") then
    assert(type((dofile(GetScriptPath() .. GetScriptTag() .. "_ScriptGroups.lua"))) == "table", "Error loading " .. GetScriptPath() .. GetScriptTag() .. "_ScriptGroups.lua")
  end
  local script_groups = dofile(GetScriptPath() .. GetScriptTag() .. "_ScriptGroups.lua")
  if type(script_groups) == "table" then
    for _FORV_5_, _FORV_6_ in pairs(script_groups) do
      if string.find(_FORV_5_, "sg_", 1, 1) ~= 1 then
        _FORV_6_.Name = "sg_" .. _FORV_5_
      else
        _FORV_6_.Name = _FORV_5_
      end
      GdsCreateScriptGroup(_FORV_6_)
    end
  end
end
function SetScriptStartStep(_ARG_0_)
  if tStateMachine then
    tStateMachine.tParams.iScriptStartStep = _ARG_0_
  end
end
function GetScriptStartStep()
  if tStateMachine then
    return tStateMachine.tParams.iScriptStartStep
  end
end
function GetScriptUpdateInterval()
  if tStateMachine then
    return tStateMachine.tParams.iScriptUpdateInterval
  end
end
function SetScriptUpdateInterval(_ARG_0_)
  if tStateMachine then
    if type(_ARG_0_) == "table" then
      tStateMachine.tParams.iScriptUpdateInterval = _ARG_0_[1]
    else
      tStateMachine.tParams.iScriptUpdateInterval = _ARG_0_
    end
  end
end
function AssertDifficulty(_ARG_0_)
  assert(type(_ARG_0_) == "number", "DifficultyIs... - difficulty parameter is not a number!")
  assert(_ARG_0_ >= DifficultyEasy and _ARG_0_ <= DifficultyVeryHard, "DifficultyIs... - difficulty parameter is out of range (smaller than easy or greater than veryhard)!")
end
function DifficultyIsEqual(_ARG_0_)
  if type(_ARG_0_) == "table" then
    _ARG_0_ = _ARG_0_[1]
  end
  if DEBUG then
    AssertDifficulty(_ARG_0_)
  end
  return MissionDifficulty == _ARG_0_
end
function DifficultyIsNotEqual(_ARG_0_)
  return not DifficultyIsEqual(_ARG_0_)
end
function DifficultyIsGreater(_ARG_0_)
  if type(_ARG_0_) == "table" then
    _ARG_0_ = _ARG_0_[1]
  end
  if DEBUG then
    AssertDifficulty(_ARG_0_)
  end
  return _ARG_0_ <= MissionDifficulty
end
function DifficultyIsLessOrEqual(_ARG_0_)
  return not DifficultyIsGreater(_ARG_0_)
end
function DifficultyIsLess(_ARG_0_)
  if type(_ARG_0_) == "table" then
    _ARG_0_ = _ARG_0_[1]
  end
  if DEBUG then
    AssertDifficulty(_ARG_0_)
  end
  return _ARG_0_ >= MissionDifficulty
end
function DifficultyIsGreaterOrEqual(_ARG_0_)
  return not DifficultyIsLess(_ARG_0_)
end
function GetDifficulty()
  return MissionDifficulty
end
function CheckNetworkVariableUniqueness(_ARG_0_, _ARG_1_)
  for _FORV_5_, _FORV_6_ in ipairs(_ARG_1_) do
    if DEBUG then
      assert(_ARG_0_[_FORV_6_] == nil, "RegisterNetworkVariables - Duplicate Variable found: '" .. tostring(_FORV_6_) .. [[
'
Network variable names must be unique across all maps!]])
    else
      assert(_ARG_0_[_FORV_6_] == nil, _UPVALUE0_)
    end
    _ARG_0_[_FORV_6_] = true
  end
end
function RegisterNetworkVariables(_ARG_0_)
  for _FORV_7_, _FORV_8_ in pairs(_ARG_0_) do
    CheckNetworkVariableUniqueness({}, _FORV_8_)
    if string.upper(_FORV_7_) == string.upper(GdsGetMapName()) then
      for _FORV_13_, _FORV_14_ in ipairs(_FORV_8_) do
        GdsAddWriteableNetworkVariable(_FORV_14_)
      end
    end
  end
end
function ProfileStart()
  if DEBUG then
    CScriptMain.ProfileStart()
  end
end
function ProfileEnd(_ARG_0_)
  if DEBUG then
    CScriptMain.ProfileEnd(_ARG_0_ or "<no message>")
  end
end
