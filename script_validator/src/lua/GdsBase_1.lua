
RegisterStates()
RegisterEvents()
RegisterActionsConditions()

tStateMachine = {}
tStateMachine.iEntityType = 6--CScriptMain_Entity_Type--CScriptMain.World
tStateMachine.sScriptFile = CScriptMain_Script_File--"_main.lua"
tStateMachine.sScriptTag = CScriptMain_Script_File--"_scripts/main.lua"
tStateMachine.sScriptPath = CScriptMain_Script_File--"_scripts/main.lua"
InitGlobalVars(tStateMachine)
InitAllPlayersGroup()
CreateScriptGroups()
--CreateSpawnGroups() -- I do not need spawn groups for now

--[[
function DoScript(_ARG_0_)
  InitGlobalVars(_ARG_0_)
  if IsPlatformScript() then
    if FileExists(GetScriptPath() .. "_GlobalVars.lua") then
      dofile(GetScriptPath() .. "_GlobalVars.lua")
    end
    InitAllPlayersGroup()
    CreateScriptGroups()
    CreateSpawnGroups()
  end
  dofile(GetScriptFile())
  CreateStateMachine()
  if tConfig.bDumpTable then
    WriteTableToFile(tStateMachine, "bf1\\map\\" .. GetScriptTag() .. "_SM_Dump.txt", true, "tStateMachine")
  end
  collectgarbage()
  if IsPlatformScript() then
    LoadScriptListScripts(_ARG_0_)
  end
end
function LoadScriptListScripts(_ARG_0_)
  if FileExists(GetScriptPath() .. "_ScriptList.lua") then
    assert(type((dofile(GetScriptPath() .. "_ScriptList.lua"))) == "table", "Error loading " .. GetScriptPath() .. "_ScriptList.lua")
  elseif FileExists(GetScriptPath() .. GetScriptTag() .. "_ScriptList.lua") then
    assert(type((dofile(GetScriptPath() .. GetScriptTag() .. "_ScriptList.lua"))) == "table", "Error loading " .. GetScriptPath() .. GetScriptTag() .. "_ScriptList.lua")
  end
  if type((dofile(GetScriptPath() .. GetScriptTag() .. "_ScriptList.lua"))) == "table" then
    table.foreachi(dofile(GetScriptPath() .. GetScriptTag() .. "_ScriptList.lua"), function(_ARG_0_, _ARG_1_)
      _UPVALUE0_.iEntityType = CScriptMain.TypeNone
      _UPVALUE0_.sScriptFile = GetScriptPath() .. string.lower(_ARG_1_)
      _UPVALUE0_.sScriptTag = string.gsub(_ARG_1_, ".lua", "")
      DoScript(_UPVALUE0_)
    end)
  end
end
function PostScriptLoad()
  ClearStateEventTables()
  tStateMachine = nil
  collectgarbage()
end ]]