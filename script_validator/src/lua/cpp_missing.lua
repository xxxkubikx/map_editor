
RegisterEnums("CScriptActionQuantorEntityRemove", {
  "RemoveByKill",
  "RemoveByVanish"
})
RegisterEnums("CScriptConditionLogical", {
  "LogicAND",
  "LogicOR"
})