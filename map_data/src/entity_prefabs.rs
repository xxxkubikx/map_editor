use egui::{Ui, Widget};
use sr_libs::utils::commands::{read_u16};
use sr_libs::utils::commands::bytes::{BytesMut};
use crate::{Fix, IssueCounter, Validate};
use crate::{Map, MapError};
use crate::buildings::Building;
use crate::monuments::Monument;
use crate::object::Object;
use crate::power_slot::PowerSlot;
use crate::squad::Squad;

#[derive(serde::Serialize)]
pub struct Prefabs {
    pub entities: Vec<PrefabEntity>,
    translate_x: f32,
    translate_y: f32,
}

#[derive(serde::Serialize)]
pub enum PrefabEntity {
    Building(Building),
    Squad(Squad),
    Object(Object),
    PowerSlot(PowerSlot),
    TokenSlot(Monument),
}

impl Prefabs {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, MapError> {
        let mut loop_buf = buf;
        let mut entities = vec![];
        while loop_buf.len() > 4 {
            if let Ok((e, buf)) = Building::read(loop_buf) {
                loop_buf = buf;
                entities.push(PrefabEntity::Building(e));
            } else if let Ok((e, buf)) = Squad::read(loop_buf) {
                loop_buf = buf;
                entities.push(PrefabEntity::Squad(e));
            } else if let Ok((e, buf)) = Object::read(loop_buf) {
                loop_buf = buf;
                entities.push(PrefabEntity::Object(e));
            } else if let Ok((e, buf)) = PowerSlot::read(loop_buf) {
                loop_buf = buf;
                entities.push(PrefabEntity::PowerSlot(e));
            } else if let Ok((e, buf)) = Monument::read(loop_buf) {
                loop_buf = buf;
                entities.push(PrefabEntity::TokenSlot(e));
            } else {
                let (entity_type, _) = read_u16(loop_buf)
                    .map_err(|e| MapError::MapFileParsing(format!("entity_type, because: {e:?}")))?;
                return Err(MapError::MapFileParsing(format!("Unexpected entity: {entity_type} (hex: 0x{entity_type:X})")))
            }
        }
        Ok(Self{entities, translate_x: 0.0, translate_y: 0.0 })
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        for e in &self.entities {
            match e {
                PrefabEntity::Building(e) => e.write(&mut buf),
                PrefabEntity::Squad(e) => e.write(&mut buf),
                PrefabEntity::Object(e) => e.write(&mut buf),
                PrefabEntity::PowerSlot(e) => e.write(&mut buf),
                PrefabEntity::TokenSlot(e) => e.write(&mut buf),
            }
        }
        buf.to_vec()
    }
}

impl Fix for Prefabs {
    fn fix(&mut self, map: &Map) {
        for b in &mut self.entities {
            b.fix(map)
        }
    }
}

impl Validate for Prefabs {
    fn validate(&self, map: &Map) -> IssueCounter {
        self.entities.iter().map(|b| b.validate(map)).sum()
    }
}

impl Fix for PrefabEntity {
    fn fix(&mut self, map: &Map) {
        match self {
            PrefabEntity::Building(e) => e.fix(map),
            PrefabEntity::Squad(e) => e.fix(map),
            PrefabEntity::Object(e) => e.fix(map),
            PrefabEntity::PowerSlot(e) => e.fix(map),
            PrefabEntity::TokenSlot(e) => e.fix(map),
        }
    }
}

impl Validate for PrefabEntity {
    fn validate(&self, map: &Map) -> IssueCounter {
        match self {
            PrefabEntity::Building(e) => e.validate(map),
            PrefabEntity::Squad(e) => e.validate(map),
            PrefabEntity::Object(e) => e.validate(map),
            PrefabEntity::PowerSlot(e) => e.validate(map),
            PrefabEntity::TokenSlot(e) => e.validate(map),
        }
    }
}

impl Prefabs {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.horizontal(|ui|{
            ui.label("x/y: ");
            egui::DragValue::new(&mut self.translate_x).ui(ui);
            egui::DragValue::new(&mut self.translate_y).ui(ui);
            if ui.button("translate").clicked() {
                for entity in self.entities.iter_mut() {
                    let positioned = match entity {
                        PrefabEntity::Building(e) => &mut e.b.b,
                        PrefabEntity::Squad(e) => &mut e.b,
                        PrefabEntity::Object(e) => &mut e.b,
                        PrefabEntity::PowerSlot(e) => &mut e.b.b,
                        PrefabEntity::TokenSlot(e) => &mut e.b.b,
                    };
                    positioned.collision_pos.x += self.translate_x;
                    positioned.collision_pos.y += self.translate_y;
                    positioned.vis_pos.x += self.translate_x;
                    positioned.vis_pos.y += self.translate_y;
                }
            }
        });
        egui::ScrollArea::vertical().show(ui, |ui|{
            for entity in self.entities.iter_mut() {
                entity.show_mut(ui);
            }
        });
    }
}

impl PrefabEntity {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        match self {
            PrefabEntity::Building(e) => e.show_mut(ui),
            PrefabEntity::Squad(e) => e.show_mut(ui),
            PrefabEntity::Object(e) => e.show_mut(ui),
            PrefabEntity::PowerSlot(e) => e.show_mut(ui),
            PrefabEntity::TokenSlot(e) => e.show_mut(ui),
        }
    }
}
