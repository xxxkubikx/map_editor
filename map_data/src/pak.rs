use std::io::Write;
use std::path::Path;
use sr_libs::utils::commands::{encode_str, ParsingError, read_ansi_string, read_u32};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};

pub fn open(path: &Path) -> Result<Vec<(String, Vec<u8>)>, PakError> {
    let data = std::fs::read(path).map_err(PakError::Read)?;
    get_files_for_pak(data.as_slice())
}

const MAGIC_HEADER: u32 = 0x14B4150;
pub fn get_files_for_pak(data: &[u8]) -> Result<Vec<(String, Vec<u8>)>, PakError> {
    use std::io::Read;
    let mut file_names = Vec::new();

    if data.len() <= 28 {
        return Ok(file_names);
    }

    let original_data = data;
    let (magic_header, data) = read_u32(data).map_err(PakError::Header)?;
    if magic_header != MAGIC_HEADER {
        return Ok(file_names);
    }
    let (offset, data) = read_u32(data).map_err(PakError::Offset)?;
    let (_size, data) = read_u32(data).map_err(PakError::Size)?;
    let (compressed_size, _) = read_u32(data).map_err(PakError::CompressedSize)?;
    let compressed = &mut &original_data[offset as usize..(offset + compressed_size) as usize];
    let mut decoder = flate2::read::ZlibDecoder::new(compressed);

    let file_structure = &mut Vec::new();
    let _ = decoder
        .read_to_end(file_structure)
        .map_err(PakError::FileStructures)?;

    let (file_count, file_structure) = read_u32(file_structure).map_err(PakError::FileCount)?;
    let mut loop_buf = file_structure;
    for _ in 0..file_count {
        let (name, buf) = read_ansi_string(loop_buf).map_err(PakError::FileName)?;
        let (file_offset, buf) = read_u32(buf).map_err(PakError::FileOffset)?;
        let (file_end, buf) = read_u32(buf).map_err(PakError::FileEnd)?;
        file_names.push((
            name,
            original_data[file_offset as usize..file_end as usize].to_vec(),
        ));
        loop_buf = buf;
    }
    Ok(file_names)
}

pub fn save(path: &Path, files: Vec<(String, Vec<u8>)>) -> std::io::Result<()> {
    let mut buf = BytesMut::new();
    buf.put_u32_le(MAGIC_HEADER);
    buf.put_u32_le(0);
    buf.put_u32_le(0);
    buf.put_u32_le(0);
    let mut file_structures = BytesMut::new();
    file_structures.put_u32_le(files.len() as u32);
    for (path, data) in files {
        encode_str(&mut file_structures, &path);
        file_structures.put_u32_le(buf.len() as u32);
        buf.put_slice(&data);
        file_structures.put_u32_le(buf.len() as u32);
    }
    let mut compressor = flate2::write::ZlibEncoder::new(Vec::new(), flate2::Compression::best());
    compressor.write_all(&file_structures)?;
    let compressed_file_structures = compressor.finish()?;
    let offset = buf.len() as u32;
    let size = file_structures.len() as u32;
    let compressed_size = compressed_file_structures.len() as u32;
    buf[4..8].copy_from_slice(&offset.to_le_bytes());
    buf[8..12].copy_from_slice(&size.to_le_bytes());
    buf[12..16].copy_from_slice(&compressed_size.to_le_bytes());
    buf.put_slice(&compressed_file_structures);
    std::fs::write(path, &buf)
}

#[derive(Debug)]
pub enum PakError {
    Read(std::io::Error),
    Header(ParsingError),
    Offset(ParsingError),
    Size(ParsingError),
    CompressedSize(ParsingError),
    FileStructures(std::io::Error),
    FileCount(ParsingError),
    FileName(ParsingError),
    FileOffset(ParsingError),
    FileEnd(ParsingError),
}
