use crate::{IssueCounter, Validate};
use crate::{Map, OtherFile};

// TODO replace both with just `OtherFile`, and move validation to the validate call
pub struct AtlasCol {
    pub error: Option<String>,
    pub col: OtherFile,
}
pub struct AtlasNor {
    pub error: Option<String>,
    pub nor: OtherFile,
}

impl AtlasCol {
    pub fn new(files: &mut Vec<OtherFile>) -> Option<Self> {
        let i = files.iter().position(|f|
            f.relative_path.eq_ignore_ascii_case("terrain\\atlas_col.dds")
            || f.relative_path.eq_ignore_ascii_case("terrain/atlas_col.dds"))?;
        let col = files.remove(i);
        let error = if &col.content[84..88] != b"DXT5" {
            Some(format!("'terrain\\atlas_col.dds' is in format {}, instead of DXT5",
                         std::str::from_utf8(&col.content[84..88]).unwrap_or("unknown")))
        } else {
            None
        };
        Some(Self { col, error })
    }
}

impl AtlasNor {
    pub fn new(files: &mut Vec<OtherFile>) -> Option<Self> {
        let i = files.iter().position(|f|
            f.relative_path.eq_ignore_ascii_case("terrain\\atlas_nor.dds")
                || f.relative_path.eq_ignore_ascii_case("terrain/atlas_nor.dds"))?;
        let nor = files.remove(i);
        let error = if &nor.content[84..88] != b"DXT5" {
            Some(format!("'terrain\\atlas_nor.dds' is in format {}, instead of DXT5",
                                std::str::from_utf8(&nor.content[84..88]).unwrap_or("unknown")))
        } else {
            None
        };
        Some(Self { nor, error })
    }
}

impl Validate for AtlasCol {
    fn validate(&self, _map: &Map) -> IssueCounter {
        if let Some(e) = &self.error {
            IssueCounter::with_bug(e.clone())
        } else {
            IssueCounter::default()
        }
    }
}

impl Validate for AtlasNor {
    fn validate(&self, _map: &Map) -> IssueCounter {
        if let Some(e) = &self.error {
            IssueCounter::with_bug(e.clone())
        } else {
            IssueCounter::default()
        }
    }
}
