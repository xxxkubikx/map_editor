use std::error::Error;
use std::path::Path;
use egui::epaint::ColorImage;
use egui::{Pos2, Ui, Widget};
use egui_snarl::{InPinId, NodeId, OutPinId, Snarl};
use egui_snarl::ui::SnarlStyle;
use image::{EncodableLayout, RgbImage};
use modifiers::{GrayImage, ModifierPipeline};
use crate::terrain::Heights;
use crate::retained_image::RetainedImage;

mod modifiers;
mod heights;

const MAX_HEIGH : f32 = 255.;

pub struct HeightMapGenerator {
    initial: HeightMapChoice,
    modifier_pipeline: ModifierPipeline,
    snarl: Snarl<modifiers::Node>,
    style: SnarlStyle,
}

impl Default for HeightMapGenerator {
    fn default() -> Self {
        let mut snarl = Snarl::new();
        let source = modifiers::Node::Source;
        let id = snarl.insert_node(Pos2::new(-300., 0.), source);
        assert_eq!(NodeId(0), id);
        let out = modifiers::Node::Out;
        let id = snarl.insert_node(Pos2::new(300., 0.), out);
        assert_eq!(NodeId(1), id);
        snarl.connect(OutPinId{ node: NodeId(0), output: 0 }, InPinId{ node: NodeId(1), input: 0 });
        let mut modifier_pipeline = ModifierPipeline::new(256, 256);
        modifier_pipeline.set_flat_base(50.);
        Self {
            initial: HeightMapChoice::Flat { width: 256, height: 256, level: 50., safe_sizes: true, },
            modifier_pipeline,
            snarl,
            style: SnarlStyle{ downscale_wire_frame: Some(true), .. SnarlStyle::default()},
        }
    }
}


enum HeightMapChoice {
    Flat{width: u16, height: u16, level: f32, safe_sizes: bool},
    Image{rgb: RetainedImage, gray: GrayImage},
    Dialog(egui_file::FileDialog)
}

const SAFE_SIZES : &[(u16, &str)] = &[(128, "128"), (256, "256"), (512, "512"), (1024, "1024"), (2048, "2048")];
// Up to 16384 will work in theory, but performance should be a concern.
// Or maybe even 32768, but that is risking an issue that the size is somewhere handled as signed value (in BattleForge)
const MAX_MAP_SIZE : u16 = 8192;

impl HeightMapGenerator {
    pub fn width(&self) -> u32 {
        match &self.initial {
            HeightMapChoice::Flat { width, ..} => *width as u32,
            HeightMapChoice::Image { gray, .. } => gray.width(),
            HeightMapChoice::Dialog(_) => {
                unreachable!()
            }
        }
    }
    pub fn height(&self) -> u32 {
        match &self.initial {
            HeightMapChoice::Flat { height, ..} => *height as u32,
            HeightMapChoice::Image { gray, .. } => gray.height(),
            HeightMapChoice::Dialog(_) => {
                unreachable!()
            }
        }
    }
    pub fn heights(&self) -> Heights {
        let heights = heights::compute(self.width() as usize, self.height() as usize, self.modifier_pipeline.final_heights());
        Heights{
            unk: Some(1),
            iy: self.height(),
            ix: self.width(),
            fy: self.height() as f32 * 1.4,
            fx: self.width() as f32 * 1.4,
            heights,
        }
    }
    pub fn show(&mut self, ui: &mut Ui) {
        match &mut self.initial {
            HeightMapChoice::Image { rgb, gray } => {
                ui.label(format!("width: {}", gray.width()));
                ui.label(format!("height: {}", gray.height()));
                ui.label("height map").on_hover_ui(|ui|{
                    rgb.show_scaled(ui, 1.0);
                });
                if ui.button("remove height map").clicked() {
                    self.modifier_pipeline = ModifierPipeline::new(256, 256);
                    self.initial = HeightMapChoice::Flat { width: 256, height: 256, level: 50., safe_sizes: true };
                }
            }
            HeightMapChoice::Dialog(d) => {
                d.show(ui.ctx());
                if d.selected() {
                    fn load(path: &Path) -> Result<(RgbImage, GrayImage), Box<dyn Error>> {
                        let img = std::fs::read(path)?;
                        let img = image::load_from_memory(&img)?;
                        let mut gray = img.to_luma32f();
                        for p in gray.iter_mut() {
                            *p *= MAX_HEIGH;
                        }
                        Ok((img.to_rgb8(), gray))
                    }
                    #[cfg(not(target_arch = "wasm32"))]
                        let img = load(d.path().unwrap());
                    match img {
                        Ok((rgb, gray)) => {
                            if rgb.width() > MAX_MAP_SIZE as u32 || rgb.height() > MAX_MAP_SIZE as u32 {
                                log::error!("Invalid image size: {}x{}", rgb.width(), rgb.height());
                                return;
                            }
                            let rgb = ColorImage::from_rgb(
                                [rgb.width() as usize, rgb.height() as usize],
                                rgb.as_bytes());
                            self.modifier_pipeline = ModifierPipeline::from_image(gray.clone());
                            let rgb = RetainedImage::from_color_image("new height map", rgb);
                            self.initial = HeightMapChoice::Image{rgb, gray};
                        }
                        Err(e) => {
                            log::error!("Loading height map failed: {e}");
                        }
                    }
                }
            }
            HeightMapChoice::Flat{width, height, level, safe_sizes} => {
                ui.checkbox(safe_sizes, "safe sizes")
                    .on_hover_text("Using unsafe sizes, might result in this editor crashing, because not all parts support them.
EA's editot on the other hand seems to handle them OK (if it does not crash for other reasons).");
                if *safe_sizes {
                    egui::containers::ComboBox::new("width", "width")
                        .selected_text(SAFE_SIZES.iter().find(|(s, _)| s == width).map(|&(_,t)|t).unwrap_or("unsafe"))
                        .show_ui(ui, |ui|{
                            for safe in SAFE_SIZES.iter() {
                                if ui.selectable_value(width, safe.0, safe.1).changed() {
                                    self.modifier_pipeline = ModifierPipeline::new(*width, *height);
                                }
                            }
                        });
                    egui::containers::ComboBox::new("height", "height")
                        .selected_text(SAFE_SIZES.iter().find(|(s, _)| s == height).map(|&(_,t)|t).unwrap_or("unsafe"))
                        .show_ui(ui, |ui|{
                            for safe in SAFE_SIZES.iter() {
                                if ui.selectable_value(height, safe.0, safe.1).changed() {
                                    self.modifier_pipeline = ModifierPipeline::new(*width, *height);
                                }
                            }
                        });
                } else {
                    ui.horizontal(|ui|{
                        ui.label("width");
                        if egui::DragValue::new(width).clamp_range(64..=MAX_MAP_SIZE).ui(ui).changed() {
                            self.modifier_pipeline = ModifierPipeline::new(*width, *height);
                        }
                    });
                    ui.horizontal(|ui|{
                        ui.label("height");
                        if egui::DragValue::new(height).clamp_range(64..=MAX_MAP_SIZE).ui(ui).changed() {
                            self.modifier_pipeline = ModifierPipeline::new(*width, *height);
                        }
                    });
                }
                ui.horizontal(|ui|{
                    ui.label("base level");
                    let max = if *safe_sizes { 96. } else { MAX_HEIGH };
                    if egui::DragValue::new(level).clamp_range(1.0..=max).ui(ui).changed() {
                        self.modifier_pipeline.set_flat_base(*level)
                    }
                });
                if ui.button("load height map").clicked() {
                    let mut d = egui_file::FileDialog::open_file(None);
                    d.open();
                    self.initial = HeightMapChoice::Dialog(d);
                }
            },
        }

        /*
        egui::SidePanel::left("style").show(ui.ctx(), |ui| {
            egui::ScrollArea::vertical().show(ui, |ui| {
                egui_probe::Probe::new("Snarl style", &mut self.style).show(ui);
            });
        });// */
        egui::CentralPanel::default().show(ui.ctx(), |ui| {
            self.snarl.show(&mut self.modifier_pipeline, &self.style, egui::Id::new("snarl"), ui);
        });
    }
}