mod height_map;

use std::cell::RefCell;
use std::collections::BTreeMap;

use crate::{TEAM_MANAGER_TABLE_ID, ATMO_ZONES_TABLE_ID, CLIFF_TABLE_ID, EFFECT_ZONES_TABLE_ID, F8003_TABLE_ID, FilesEnum, LIGHTINGS_TABLE_ID, Map, MapTable, OtherFile, PKIT_START_INFO_TABLE_ID, PLAYER_KITS_TABLE_ID, TEAM_CONFIGURATIONS_TABLE_ID, TERRAIN_TABLE_ID, TEXTURE_LAYERS_TABLE_ID, WAYPOINT_GRID_MANAGER_TABLE_ID};
use crate::_8003::Data8003;
use crate::atmo_zones::AtmoZones;
use crate::cliff::Cliffs;
use crate::effect_zones::EffectZones;
use crate::entity::{Entity, IdEntity};
use crate::generator::empty::height_map::HeightMapGenerator;
use crate::pkit_info::PkitInfo;
use crate::player_kit::{PlayerKit, PlayerKits};
use crate::team_config::{PlayerType, PlayerV5, Relation, RelationType, TeamConfiguration, TeamConfigurations, TeamIdentifiers, TeamIdentifierV6, TeamRelations, TeamSetup, TeamSetupV5, TeamV5};
use crate::terrain::{Blocking, BlockingFlags, CGdCliffs, CGdWaterMap, CliffMask, MapVersion, Terrain, TerrainData, TextureMaskData, TextureMasks};
use crate::texture_layer::{TextureLayer, TextureLayers};

pub struct EmptyMap{
    height_map: HeightMapGenerator,
    pub done: bool,
}



impl Default for EmptyMap {
    fn default() -> Self {
        Self{
            height_map: HeightMapGenerator::default(),
            done: false,
        }
    }
}


impl EmptyMap {
    pub fn show(&mut self, ctx: &egui::Context) {
        egui::Window::new("Create empty map").show(ctx, |ui|{
            self.height_map.show(ui);

            if ui.button("create").clicked() {
                self.done = true;
            }
        });
    }
    pub fn create(&self) -> Map {

        let mut map = BTreeMap::new();
        map.insert(10000, {
            let data = vec![2,0,0,0,0,3,0,0,0,0];
            (RefCell::new(MapTable::Unknown(data.clone())), data, 1, 1)
        });
        map.insert(PKIT_START_INFO_TABLE_ID, {
            let table = MapTable::PkitInfo(PkitInfo{
                unknown: 1,
                maybe_strings: [String::new(), String::new(), String::new(), String::new(), String::new()],
                y: self.height_map.height() as u16,
                x: self.height_map.width() as u16,
                another_0: 0,
                something: vec![
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x33, 0x33, 0xB3, 0x43, 0x33, 0x33, 0xB3, 0x43, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x33, 0x33, 0xB3, 0x43, 0x33, 0x33, 0xB3, 0x43 ],
                pkit_starting_points: vec![
                    /*PkitStart{
                        name: "pk_kit1".to_string(),
                        y: 10.0,
                        x: 10.0,
                    }*/
                ],
                more_zeros: [0, 0],
            });
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 2)
        });
        map.insert(TEAM_CONFIGURATIONS_TABLE_ID, {
            let table = MapTable::TeamConfigurations(TeamConfigurations{
                configurations: vec![
                    TeamConfiguration{
                        setup: TeamSetup::V5(TeamSetupV5{
                            setup: "setup".to_string(),
                            teams: vec![
                                TeamV5{
                                    team_name: "tm_Neutral".to_string(),
                                    team_id: 1,
                                    players: vec![],
                                },
                                TeamV5{
                                    team_name: "tm_Animal".to_string(),
                                    team_id: 2,
                                    players: vec![],
                                },
                                TeamV5{
                                    team_name: "tm_Creep".to_string(),
                                    team_id: 3,
                                    players: vec![],
                                },
                                TeamV5{
                                    team_name: "tm_Team1".to_string(),
                                    team_id: 4,
                                    players: vec![
                                        PlayerV5{
                                            player_type: PlayerType::Human,
                                            player_tag: "pl_Player1".to_string(),
                                            kit_tag: "pk_kit1".to_string(),
                                        }
                                    ],
                                },
                                TeamV5{
                                    team_name: "tm_Enemy1".to_string(),
                                    team_id: 5,
                                    players: vec![
                                        PlayerV5{
                                            player_type: PlayerType::Monster,
                                            player_tag: "pl_Enemy1".to_string(),
                                            kit_tag: "pk_enemy1".to_string(),
                                        }
                                    ],
                                },
                            ],
                        }),
                        relation: TeamRelations{
                            team_relations: BTreeMap::from_iter([
                                (Relation::new(1, 1), RelationType::Friends),
                                (Relation::new(1, 2), RelationType::Neutral),
                                (Relation::new(1, 3), RelationType::Neutral),
                                (Relation::new(1, 4), RelationType::Neutral),
                                (Relation::new(1, 5), RelationType::Neutral),

                                (Relation::new(2, 1), RelationType::Neutral),
                                (Relation::new(2, 2), RelationType::Friends),
                                (Relation::new(2, 3), RelationType::Neutral),
                                (Relation::new(2, 4), RelationType::Neutral),
                                (Relation::new(2, 5), RelationType::Neutral),

                                (Relation::new(3, 1), RelationType::Neutral),
                                (Relation::new(3, 2), RelationType::Neutral),
                                (Relation::new(3, 3), RelationType::Friends),
                                (Relation::new(3, 4), RelationType::Neutral),
                                (Relation::new(3, 5), RelationType::Neutral),

                                (Relation::new(4, 1), RelationType::Neutral),
                                (Relation::new(4, 2), RelationType::Neutral),
                                (Relation::new(4, 3), RelationType::Neutral),
                                (Relation::new(4, 4), RelationType::Friends),
                                (Relation::new(4, 5), RelationType::Enemies),

                                (Relation::new(5, 1), RelationType::Neutral),
                                (Relation::new(5, 2), RelationType::Neutral),
                                (Relation::new(5, 3), RelationType::Neutral),
                                (Relation::new(5, 4), RelationType::Enemies),
                                (Relation::new(5, 5), RelationType::Friends),
                            ].into_iter()),
                            team_identifiers: TeamIdentifiers::V6(vec![
                                TeamIdentifierV6{
                                    team_identifier_name: "TM_ANIMAL".to_string(),
                                    team_id: 2,
                                    unknown_int: 0,
                                    fow: 0,
                                },
                                TeamIdentifierV6{
                                    team_identifier_name: "TM_CREEP".to_string(),
                                    team_id: 3,
                                    unknown_int: 0,
                                    fow: 0,
                                },
                                TeamIdentifierV6{
                                    team_identifier_name: "TM_ENEMY1".to_string(),
                                    team_id: 5,
                                    unknown_int: 0,
                                    fow: 0,
                                },
                                TeamIdentifierV6{
                                    team_identifier_name: "TM_NEUTRAL".to_string(),
                                    team_id: 1,
                                    unknown_int: 0,
                                    fow: 0,
                                },
                                TeamIdentifierV6{
                                    team_identifier_name: "TM_TEAM1".to_string(),
                                    team_id: 4,
                                    unknown_int: 0,
                                    fow: 0,
                                },
                            ]),
                        },
                    }
                ],
            });
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 3)
        });
        map.insert(F8003_TABLE_ID, {
            let table = MapTable::F8003(Data8003{
                something: vec![],
            });
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 1)
        });
        map.insert(CLIFF_TABLE_ID, {
            let table = MapTable::Cliffs(Cliffs(vec![]));
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 2)
        });
        map.insert(TEXTURE_LAYERS_TABLE_ID, {
            let table = MapTable::TextureLayers(TextureLayers(vec![
                TextureLayer{
                    name: "Layer".to_string(),
                    enabled: 1,
                }
            ]));
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 2)
        });
        map.insert(WAYPOINT_GRID_MANAGER_TABLE_ID, {
            let data = vec![1,0,0,0,0,0,0,0,0];
            (RefCell::new(MapTable::Unknown(data.clone())), data, 1, 1)
        });/*
        map.insert(MONUMENTS_TABLE_ID, {
            let table = MapTable::Monuments(Monuments(vec![]), 0);
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 1)
        });
        map.insert(POWER_SLOTS_TABLE_ID, {
            let table = MapTable::PowerSlots(PowerSlots(vec![]), 0);
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 1)
        });
        map.insert(STARTING_POINTS_TABLE_ID, {
            let table = MapTable::StartingPoints(StartingPoints(vec![]), 0);
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 1)
        });
        map.insert(BARRIER_SETS_TABLE_ID, {
            let table = MapTable::BarrierSets(BarrierSets(vec![]), 0);
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 1)
        });
        map.insert(BARRIERS_TABLE_ID, {
            let table = MapTable::Barriers(Barriers(vec![]), 0);
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 1)
        });*/
        map.insert(LIGHTINGS_TABLE_ID, {
            let data = vec![1, 0, 0];
            let table = MapTable::Unknown(data.clone());
            (RefCell::new(table), data, 1, 1)
        });
        map.insert(TEAM_MANAGER_TABLE_ID, {
            let data = vec![6, 0, 0, 0, 0];
            let table = MapTable::Unknown(data.clone());
            (RefCell::new(table), data, 1, 1)
        });
        map.insert(EFFECT_ZONES_TABLE_ID, {
            let table = MapTable::EffectZones(EffectZones{
                b: IdEntity {
                    b: Entity {},
                    often_one: 3,
                    params: vec![],
                },
                unknown: 0,
                _height: self.height_map.height() as u16,
                _width: self.height_map.width() as u16,
                height: self.height_map.height(),
                width: self.height_map.width(),
                zones: vec![0; self.height_map.width() as usize * self.height_map.height() as usize],
            });
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 1)
        });
        map.insert(ATMO_ZONES_TABLE_ID, {
            let table = MapTable::AtmoZones(AtmoZones{
                b: IdEntity {
                    b: Entity {},
                    often_one: 3,
                    params: vec![],
                },
                unknown_byte: 0,
                zones: vec![],
                unk_u16_1: self.height_map.height() as u16,
                unk_u16_2: self.height_map.width() as u16,
                y: self.height_map.height(),
                x: self.height_map.width(),
                data: vec![0; self.height_map.width() as usize * self.height_map.height() as usize],
            });
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 1)
        });/*
        map.insert(1007, {
            let table = MapTable::Unknown(vec![]);
            (RefCell::new(table), vec![], 1, 2)
        });
        map.insert(BUILDINGS_TABLE_ID, {
            let table = MapTable::Buildings(Buildings(vec![]), 0);
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 3)
        });
        map.insert(SQUADS_TABLE_ID, {
            let table = MapTable::Squads(Squads(vec![]), 0);
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 2)
        });*/
        map.insert(PLAYER_KITS_TABLE_ID, {
            let table = MapTable::PlayerKits(PlayerKits(vec![
                PlayerKit{
                    b: Entity {},
                    name: "pk_kit1".to_string(),
                    enum_affecting_what_fields_follow: 4,
                    starting_power: 200,
                    starting_void_power: 0,
                },
                PlayerKit{
                    b: Entity {},
                    name: "pk_enemy1".to_string(),
                    enum_affecting_what_fields_follow: 4,
                    starting_power: 200,
                    starting_void_power: 0,
                },
            ]), 0);
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 2)
        });/*
        map.insert(OBJECTS_TABLE_ID, {
            let table = MapTable::Objects(Objects(vec![]), 0);
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 2)
        });*/
        map.insert(TERRAIN_TABLE_ID, {
            let heights_map = self.height_map.heights();
            let aviatic_heights_map = heights_map.clone();
            let terrain = TerrainData{
                category: 3,
                map_layer: 1,
                aspects: vec![],
                terrain: MapVersion::Type23{
                    y: heights_map.iy as u16,
                    x: heights_map.ix as u16,
                    blocking: Blocking{
                        y: heights_map.iy * 2,
                        x: heights_map.ix * 2,
                        blocking: vec![
                            BlockingFlags::empty();
                            heights_map.iy as usize * 2 * heights_map.ix as usize * 2
                        ],
                    },
                    texture_masks: TextureMasks{
                        atlas_col: "C:/Users/xxxku/Documents/BattleForge/editor_map/empty/terrain/atlas_col.dds".to_string(),
                        atlas_nor: "C:/Users/xxxku/Documents/BattleForge/editor_map/empty/terrain/atlas_nor.dds".to_string(),
                        f0_1: 1.0,
                        f1_0: 0.0,
                        f2_1: 1.0,
                        f3_0_75: 0.75,
                        masks: vec![
                            TextureMaskData{
                                terrain_textures_default_col: "bf1/gfx/terrain/terrain_textures/terrain_textures_default_col.tga".to_string(),
                                terrain_textures_default_ble: "".to_string(),
                                terrain_textures_default_nor: "bf1/gfx/terrain/terrain_textures/terrain_textures_default_nor.tga".to_string(),
                                terrain_textures_default_spec: "bf1/gfx/terrain/terrain_textures/terrain_textures_default_spec.tga".to_string(),
                                y: heights_map.iy,
                                x: heights_map.ix,
                                data: vec![255; heights_map.iy as usize * heights_map.ix as usize],
                            },
                        ],
                        unks1: vec![],
                    },
                    water: CGdWaterMap{
                        y: heights_map.iy,
                        x: heights_map.ix,
                        data: vec![0; heights_map.iy as usize * heights_map.ix as usize],
                        unk1s: vec![],
                    },
                    cliffs: {
                        let size = heights_map.iy as usize * ((heights_map.ix as usize + 31) >> 5);
                        CGdCliffs{
                            bits_0: CliffMask {
                                y: heights_map.iy,
                                x: heights_map.ix,
                                data: vec![0; size],
                            },
                            bits_1: CliffMask {
                                y: heights_map.iy,
                                x: heights_map.ix,
                                data: vec![0; size],
                            },
                        }
                    },
                    heights_map,
                    aviatic_heights_map,
                }
            };
            let gui_data = crate::terrain::GuiData::new(&terrain);
            let table = MapTable::Terrain(Terrain{
                terrain,
                gui_data,
            });
            let data = table.to_bytes();
            (RefCell::new(table), data, 1, 2)
        });
        let map = Map{
            map,
            entity_tags: Default::default(),
            map_name: "TODO name".to_string(),
            files: vec![
                RefCell::new(FilesEnum::Descriptions(OtherFile{
                    relative_path: "ui\\DESCRIPTION.xml".to_string(),
                    content: DESCRIPTION.as_bytes().to_vec(),
                })),
            ],
            map_compression: flate2::Compression::none(),
        };

        map
    }
}


const DESCRIPTION: &str = r#"<mapdescription author="TODO your nickname" public="0">
    <DESCRIPTION language="en">
        <mapname>TODO EN name</mapname>
        <DESCRIPTION>TODO EN DESCRIPTION</DESCRIPTION>
    </DESCRIPTION>
    <DESCRIPTION language="de">
        <mapname>TODO DE name</mapname>
        <DESCRIPTION>TODO DE DESCRIPTION</DESCRIPTION>
    </DESCRIPTION>
    <DESCRIPTION language="fr">
        <mapname>TODO FR name</mapname>
        <DESCRIPTION>TODO FR DESCRIPTION</DESCRIPTION>
    </DESCRIPTION>
    <DESCRIPTION language="ru">
        <mapname>TODO RU name</mapname>
        <DESCRIPTION>TODO TU DESCRIPTION</DESCRIPTION>
    </DESCRIPTION>
</mapdescription>
"#;