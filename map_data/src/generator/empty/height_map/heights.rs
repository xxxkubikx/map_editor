
pub fn compute(width: usize, height: usize, data: &[f32]) -> Vec<f32> {
    let data_size = {
        if height <= 1 || width <= 1 {
            unreachable!()
        }
        let mut y = height.next_power_of_two();
        let mut x = width.next_power_of_two();
        let mut data_size = x * y;
        while y != 0 || x != 0 {
            data_size += y.max(1) * x.max(1);
            y >>= 1;
            x >>= 1;
        }
        data_size
    };
    let mut heights = vec![50.0; data_size];
    heights[..data.len()].copy_from_slice(data);

    let mut offset = height * width;
    for i in 0..offset {
        heights[i + offset] = heights[i];
    }
    for scales in [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192].windows(2) {
        let scale = scales[0];
        let next_scale = scales[1];
        let size_y = height / scale;
        let size_x = width / scale;
        let next_size_y = height / next_scale;
        let next_size_x = width / next_scale;
        if next_size_y == 0 && next_size_x == 0 {
            break;
        }
        let y_add = if next_size_y == 0 { 0 } else { size_x };
        let x_add = if next_size_x == 0 { 0 } else { 1 };
        let size_y = size_y.max(1);
        let size_x = size_x.max(1);
        let next_size_x = next_size_x.max(1);
        let next_offset = offset + size_y * size_x;
        for y in (0..size_y).step_by(2) {
            for x in (0..size_x).step_by(2) {
                let average = [
                    heights[(y * size_x) + x + offset],
                    heights[(y * size_x) + x + x_add + offset],
                    heights[(y * size_x) + y_add + x + offset],
                    heights[(y * size_x) + y_add + x + x_add + offset],
                ].into_iter().sum::<f32>() / 4.;
                let i = (y / 2 * next_size_x) + x / 2 + next_offset;
                heights[i] = average;
            }
        }
        offset += size_y * size_x;
    }

    heights
}