use std::collections::HashMap;
use std::fmt::Display;
use std::ops::{Add, Div, Mul, Neg, Sub};
use std::str::FromStr;
use egui::Ui;
use egui_snarl::{InPin, InPinId, Snarl};
use crate::generator::empty::height_map::modifiers::Node;

#[derive(Copy, Clone, Eq, PartialEq)]
pub enum NumberTypes {
    F32, U8,
}

#[derive(Copy, Clone)]
pub enum NumberValue {
    F32(f32),
    U8(u8),
}


pub fn expression_string_in_ui(pin: &InPin, ui: &mut Ui, snarl: &mut Snarl<Node>) {
    let input_changed = match &*pin.remotes {
        [] => {
            let input = snarl[pin.id.node].string_in();
            let r = egui::TextEdit::singleline(input)
                .clip_text(false)
                .desired_width(0.0)
                .margin(ui.spacing().item_spacing)
                .show(ui)
                .response;

            r.changed()
        }
        [remote] => {
            let new_string = snarl[remote.node].string_out().to_owned();

            egui::TextEdit::singleline(&mut &*new_string)
                .clip_text(false)
                .desired_width(0.0)
                .margin(ui.spacing().item_spacing)
                .show(ui);

            let input = snarl[pin.id.node].string_in();
            if new_string != *input {
                *input = new_string;
                true
            } else {
                false
            }
        }
        _ => unreachable!("Expr pins has only one wire"),
    };

    if input_changed {
        let expr_node = snarl[pin.id.node].expr_node();

        match syn::parse_str(&expr_node.text) {
            Ok(expr) => {
                expr_node.expr = expr;

                let values = Iterator::zip(
                    expr_node.bindings.iter().map(String::clone),
                    expr_node.values.iter().copied(),
                )
                    .collect::<HashMap<String, f32>>();

                let mut new_bindings = Vec::new();
                expr_node.expr.extend_bindings(&mut new_bindings);

                let old_bindings =
                    std::mem::replace(&mut expr_node.bindings, new_bindings.clone());

                let new_values = new_bindings
                    .iter()
                    .map(|name| values.get(&**name).copied().unwrap_or(0.0))
                    .collect::<Vec<_>>();

                expr_node.values = new_values;

                let old_inputs = (0..old_bindings.len())
                    .map(|idx| {
                        snarl.in_pin(InPinId {
                            node: pin.id.node,
                            input: idx,
                        })
                    })
                    .collect::<Vec<_>>();

                for (idx, name) in old_bindings.iter().enumerate() {
                    let new_idx =
                        new_bindings.iter().position(|new_name| *new_name == *name);

                    match new_idx {
                        None => {
                            snarl.drop_inputs(old_inputs[idx].id);
                        }
                        Some(new_idx) if new_idx != idx => {
                            let new_in_pin = InPinId {
                                node: pin.id.node,
                                input: new_idx,
                            };
                            for &remote in &old_inputs[idx].remotes {
                                snarl.disconnect(remote, old_inputs[idx].id);
                                snarl.connect(remote, new_in_pin);
                            }
                        }
                        _ => {}
                    }
                }
            }
            Err(_) => {}
        }
    }
}


#[derive(Clone, serde::Serialize, serde::Deserialize)]
pub struct ExprNodeF32 {
    pub text: String,
    pub bindings: Vec<String>,
    pub values: Vec<f32>,
    pub expr: Expr<f32>,
}

impl ExprNodeF32 {
    pub fn new() -> Self {
        ExprNodeF32 {
            text: "0".to_string(),
            bindings: Vec::new(),
            values: Vec::new(),
            expr: Expr::Val(0.0),
        }
    }

    pub fn eval(&self) -> f32 {
        self.expr.eval(&self.bindings, &self.values)
    }
}

#[derive(Clone, Copy, serde::Serialize, serde::Deserialize)]
pub enum UnOp {
    Pos,
    Neg,
}

#[derive(Clone, Copy, serde::Serialize, serde::Deserialize)]
pub enum BinOp {
    Add,
    Sub,
    Mul,
    Div,
}

#[derive(Clone, serde::Serialize, serde::Deserialize)]
pub enum Expr<T> {
    Var(String),
    Val(T),
    UnOp {
        op: UnOp,
        expr: Box<Expr<T>>,
    },
    BinOp {
        lhs: Box<Expr<T>>,
        op: BinOp,
        rhs: Box<Expr<T>>,
    },
}

impl<T> Expr<T>
    where T: Copy
    + Neg<Output = T> + Add<Output = T> + Sub<Output = T>
    + Mul<Output = T> + Div<Output = T> + FromStr, T::Err: Display {
    pub fn eval(&self, bindings: &[String], args: &[T]) -> T {
        let binding_index =
            |name: &str| bindings.iter().position(|binding| binding == name).unwrap();

        match self {
            Expr::Var(ref name) => args[binding_index(name)],
            Expr::Val(value) => *value,
            Expr::UnOp { op, ref expr } => match op {
                UnOp::Pos => expr.eval(bindings, args),
                UnOp::Neg => -expr.eval(bindings, args),
            },
            Expr::BinOp {
                ref lhs,
                op,
                ref rhs,
            } => match op {
                BinOp::Add => lhs.eval(bindings, args) + rhs.eval(bindings, args),
                BinOp::Sub => lhs.eval(bindings, args) - rhs.eval(bindings, args),
                BinOp::Mul => lhs.eval(bindings, args) * rhs.eval(bindings, args),
                BinOp::Div => lhs.eval(bindings, args) / rhs.eval(bindings, args),
            },
        }
    }

    pub fn extend_bindings(&self, bindings: &mut Vec<String>) {
        match self {
            Expr::Var(name) => {
                if !bindings.contains(name) {
                    bindings.push(name.clone());
                }
            }
            Expr::Val(_) => {}
            Expr::UnOp { expr, .. } => {
                expr.extend_bindings(bindings);
            }
            Expr::BinOp { lhs, rhs, .. } => {
                lhs.extend_bindings(bindings);
                rhs.extend_bindings(bindings);
            }
        }
    }
}

impl syn::parse::Parse for UnOp {
    fn parse(input: syn::parse::ParseStream<'_>) -> syn::Result<Self> {
        let lookahead = input.lookahead1();
        if lookahead.peek(syn::Token![+]) {
            input.parse::<syn::Token![+]>()?;
            Ok(UnOp::Pos)
        } else if lookahead.peek(syn::Token![-]) {
            input.parse::<syn::Token![-]>()?;
            Ok(UnOp::Neg)
        } else {
            Err(lookahead.error())
        }
    }
}

impl syn::parse::Parse for BinOp {
    fn parse(input: syn::parse::ParseStream<'_>) -> syn::Result<Self> {
        let lookahead = input.lookahead1();
        if lookahead.peek(syn::Token![+]) {
            input.parse::<syn::Token![+]>()?;
            Ok(BinOp::Add)
        } else if lookahead.peek(syn::Token![-]) {
            input.parse::<syn::Token![-]>()?;
            Ok(BinOp::Sub)
        } else if lookahead.peek(syn::Token![*]) {
            input.parse::<syn::Token![*]>()?;
            Ok(BinOp::Mul)
        } else if lookahead.peek(syn::Token![/]) {
            input.parse::<syn::Token![/]>()?;
            Ok(BinOp::Div)
        } else {
            Err(lookahead.error())
        }
    }
}

impl<T> syn::parse::Parse for Expr<T>
    where T: FromStr, T::Err: Display {
    fn parse(input: syn::parse::ParseStream<'_>) -> syn::Result<Self> {
        let lookahead = input.lookahead1();

        let lhs;
        if lookahead.peek(syn::token::Paren) {
            let content;
            syn::parenthesized!(content in input);
            let expr = content.parse::<Expr<T>>()?;
            if input.is_empty() {
                return Ok(expr);
            }
            lhs = expr;
            // } else if lookahead.peek(syn::LitFloat) {
            //     let lit = input.parse::<syn::LitFloat>()?;
            //     let value = lit.base10_parse::<f32>()?;
            //     let expr = Expr::Val(value);
            //     if input.is_empty() {
            //         return Ok(expr);
            //     }
            //     lhs = expr;
        } else if lookahead.peek(syn::LitInt) {
            let lit = input.parse::<syn::LitInt>()?;
            let value = lit.base10_parse::<T>()?;
            let expr = Expr::Val(value);
            if input.is_empty() {
                return Ok(expr);
            }
            lhs = expr;
        } else if lookahead.peek(syn::Ident) {
            let ident = input.parse::<syn::Ident>()?;
            let expr = Expr::Var(ident.to_string());
            if input.is_empty() {
                return Ok(expr);
            }
            lhs = expr;
        } else {
            let unop = input.parse::<UnOp>()?;

            return Self::parse_with_unop(unop, input);
        }

        let binop = input.parse::<BinOp>()?;

        Self::parse_binop(Box::new(lhs), binop, input)
    }
}

impl<T> Expr<T>
where T: FromStr, T::Err: Display {
    fn parse_with_unop(op: UnOp, input: syn::parse::ParseStream<'_>) -> syn::Result<Self> {
        let lookahead = input.lookahead1();

        let lhs;
        if lookahead.peek(syn::token::Paren) {
            let content;
            syn::parenthesized!(content in input);
            let expr = Expr::UnOp {
                op,
                expr: Box::new(content.parse::<Expr<T>>()?),
            };
            if input.is_empty() {
                return Ok(expr);
            }
            lhs = expr;
        } else if lookahead.peek(syn::LitFloat) {
            let lit = input.parse::<syn::LitFloat>()?;
            let value = lit.base10_parse::<T>()?;
            let expr = Expr::UnOp {
                op,
                expr: Box::new(Expr::Val(value)),
            };
            if input.is_empty() {
                return Ok(expr);
            }
            lhs = expr;
        } else if lookahead.peek(syn::LitInt) {
            let lit = input.parse::<syn::LitInt>()?;
            let value = lit.base10_parse::<T>()?;
            let expr = Expr::UnOp {
                op,
                expr: Box::new(Expr::Val(value)),
            };
            if input.is_empty() {
                return Ok(expr);
            }
            lhs = expr;
        } else if lookahead.peek(syn::Ident) {
            let ident = input.parse::<syn::Ident>()?;
            let expr = Expr::UnOp {
                op,
                expr: Box::new(Expr::Var(ident.to_string())),
            };
            if input.is_empty() {
                return Ok(expr);
            }
            lhs = expr;
        } else {
            return Err(lookahead.error());
        }

        let op = input.parse::<BinOp>()?;

        Self::parse_binop(Box::new(lhs), op, input)
    }

    fn parse_binop(lhs: Box<Expr<T>>, op: BinOp, input: syn::parse::ParseStream<'_>) -> syn::Result<Self> {
        let lookahead = input.lookahead1();

        let rhs;
        if lookahead.peek(syn::token::Paren) {
            let content;
            syn::parenthesized!(content in input);
            rhs = Box::new(content.parse::<Expr<T>>()?);
            if input.is_empty() {
                return Ok(Expr::BinOp { lhs, op, rhs });
            }
        } else if lookahead.peek(syn::LitFloat) {
            let lit = input.parse::<syn::LitFloat>()?;
            let value = lit.base10_parse::<T>()?;
            rhs = Box::new(Expr::Val(value));
            if input.is_empty() {
                return Ok(Expr::BinOp { lhs, op, rhs });
            }
        } else if lookahead.peek(syn::LitInt) {
            let lit = input.parse::<syn::LitInt>()?;
            let value = lit.base10_parse::<T>()?;
            rhs = Box::new(Expr::Val(value));
            if input.is_empty() {
                return Ok(Expr::BinOp { lhs, op, rhs });
            }
        } else if lookahead.peek(syn::Ident) {
            let ident = input.parse::<syn::Ident>()?;
            rhs = Box::new(Expr::Var(ident.to_string()));
            if input.is_empty() {
                return Ok(Expr::BinOp { lhs, op, rhs });
            }
        } else {
            return Err(lookahead.error());
        }

        let next_op = input.parse::<BinOp>()?;

        match (op, next_op) {
            (BinOp::Add | BinOp::Sub, BinOp::Mul | BinOp::Div) => {
                let rhs = Self::parse_binop(rhs, next_op, input)?;
                Ok(Expr::BinOp {
                    lhs,
                    op,
                    rhs: Box::new(rhs),
                })
            }
            _ => {
                let lhs = Expr::BinOp { lhs, op, rhs };
                Self::parse_binop(Box::new(lhs), next_op, input)
            }
        }
    }
}