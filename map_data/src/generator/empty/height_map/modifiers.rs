mod numbers;

use std::collections::{BTreeMap};
use std::collections::btree_map::Entry;

use egui::{Color32, ColorImage, DragValue, Ui, Widget, Pos2, Style, Painter};
use egui_snarl::{ui::{PinInfo, SnarlViewer}, InPin, NodeId, OutPin, Snarl};
use egui_snarl::ui::SnarlStyle;
use image::{ImageBuffer, Luma};
use crate::generator::empty::height_map::MAX_HEIGH;
use crate::generator::empty::height_map::modifiers::numbers::{expression_string_in_ui, ExprNodeF32, NumberTypes};
use crate::retained_image::RetainedImage;


pub type GrayImage = ImageBuffer<Luma<f32>, Vec<f32>>;

pub fn to_retained(name: impl Into<String>, img: &GrayImage) -> RetainedImage {
    let mut color = ColorImage::new([img.width() as _, img.height() as _], Color32::DEBUG_COLOR);
    for (x, y, p) in img.enumerate_pixels().map(|(x, y, p)| (x as usize, y as usize, p.0[0])) {
        if p > MAX_HEIGH {
            color[(x,y)] = Color32::from_gray(255);
        } else {
            color[(x,y)] = Color32::from_gray(p as u8);
        }
    }
    RetainedImage::from_color_image(name, color)
}

const STRING_COLOR: Color32 = Color32::from_rgb(0x00, 0xb0, 0x00);
const NUMBER_COLOR: Color32 = Color32::from_rgb(0xb0, 0x00, 0x00);
const IMAGE_COLOR: Color32 = Color32::from_rgb(0xb0, 0x00, 0xb0);
const HEIGHT_MAP_COLOR: Color32 = Color32::from_rgb(0xff, 0xff, 0xff);
const UNTYPED_COLOR: Color32 = Color32::from_rgb(0xb0, 0xb0, 0xb0);

#[derive(Copy, Clone, Eq, PartialEq)]
enum ValueType {
    Number(NumberTypes),
    HeightMap,
    String,
    Void,
}

impl ValueType {
    pub fn color(&self) -> Color32 {
        match self {
            ValueType::Number(_) => NUMBER_COLOR,
            ValueType::HeightMap => HEIGHT_MAP_COLOR,
            ValueType::String => STRING_COLOR,
            ValueType::Void => UNTYPED_COLOR,
        }
    }
}

#[derive(Clone)]
pub struct BezierRescaler{
    min: f32,
    max: f32,
    c1: f32,
    c2: f32,
}

#[derive(Clone)]
pub enum Node {
    Source,
    Out,
    BezierRescale(BezierRescaler),
    Invert,
    SmoothenSquares(u8),

    /// Value node with a single output.
    /// The value is editable in UI.
    Number(f32),

    /// Value node with a single output.
    String(String),

    /// Expression node with a single output.
    /// It has number of inputs equal to number of variables in the expression.
    ExprNode(ExprNodeF32),
}

impl Node {
    fn in_type(&self, input: usize) -> ValueType {
        match self {
            Node::Source => ValueType::Void,
            Node::Out => ValueType::HeightMap,
            Node::BezierRescale(_) => match input {
                0 => ValueType::HeightMap,
                1 | 2 | 3 | 4 => ValueType::Number(NumberTypes::F32),
                _ => ValueType::Void,
            }
            Node::Invert => ValueType::HeightMap,
            Node::SmoothenSquares(_) => match input {
                0 => ValueType::HeightMap,
                1 => ValueType::Number(NumberTypes::U8),
                _ => ValueType::Void,
            }
            Node::Number(_) => ValueType::Void,
            Node::String(_) => ValueType::Void,
            Node::ExprNode(e) => match input {
                0 => ValueType::String,
                _ if input <= e.bindings.len() => ValueType::Number(NumberTypes::F32),
                _ => ValueType::Void,
            }
        }
    }
    fn out_type(&self, _output: usize) -> ValueType {
        match self {
            Node::Source => ValueType::HeightMap,
            Node::Out => ValueType::Void,
            Node::BezierRescale(_) => ValueType::HeightMap,
            Node::Invert => ValueType::HeightMap,
            Node::SmoothenSquares(_) => ValueType::HeightMap,
            Node::Number(_) => ValueType::Number(NumberTypes::F32),
            Node::String(_) => ValueType::String,
            Node::ExprNode(_) => ValueType::Number(NumberTypes::F32),
        }
    }
    fn can_connect(&self, output: usize, to: &Self, input: usize) -> bool {
        match (self.out_type(output), to.in_type(input)) {
            (ValueType::Void, _) | (_, ValueType::Void) => false,
            (out_type, in_type) => out_type == in_type,
        }
    }
}

fn bezier_escale_in_label(idx: usize) -> &'static str {
    match idx {
        1 => "min",
        2 => "c1",
        3 => "c2",
        _ => "max",
    }
}

impl Node {
    fn apply(&self, source: &GrayImage, result: &mut GrayImage) {
        match self {
            Node::BezierRescale(br) => {
                //let mut pairs = vec![];
                for (old, r) in source.as_raw().iter().copied().zip(result.iter_mut()) {
                    let t = (old / MAX_HEIGH).min(1.);
                    let b = ((1. - t).powi(3) * br.min)
                        + (3. * (1. - t).powi(2) * t * br.c1)
                        + (3. * (1. - t) * t * t * br.c2)
                        + (t.powi(3) * br.max);
                    *r = b;
                    /*
                    let pair = (t, old, *r);
                    if !pairs.contains(&pair) {
                        pairs.push(pair);
                    }// */
                }
                /*
                for pair in pairs {
                    print!("{pair:?}, ")
                }
                println!();// */
            }
            Node::Out => {}
            Node::Invert => {
                for (s, r) in source.as_raw().iter().copied().zip(result.iter_mut()) {
                    *r = MAX_HEIGH - s;
                }
            }
            Node::SmoothenSquares(radius) => {
                let radius = *radius as u32;
                for x in radius .. source.width() - radius {
                    for y in radius .. source.height() - radius {
                        let mut acc = 0.0;
                        let mut count = 0;
                        for x in x.saturating_sub(radius) .. (x + radius).min(source.width()) {
                            for y in y.saturating_sub(radius) .. (y + radius).min(source.height()) {
                                acc += source.get_pixel(x, y).0[0];
                                count += 1;
                            }
                        }
                        result.get_pixel_mut(x, y).0[0] = acc / count as f32;
                    }
                }
            }
            Node::Source |
            Node::Number(_) |
            Node::String(_) |
            Node::ExprNode(_) => {
                unreachable!()
            }
        }
    }
    fn number_out(&self) -> f32 {
        match self {
            Node::Number(value) => *value,
            Node::ExprNode(expr_node) => expr_node.eval(),
            Node::Source |
            Node::Out |
            Node::BezierRescale(_) |
            Node::Invert |
            Node::SmoothenSquares(_) |
            Node::String(_) => unreachable!(),
        }
    }

    fn number_in(&mut self, idx: usize) -> &mut f32 {
        match self {
            Node::BezierRescale(br) => {
                match idx {
                    1 => &mut br.min,
                    2 => &mut br.c1,
                    3 => &mut br.c2,
                    4 => &mut br.max,
                    _ => unreachable!(),
                }
            },
            Node::ExprNode(expr_node) => &mut expr_node.values[idx - 1],
            Node::Source |
            Node::Out |
            Node::Number(_) |
            Node::Invert |
            Node::SmoothenSquares(_) |
            Node::String(_) => unreachable!(),
        }
    }

    fn label_in(&mut self, idx: usize) -> &str {
        match self {
            Node::BezierRescale(_) => {
                bezier_escale_in_label(idx)
            }
            Node::ExprNode(expr_node) => &expr_node.bindings[idx - 1],
            Node::Source |
            Node::Out |
            Node::Number(_) |
            Node::Invert |
            Node::SmoothenSquares(_) |
            Node::String(_) => unreachable!(),
        }
    }

    fn string_out(&self) -> &str {
        match self {
            Node::String(value) => &value,
            Node::BezierRescale(_) |
            Node::Source |
            Node::Out |
            Node::Number(_) |
            Node::Invert |
            Node::SmoothenSquares(_) |
            Node::ExprNode(_) => unreachable!(),
        }
    }

    fn string_in(&mut self) -> &mut String {
        match self {
            Node::ExprNode(expr_node) => &mut expr_node.text,
            Node::BezierRescale(_) |
            Node::Source |
            Node::Out |
            Node::Number(_) |
            Node::Invert |
            Node::SmoothenSquares(_) |
            Node::String(_) => unreachable!(),
        }
    }

    fn expr_node(&mut self) -> &mut ExprNodeF32 {
        match self {
            Node::ExprNode(expr_node) => expr_node,
            Node::BezierRescale(_) |
            Node::Source |
            Node::Out |
            Node::Number(_) |
            Node::Invert |
            Node::SmoothenSquares(_) |
            Node::String(_) => unreachable!(),
        }
    }
}

pub struct ModifierPipeline{
    width: u32,
    height: u32,
    flat_base_level: f32,
    height_maps_in: BTreeMap<NodeId, GrayImage>,
    height_maps_out: BTreeMap<NodeId, GrayImage>,
    previews: BTreeMap<NodeId, (f32, bool, RetainedImage)>,
}

macro_rules! _height_maps_out {
    ($self:ident, $to:ident, $in_map:ident) => {
        match $self.height_maps_out.entry($to) {
            Entry::Occupied(e) => {
                Some(($in_map, e.into_mut()))
            }
            Entry::Vacant(missing) => {
                Some(($in_map, missing.insert($in_map.clone())))
            }
        }
    };
}
impl ModifierPipeline {
    pub fn new(width: u16, height: u16) -> Self {
        let mut mp =
        Self{
            width: width as u32,
            height: height as u32,
            flat_base_level: 50.,
            height_maps_in: Default::default(),
            height_maps_out: Default::default(),
            previews: Default::default(),
        };
        mp.set_flat_base(50.);
        mp
    }
    pub fn from_image(img: GrayImage) -> Self {
        let width = img.width();
        let height = img.height();
        let mut height_maps_out = BTreeMap::default();
        height_maps_out.insert(NodeId(0), img);
        Self{
            width,
            height,
            flat_base_level: 50.,
            height_maps_in: BTreeMap::default(),
            height_maps_out,
            previews: Default::default(),
        }
    }
    pub fn set_flat_base(&mut self, flat_base_level: f32) {
        self.flat_base_level = flat_base_level;
        let hm = self.height_maps_out.entry(NodeId(0)).or_insert_with(||{
            GrayImage::new(self.width, self.height)
        });
        for p in hm.pixels_mut() {
            p.0[0] = flat_base_level
        }
        let _ = self.previews_remove(&NodeId(0));
    }
    pub fn final_heights(&self) -> &[f32] {
        self.height_maps_in.get(&NodeId(1)).unwrap().as_raw()
    }
    
    fn previews_remove(&mut self, node: &NodeId) {
        if let Some((_, dirty, _)) = self.previews.get_mut(node) {
            *dirty = true
        }
    }

    fn height_map_out_in_out(&mut self, from: NodeId, to: NodeId, only_if_changed: bool) -> Option<(&GrayImage, &mut GrayImage)> {
        let from_map = self.height_maps_out.get(&from)?;
        let in_map = match self.height_maps_in.entry(to) {
            Entry::Occupied(in_map) => {
                if in_map.get() == from_map {
                    if only_if_changed {
                        return None
                    }
                    in_map.into_mut()
                } else {
                    let in_map = in_map.into_mut();
                    in_map.copy_from_slice(from_map.as_raw());
                    in_map
                }
            }
            Entry::Vacant(missing) => {
                missing.insert(from_map.clone())
            }
        };
        _height_maps_out!(self, to, in_map)
    }
    fn height_map_in_out(&mut self, to: NodeId) -> Option<(&GrayImage, &mut GrayImage)> {
        let in_map = self.height_maps_in.get(&to)?;
        _height_maps_out!(self, to, in_map)
    }
    fn update(&mut self, node: NodeId, snarl: &mut Snarl<Node>) {
        if let Some((source, result)) = self.height_map_in_out(node) {
            snarl[node].apply(source, result);
            self.previews_remove(&node);
        }
        self.previews_remove(&node);
    }
    fn preview(&mut self, node: NodeId) -> Option<(&mut f32, &mut RetainedImage)> {
        macro_rules! insert {
            ($self:ident, $node: ident, $entry:ident, $scale:expr) => {
                if $node.0 == 1 {
                    if let Some(img) = $self.height_maps_in.get(&$node){
                        let img = to_retained(format!("Node: {:?}", $node), img);
                        let (scale, _, img) = $entry.insert(($scale, false, img));
                        Some((scale, img))
                    } else {
                        None
                    }
                } else {
                    if let Some(img) = $self.height_maps_out.get(&$node){
                        let img = to_retained(format!("Node: {:?}", $node), img);
                        let (scale, _, img) = $entry.insert(($scale, false, img));
                        Some((scale, img))
                    } else {
                        None
                    }
                }
            };
        }
        match self.previews.entry(node) {
            Entry::Occupied(mut entry) => {
                let (scale, dirty, _) = entry.get();
                if *dirty {
                    insert!(self, node, entry, *scale);
                    None
                } else {
                    let (scale, _, img) = entry.into_mut();
                    Some((scale, img))
                }
            },
            Entry::Vacant(missing) => {
                insert!(self, node, missing, 0.1)
            }
        }
    }
    fn propagate(&mut self, from: NodeId, to: NodeId) {
        if let Some(from_map) = self.height_maps_out.get(&from) {
            if let Some(to_map) = self.height_maps_in.get(&to) {
                if from_map != to_map {
                    self.height_maps_in.insert(to, from_map.clone());
                    self.previews_remove(&to);
                }
            } else {
                self.height_maps_in.insert(to, from_map.clone());
                self.previews_remove(&to);
            }
        } else {
            self.height_maps_in.remove(&to);
            self.previews_remove(&to);
        }
    }
    fn preview_ui(&mut self, scale: f32, node: NodeId, ui: &mut Ui) {
        if let Some((img_scale, img)) = self.preview(node) {
            //ui.vertical(|ui|{
            DragValue::new(img_scale).clamp_range(0.1..=2.).speed(0.1).ui(ui)
                .on_hover_text("scale the image").changed();
            img.show_scaled(ui, *img_scale * scale);
            //});
        }
    }
    fn update_from_remote(&mut self, snarl: &mut Snarl<Node>, pin: &InPin) {
        if !pin.remotes.is_empty() {
            if let Some((source, result)) =
                self.height_map_out_in_out(pin.remotes[0].node, pin.id.node, true) {
                snarl[pin.id.node].apply(source, result);
                self.previews_remove(&pin.id.node);
            }
        }
    }
}


impl SnarlViewer<Node> for ModifierPipeline {
    fn title(&mut self, node: &Node) -> String {
        match node {
            Node::Source => "Source".to_owned(),
            Node::Number(_) => "Number".to_owned(),
            Node::String(_) => "String".to_owned(),
            Node::ExprNode(_) => "Expr".to_owned(),
            Node::Out => "Out".to_owned(),
            Node::BezierRescale { .. } => "Bezier Rescale".to_owned(),
            Node::SmoothenSquares(_) => "Smoothen".to_owned(),
            Node::Invert => "Invert".to_owned(),
        }
    }

    fn has_on_hover_popup(&mut self, _: &Node) -> bool {
        true
    }

    fn outputs(&mut self, node: &Node) -> usize {
        match node {
            Node::Source => 1,
            Node::Number(_) => 1,
            Node::String(_) => 1,
            Node::ExprNode(_) => 1,
            Node::Out => 0,
            Node::BezierRescale { .. } => 1,
            Node::SmoothenSquares(_) => 1,
            Node::Invert => 1,
        }
    }

    fn inputs(&mut self, node: &Node) -> usize {
        match node {
            Node::Source => 0,
            Node::Number(_) => 0,
            Node::String(_) => 0,
            Node::ExprNode(expr_node) => 1 + expr_node.bindings.len(),
            Node::Out => 1,
            Node::BezierRescale { .. } => 5,
            Node::SmoothenSquares(_) => 2,
            Node::Invert => 1,
        }
    }

    fn show_input(
        &mut self,
        pin: &InPin,
        ui: &mut Ui,
        scale: f32,
        snarl: &mut Snarl<Node>,
    ) -> PinInfo {
        match snarl[pin.id.node] {
            Node::Source => {
                unreachable!("Source node has no inputs")
            },
            Node::Number(_) => {
                unreachable!("Number node has no inputs")
            }
            Node::String(_) => {
                unreachable!("String node has no inputs")
            }
            Node::ExprNode(_) if pin.id.input == 0 => {
                expression_string_in_ui(pin, ui, snarl);
                PinInfo::triangle().with_fill(STRING_COLOR)
            }
            Node::ExprNode(ref expr_node) => {
                if pin.id.input <= expr_node.bindings.len() {
                    match &*pin.remotes {
                        [] => {
                            let node = &mut snarl[pin.id.node];
                            ui.label(node.label_in(pin.id.input));
                            ui.add(DragValue::new(node.number_in(pin.id.input)));
                            PinInfo::square().with_fill(NUMBER_COLOR)
                        }
                        [remote] => {
                            let new_value = snarl[remote.node].number_out();
                            let node = &mut snarl[pin.id.node];
                            ui.label(node.label_in(pin.id.input));
                            ui.label(format_float(new_value));
                            *node.number_in(pin.id.input) = new_value;
                            PinInfo::square().with_fill(NUMBER_COLOR)
                        }
                        _ => unreachable!("Expr pins has only one wire"),
                    }
                } else {
                    ui.label("Removed");
                    PinInfo::circle().with_fill(Color32::BLACK)
                }
            }
            Node::Out => {
                if !pin.remotes.is_empty() {
                    if let Some(_) = self.height_map_out_in_out(pin.remotes[0].node, pin.id.node, true) {
                        self.previews_remove(&pin.id.node);
                    }
                }
                self.preview_ui(scale, pin.id.node, ui);
                PinInfo::square().with_fill(HEIGHT_MAP_COLOR)
            }
            Node::BezierRescale(_) => {
                if pin.id.input == 0 {
                    self.update_from_remote(snarl, pin);
                    PinInfo::square().with_fill(HEIGHT_MAP_COLOR)
                } else {
                    let mut changed = false;
                    if pin.remotes.is_empty() {
                        let node = &mut snarl[pin.id.node];
                        let p = node.number_in(pin.id.input);
                        ui.vertical(|ui|{
                            ui.label(bezier_escale_in_label(pin.id.input));
                            changed |= DragValue::new(p).clamp_range(0. ..=MAX_HEIGH).speed(0.1).ui(ui).changed();
                        });
                    } else {
                        let new_value = snarl[pin.remotes[0].node].number_out();
                        let node = &mut snarl[pin.id.node];
                        ui.vertical(|ui|{
                            ui.label(node.label_in(pin.id.input));
                            ui.label(format_float(new_value));
                        });
                        changed |= new_value != *node.number_in(pin.id.input);
                    }
                    if changed {
                        self.update(pin.id.node, snarl);
                    }
                    PinInfo::circle().with_fill(NUMBER_COLOR)
                }
            }
            Node::SmoothenSquares(ref mut square_radius) => {
                if pin.id.input == 0 {
                    self.update_from_remote(snarl, pin);
                    PinInfo::square().with_fill(HEIGHT_MAP_COLOR)
                } else {
                    if DragValue::new(square_radius).clamp_range(1..=8).ui(ui).changed() {
                        self.update(pin.id.node, snarl);
                    }
                    PinInfo::triangle().with_fill(NUMBER_COLOR)
                }
            }
            Node::Invert => {
                self.update_from_remote(snarl, pin);
                PinInfo::square().with_fill(HEIGHT_MAP_COLOR)
            }
        }
    }

    fn show_output(
        &mut self,
        pin: &OutPin,
        ui: &mut Ui,
        scale: f32,
        snarl: &mut Snarl<Node>,
    ) -> PinInfo {
        match snarl[pin.id.node] {
            Node::Source |
            Node::BezierRescale( _) |
            Node::SmoothenSquares(_) |
            Node::Invert => {
                self.preview_ui(scale, pin.id.node, ui);
                PinInfo::square().with_fill(HEIGHT_MAP_COLOR)
            }
            Node::Number(ref mut value) => {
                assert_eq!(pin.id.output, 0, "Number node has only one output");
                ui.add(DragValue::new(value));
                PinInfo::square().with_fill(NUMBER_COLOR)
            }
            Node::String(ref mut value) => {
                assert_eq!(pin.id.output, 0, "String node has only one output");
                let edit = egui::TextEdit::singleline(value)
                    .clip_text(false)
                    .desired_width(0.0)
                    .margin(ui.spacing().item_spacing);
                ui.add(edit);
                PinInfo::triangle().with_fill(STRING_COLOR)
            }
            Node::ExprNode(ref expr_node) => {
                let value = expr_node.eval();
                assert_eq!(pin.id.output, 0, "Expr node has only one output");
                ui.label(format_float(value));
                PinInfo::square().with_fill(NUMBER_COLOR)
            }
            Node::Out => unreachable!("Out node have no outputs"),
        }
    }

    fn show_on_hover_popup(
        &mut self,
        node: NodeId,
        _inputs: &[InPin],
        _outputs: &[OutPin],
        ui: &mut Ui,
        _scale: f32,
        snarl: &mut Snarl<Node>,
    ) {
        match snarl[node] {
            Node::Source => {
                ui.label("Source height map");
            }
            Node::Number(_) => {
                ui.label("Outputs a value");
            }
            Node::String(_) => {
                ui.label("Outputs a value");
            }
            Node::ExprNode(_) => {
                ui.label("Evaluates algebraic expression with input for each unique variable name");
            }
            Node::Out => {
                ui.label("Output height map");
            }
            Node::BezierRescale(_) => {
                ui.label("Recales the heights based on bezier curve");
            }
            Node::SmoothenSquares(_) => {
                ui.label("smoothens area");
            }
            Node::Invert => {
                ui.label("Inverts the height map");
            }
        }
    }
    /*
    fn draw_input_pin(&mut self, pin: &InPin, pin_info: &PinInfo, pos: Pos2, size: f32, snarl_style: &SnarlStyle, style: &Style, painter: &Painter, scale: f32, snarl: &Snarl<Node>) -> Color32 {
        snarl[pin.id.node].in_type(pin.id.input).color()
    }
    fn draw_output_pin(&mut self, pin: &OutPin, pin_info: &PinInfo, pos: Pos2, size: f32, snarl_style: &SnarlStyle, style: &Style, painter: &Painter, scale: f32, snarl: &Snarl<Node>) -> Color32 {
        snarl[pin.id.node].out_type(pin.id.output).color()
    }*/

    fn has_graph_menu(&mut self, _pos: Pos2, _snarl: &mut Snarl<Node>) -> bool {
        true
    }
    fn show_graph_menu(
        &mut self,
        pos: Pos2,
        ui: &mut Ui,
        _scale: f32,
        snarl: &mut Snarl<Node>,
    ) {
        ui.label("Add node");
        if ui.button("Number").clicked() {
            snarl.insert_node(pos, Node::Number(0.0));
            ui.close_menu();
        }
        if ui.button("Expr").clicked() {
            snarl.insert_node(pos, Node::ExprNode(ExprNodeF32::new()));
            ui.close_menu();
        }
        if ui.button("String").clicked() {
            snarl.insert_node(pos, Node::String("".to_owned()));
            ui.close_menu();
        }
        if ui.button("Bezier rescale").clicked() {
            snarl.insert_node(pos, Node::BezierRescale(BezierRescaler{
                min: 0.,
                c1: 0.,
                c2: MAX_HEIGH,
                max: MAX_HEIGH,
            }));
            ui.close_menu();
        }
        if ui.button("Smoothen").clicked() {
            snarl.insert_node(pos, Node::SmoothenSquares(2));
            ui.close_menu();
        }
        if ui.button("Invert").clicked() {
            snarl.insert_node(pos, Node::Invert);
            ui.close_menu();
        }
    }

    fn has_node_menu(&mut self, _node: &Node) -> bool {
        true
    }

    fn show_node_menu(
        &mut self,
        node: NodeId,
        _inputs: &[InPin],
        _outputs: &[OutPin],
        ui: &mut Ui,
        _scale: f32,
        snarl: &mut Snarl<Node>,
    ) {
        // source and out node can not be removed
        if node == NodeId(0) || node == NodeId(1) {
            return;
        }
        ui.label("Node menu");
        if ui.button("Remove").clicked() {
            snarl.remove_node(node);
            ui.close_menu();
        }
    }

    #[inline]
    fn connect(&mut self, from: &OutPin, to: &InPin, snarl: &mut Snarl<Node>) {
        // Validate connection
        if !snarl[from.id.node].can_connect(from.id.output, &snarl[to.id.node], to.id.input) {
            return;
        }

        for &remote in &to.remotes {
            snarl.disconnect(remote, to.id);
        }

        snarl.connect(from.id, to.id);
        self.propagate(from.id.node, to.id.node);

        if let Some((source, result)) =
            self.height_map_out_in_out(from.id.node, to.id.node, false){
            snarl[to.id.node].apply(source, result);
            self.previews_remove(&to.id.node);
        }
        self.previews_remove(&to.id.node);
        //self.propagate_to(from.id.node, snarl[from.id.node].clone(), &from.remotes, snarl);
    }
}
fn format_float(v: f32) -> String {
    let v = (v * 1000.0).round() / 1000.0;
    format!("{}", v)
}
