
use egui::{ColorImage, Widget};
use image::Rgb;
use crate::retained_image::RetainedImage;

mod step_1_vonori;
mod step_2_resources;
mod step_3_territories;
mod step_4_paths;
mod step_5_cliffs;
mod step_6_props_and_creeps;
mod step_7_normalmappatches;
mod step_8_textures;
#[allow(dead_code)]
mod step_9_file;
pub mod rng;

pub struct Generator{
    debug: DebugData,
}

struct DebugData{
    voronoi: RetainedImage,
    scale: f32,
    vonori_img_save: image::RgbImage,
    tmp_path: String,
}

#[allow(unreachable_code)]
pub fn generate_map() -> Generator {
    panic!("❗ WIP ❗ means do not touch it yet, it is not even 1% finished.");
    let seed = 65161;
    let difficulty = 1;
    let player_count = 1;
    let map_size = 512;
    let param_23 = 128;
    let vonori = step_1_vonori::VonoriLG::Load;
    let vonori = step_1_vonori::processing_voronoi_partition(vonori);
    assert_eq!(map_size, vonori.map_size as u32);
    let mut big = step_2_resources::BigStruct1098430::default();
    step_2_resources::creating_resource_distribution(map_size, seed, difficulty, player_count, param_23, &mut big);
    /*step_3_territories::shaping_and_growing();
    step_4_paths::creating_pathes_and_routes();
    // optionally save heightmap.bmp
    step_5_cliffs::attaching_cliffs();
    step_6_props_and_creeps::positioning_props();
    step_6_props_and_creeps::placing_barriers();
    step_6_props_and_creeps::placing_creeps();
    step_7_normalmappatches::positioning_normalmappatches();
    step_8_textures::creating_textures();
    //step_9_file::save();*/
    let (cimg, img) = voroni_img(map_size, &vonori);
    Generator{
        debug: DebugData {
            voronoi: RetainedImage::from_color_image("vonori", cimg),
            vonori_img_save: img,
            scale: 2.,
            tmp_path: "".to_string(),
        },
    }
}

impl Generator {
    pub fn show(&mut self, ctx: &egui::Context) {
        egui::Window::new("vonori").show(ctx, |ui|{
            egui::scroll_area::ScrollArea::both().show(ui, |ui|{
                ui.horizontal(|ui|{
                    ui.label("save path:");
                    ui.text_edit_singleline(&mut self.debug.tmp_path);
                    if ui.button("save").clicked() {
                        self.debug.vonori_img_save.save(&self.debug.tmp_path).unwrap();
                    }
                });
                ui.horizontal(|ui|{
                    ui.label("scale:");
                    egui::DragValue::new(&mut self.debug.scale).clamp_range(0.5 ..= 20.).ui(ui);
                });
                self.debug.voronoi.show_scaled(ui, self.debug.scale);
            });
        });
    }
}


fn voroni_img(map_size: u32, vonori: &step_1_vonori::VonoriData) -> (ColorImage, image::RgbImage) {
    let mut img = image::RgbImage::new(map_size, map_size);
    for y in 0..map_size {
        for x in 0..map_size {
            let i = vonori.indexes[(x + y * map_size) as usize];
            let rgb = if i == 0xFFFF {
                //[0,255,0]
                unreachable!("All indexes should be filled?")
            } else {
                let e = &vonori.something[i as usize];
                let le = i.to_le_bytes();
                let dx = x as f32 - e.x;
                let dy = y as f32 - e.y;
                let d = dy * dy + dx * dx;
                let s = d.sqrt();
                if s >= 1.4 {
                    [le[0],le[1], 0]
                } else {
                    [255,255,255]
                }
            };
            *img.get_pixel_mut(x, map_size - (y+1)) = Rgb::from(rgb);
        }
    }
    let cimg = ColorImage::from_rgb([512,512], &img);
    (cimg, img)
}
