use std::path::Path;
use log::info;
use sr_libs::utils::commands::{ParsingError, read_f32, read_u16, read_u32};

// https://en.wikipedia.org/wiki/Voronoi_diagram

pub enum VonoriLG { Generate = 0, Load = 1, }
pub enum MirrorType { Normal = 0, Mirrored = 1, }

pub struct VonoriData {
    pub map_size: u16,
    pub indexes: Vec<u16>,
    pub something: Vec<VonoriSomething>,
}
#[derive(Debug)]
pub struct VonoriSomething{
    pub index: u16,
    pub vonori_path: u16,
    pub x: f32,
    pub y: f32,
    pub data: Vec<u16>,
}

pub fn processing_voronoi_partition(vonori: VonoriLG) -> VonoriData {
    info!("processing voronoi partition");

    match vonori {
        VonoriLG::Generate => {
            creating_voronoi_centers();
            generating_voronoi_parition()
        }
        VonoriLG::Load => {
            // TODO an actual choice
            let path = Path::new(r#"G:\BattleForge\base\pakDefault\bf1\map\randommaps\vp_512_12000_nm.vp"#);
            load_voronoi_parition(path).unwrap()
        }
    }
}

fn creating_voronoi_centers() {
    info!("Creating Voronoi Centers");
    // no idea, what is it doing :(
}

fn generating_voronoi_parition() -> VonoriData {
    info!("Generating Voronoi Parition");
    // no idea, what is it doing :(
    unimplemented!();
}

fn load_voronoi_parition(vonori_path: &Path) -> Result<VonoriData, VonoriLoadError> {
    info!("Loading Voronoi Parition");
    let vonori_file_data = std::fs::read(vonori_path)?;
    let (_useless_header, rest) = vonori_file_data.split_at(20);
    let (_v59, rest) = read_u16(rest)?;
    let (map_size, rest) = read_u16(rest)?;// must match generated map's size
    let (_mirror_type, rest) = read_u32(rest)?; // must match generated map's mirror type
    let mirror_type = MirrorType::Normal;
    let (_var_50_3, rest) = read_u32(rest)?;
    let (_y, rest) = read_u32(rest)?;
    let (_x, rest) = read_u32(rest)?;
    let (_v66, rest) = read_u32(rest)?;
    let (vonori_count, rest) = read_u16(rest)?;
    match mirror_type {
        MirrorType::Normal => {
            //&[0, map_size, 0., 0., 0., 0.]
        }
        MirrorType::Mirrored => {
            // 50% chance
            // let half = (map_size - 1) * 0.5
            //&[1, map_size, half, 0., 0., half]
            //&[1, map_size, 0., half, half, 0.]
        }
    }
    let mut something = Vec::with_capacity(vonori_count as usize);
    let mut loop_rest = rest;
    for _ in 0..vonori_count {
        let (index, rest) = read_u16(loop_rest)?;
        let (vonori_path, rest) = read_u16(rest)?;
        let (x, rest) = read_f32(rest)?;
        let (y, rest) = read_f32(rest)?;
        // no idea, what is it doing :(
        // sometimes add to a vector
        let (count, rest) = read_u16(rest)?;
        loop_rest = rest;
        let mut data = Vec::with_capacity(count as usize);
        for _ in 0..count {
            let (tmp, rest) = read_u16(loop_rest)?;
            data.push(tmp);
            loop_rest = rest;
        }
        assert_eq!(index as usize, something.len(), "array index is strored, so parsing is wrong, in some edge case");
        something.push(VonoriSomething{ index, vonori_path, x, y, data, });
    }
    let rest = loop_rest;
    // no idea, what is it doing :(
    let (x, rest) = read_u32(rest)?;
    let (y, rest) = read_u32(rest)?;
    // some bit magic to determine allocation size
    let mut indexes = Vec::with_capacity((x * y) as usize);
    let mut loop_rest = rest;
    for _x in 0..map_size {
        for _y in 0..map_size {
            // asser "_uX < m_uU && _uY < m_uV"
            let (tmp, rest) = read_u16(loop_rest)?;
            indexes.push(tmp);
            loop_rest = rest;
        }
    }

    Ok(VonoriData{ map_size, indexes, something, })
}

#[derive(Debug)]
enum VonoriLoadError {
    IO(std::io::Error),
    Parsing(ParsingError),
}

impl From<std::io::Error> for VonoriLoadError {
    fn from(value: std::io::Error) -> Self {
        Self::IO(value)
    }
}

impl From<ParsingError> for VonoriLoadError {
    fn from(value: ParsingError) -> Self {
        Self::Parsing(value)
    }
}
