use egui::{Ui, Widget};
use sr_libs::utils::commands::{read_f32, read_u32};
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use crate::{Fix, IssueCounter, Validate};
use crate::{Map, MapError};

#[derive(serde::Serialize)]
pub struct CameraPosition{
    pub x: f32,
    pub y: f32,
    // something for height and rotation maybe?
    pub unk1: u32,
    pub unk2: u32,
}

impl CameraPosition {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, MapError> {
        let (x, buf) = read_f32(buf)
            .map_err(|e| MapError::MapFileParsing(format!("x, because: {e:?}")))?;
        let (y, buf) = read_f32(buf)
            .map_err(|e| MapError::MapFileParsing(format!("y, because: {e:?}")))?;
        let (unk1, buf) = read_u32(buf)
            .map_err(|e| MapError::MapFileParsing(format!("unk1, because: {e:?}")))?;
        let (unk2, _buf) = read_u32(buf)
            .map_err(|e| MapError::MapFileParsing(format!("unk2, because: {e:?}")))?;
        Ok(Self{x,y, unk1, unk2})
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buf = BytesMut::new();
        buf.put_f32_le(self.x);
        buf.put_f32_le(self.y);
        buf.put_u32_le(self.unk1);
        buf.put_u32_le(self.unk2);
        buf.to_vec()
    }
}

impl Fix for CameraPosition {
    fn fix(&mut self, _map: &Map) {
    }
}

impl Validate for CameraPosition {
    fn validate(&self, _map: &Map) -> IssueCounter {
        IssueCounter::default()
    }
}

impl CameraPosition {
    pub fn show_mut(&mut self, ui: &mut Ui) {
        ui.horizontal(|ui|{
            ui.label("x/y: ");
            egui::DragValue::new(&mut self.x).ui(ui);
            egui::DragValue::new(&mut self.y).ui(ui);
        });
        ui.horizontal(|ui|{
            ui.label("unk: ");
            egui::DragValue::new(&mut self.unk1).ui(ui);
            egui::DragValue::new(&mut self.unk2).ui(ui);
        });
    }
}
