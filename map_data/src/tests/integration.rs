use crate::app::pak;
use super::super::*;

#[test]
fn community_smithy() {
    extract_and_save_to_same("C:/Users/xxxku/Documents/BattleForge/map/community_smithy.pak");
}


fn extract_and_save_to_same(path: &str) {
    let original_pak = std::fs::read(path).unwrap();
    let files = pak::get_files_for_pak(original_pak.as_slice()).unwrap();
    let original_blob = &files.iter().find(|(name, _)| name[name.len()-4..].eq_ignore_ascii_case(".map")).as_ref().unwrap().1;
    let mut bytes = sr_libs::cff::check_signature(&original_blob).unwrap();
    let mut map_files = BTreeMap::new();
    while bytes.len() >= 16 {
        let (input, table) = sr_libs::cff::rw_utils::parse_abstract_cff_table(bytes).unwrap();
        let mut decompressor = flate2::read::ZlibDecoder::new(table.data);
        let mut data = Vec::new();
        use std::io::Read;
        decompressor.read_to_end(&mut data).unwrap();
        if table.decompressed_size != data.len() as u32 {
            panic!("Expected size: {}, real size: {}, for table: {}", table.decompressed_size, data.len(), table.id);
        }
        map_files.insert(table.id, (data, table.unknown1, table.unknown2));
        bytes = input;
    }

    let expected_files = BTreeMap::from_iter(files.iter().map(|(k,v)| (k.clone(), v.clone())));
    let map = Map::from_pak_files(files).unwrap();
    for (id, (expected, expected_unk1, expected_unk2)) in map_files.iter() {
        let (table, original, unk1, unk2) =  map.map.get(id).unwrap();
        let table = table.borrow().to_bytes();
        assert_eq!(expected, original, "table: {id} content changed");
        assert_eq!(expected, &table, "table: {id} content mismatch");
        assert_eq!(expected_unk1, unk1, "table: {id} unk1 mismatch");
        assert_eq!(expected_unk2, unk2, "table: {id} unk2 mismatch");
    }
    let repacked = map.to_pak_files().unwrap();
    let repacked_files = BTreeMap::from_iter(repacked.iter().map(|(k,v)| (k.clone(), v.clone())));
    let expected_file_names = expected_files.keys().collect::<Vec<&String>>();
    let repacked_files_names = repacked_files.keys().collect::<Vec<&String>>();
    assert_eq!(expected_file_names, repacked_files_names, "files changed by repacking");
    for (name, expected) in expected_files.iter() {
        if name.ends_with(".map") {
            // checked above, and it can differer, because it is practically double compressed
            continue;
        }
        let repacked = repacked_files.get(name).unwrap();
        assert_eq!(expected, repacked, "files {name} changed by repacking");
    }
}
