pub mod team_config;
pub mod buildings;
pub mod entity;
pub mod atlas;
pub mod object;
pub mod squad;
pub mod barrier;
pub mod power_slot;
pub mod monuments;
pub mod player_kit;
#[allow(dead_code)]
pub mod generator;
pub mod terrain;
pub mod atmo_zones;
pub mod effect_zones;
pub mod staring_point;
pub mod effect;
pub mod script_group;
pub mod _8003;
pub mod cliff;
pub mod texture_layer;
pub mod pkit_info;
pub mod camera_position;
pub mod entity_prefabs;

#[cfg(test)]
mod tests;
pub mod crc;
mod parse;

pub mod pak;
mod retained_image;

use std::cell::RefCell;
use std::collections::BTreeMap;
use std::io::Write;
use std::iter::Sum;
use std::ops::{Add, AddAssign};
use std::path::{Path, PathBuf};
use std::str::FromStr;
use egui::Color32;
use local_encoding::Encoder;
use log::info;
use sr_libs::utils::commands::bytes::{BufMut, BytesMut};
use script_validator::{EntityType, SpawnGroupsValidationResult, ValidationResult};
use crate::_8003::Data8003;
use crate::atlas::{AtlasCol, AtlasNor};
use crate::atmo_zones::AtmoZones;
use crate::barrier::{Barriers, BarrierSets};
use crate::buildings::Buildings;
use crate::camera_position::CameraPosition;
use crate::cliff::Cliffs;
use crate::effect::Effects;
use crate::effect_zones::EffectZones;
use crate::entity_prefabs::Prefabs;
use crate::monuments::Monuments;
use crate::object::Objects;
use crate::parse::{AllScripts, GenericFiles};
use crate::pkit_info::PkitInfo;
use crate::player_kit::PlayerKits;
use crate::power_slot::PowerSlots;
use crate::script_group::ScriptGroups;
use crate::squad::Squads;
use crate::staring_point::StartingPoints;
use crate::terrain::Terrain;
use crate::texture_layer::TextureLayers;

pub struct Map {
    // no support for 12p (3x4p) maps
    pub map: BTreeMap<u32, (RefCell<MapTable>, Vec<u8>, u16, u16)>,
    pub entity_tags: BTreeMap<String, (u32, EntityType)>,
    pub map_name: String,
    pub files: Vec<RefCell<FilesEnum>>,
    pub map_compression: flate2::Compression,
}

pub enum FilesEnum {
    GlobalVariables(Script),
    ScriptList(Script),
    ScriptGroups(Script),
    SpawnGroups(Script),
    Script(Script),
    AtlasCol(AtlasCol),
    AtlasNor(AtlasNor),
    Descriptions(OtherFile),

    OtherFiles(OtherFile),
}

impl FilesEnum {
    pub fn path(&self) -> PathBuf {
        match self {
            FilesEnum::GlobalVariables(s) =>
                match s.folder {
                    1 | 2 | 3 => PathBuf::new().join(format!("script{}", s.folder)).join(&s.name),
                    _ => unreachable!(),
                }
            FilesEnum::ScriptList(s) =>
                match s.folder {
                    1 | 2 | 3 => PathBuf::new().join(format!("script{}", s.folder)).join(&s.name),
                    _ => unreachable!(),
                }
            FilesEnum::ScriptGroups(s) =>
                match s.folder {
                    1 | 2 | 3 => PathBuf::new().join(format!("script{}", s.folder)).join(&s.name),
                    _ => unreachable!(),
                }
            FilesEnum::SpawnGroups(s) =>
                match s.folder {
                    1 | 2 | 3 => PathBuf::new().join(format!("script{}", s.folder)).join(&s.name),
                    _ => unreachable!(),
                }
            FilesEnum::Script(s) =>
                match s.folder {
                    1 | 2 | 3 => PathBuf::new().join(format!("script{}", s.folder)).join(&s.name),
                    _ => unreachable!(),
                }
            FilesEnum::AtlasCol(a) => PathBuf::from_str(&a.col.relative_path).unwrap(),
            FilesEnum::AtlasNor(a) => PathBuf::from_str(&a.nor.relative_path).unwrap(),
            FilesEnum::Descriptions(f) => PathBuf::from_str(&f.relative_path).unwrap(),
            FilesEnum::OtherFiles(f) => PathBuf::from_str(&f.relative_path).unwrap(),
        }
    }
    pub fn as_blob(&self) -> Vec<u8> {
        match self {
            FilesEnum::GlobalVariables(s) |
            FilesEnum::ScriptList(s) |
            FilesEnum::ScriptGroups(s) |
            FilesEnum::SpawnGroups(s) |
            FilesEnum::Script(s) => local_encoding::Encoding::ANSI.to_bytes(&s.content).unwrap(),
            FilesEnum::AtlasCol(a) => a.col.content.clone(),
            FilesEnum::AtlasNor(a) => a.nor.content.clone(),
            FilesEnum::Descriptions(f) => f.content.clone(),
            FilesEnum::OtherFiles(f) => f.content.clone(),
        }
    }
}
impl Validate for FilesEnum {
    fn validate(&self, map: &Map) -> IssueCounter {
        match self {
            FilesEnum::GlobalVariables(script) |
            FilesEnum::ScriptList(script) |
            FilesEnum::ScriptGroups(script) |
            FilesEnum::SpawnGroups(script) |
            FilesEnum::Script(script) => script.validate(map),
            FilesEnum::AtlasCol(atlas) => atlas.validate(map),
            FilesEnum::AtlasNor(atlas) => atlas.validate(map),
            FilesEnum::Descriptions(_file) => {
                IssueCounter::default()
            }
            FilesEnum::OtherFiles(_) => {
                IssueCounter::default()
            }
        }
    }
}

pub enum MapTable {
    Unknown(Vec<u8>),
    Terrain(Terrain),
    ScriptGroups(ScriptGroups, usize),
    ParsingFailed(Vec<u8>, u32, String),
    Objects(Objects, usize),
    AtmoZones(AtmoZones),
    EffectZones(EffectZones),
    Buildings(Buildings, usize),
    Squads(Squads, usize),
    Barriers(Barriers, usize),
    BarrierSets(BarrierSets, usize),
    StartingPoints(StartingPoints, usize),
    Effects(Effects, usize),
    PowerSlots(PowerSlots, usize),
    Monuments(Monuments, usize),
    PlayerKits(PlayerKits, usize),
    TeamConfigurations(team_config::TeamConfigurations),
    TextureLayers(TextureLayers),
    Cliffs(Cliffs),
    F8003(Data8003),
    PkitInfo(PkitInfo),
    Prefabs(Prefabs),
    CameraPosition(CameraPosition),
}

pub struct Script {
    pub name: String,
    pub folder: u8,
    pub content: String,
    pub validation_result: ScriptValidationResult,
}
pub enum ScriptValidationResult{
    GlobalVariables,
    ScriptList(Result<Vec<String>, String>),
    ScriptGroups(Result<BTreeMap<String, Vec<String>>, String>),
    NotPartOfHierarchy,
    Validated(ValidationResult, EntityType),
    SpawnGroupsValidated(SpawnGroupsValidationResult),
    IncludedMultipleTimes(Vec<ValidationResult>),
}

impl ScriptValidationResult {
    pub fn add(&mut self, other: Self) {
        match self {
            ScriptValidationResult::GlobalVariables => unreachable!("Global variables included in a script"),
            ScriptValidationResult::ScriptList(_) => unreachable!("Script list included in a script"),
            ScriptValidationResult::ScriptGroups(_) => unreachable!("Script groups included in a script"),
            ScriptValidationResult::SpawnGroupsValidated(_) => unreachable!("Spawn groups included in a script"),
            ScriptValidationResult::NotPartOfHierarchy => { *self = other; }
            ScriptValidationResult::Validated(_, _) => {
                let mut old = Self::NotPartOfHierarchy;
                std::mem::swap(&mut old, self);
                let r =
                    match old {
                        ScriptValidationResult::Validated(r, _) => r,
                        _ => unreachable!() };
                match other {
                    ScriptValidationResult::GlobalVariables => unreachable!("Script can not be both global variables, and normal script at same time!"),
                    ScriptValidationResult::ScriptList(_) => unreachable!("Script can not be both script list, and normal script at same time!"),
                    ScriptValidationResult::ScriptGroups(_) => unreachable!("Script groups can not include a script"),
                    ScriptValidationResult::SpawnGroupsValidated(_) => todo!("Spawn groups can not include a script"),
                    ScriptValidationResult::NotPartOfHierarchy => unreachable!("Removing not supported"),
                    ScriptValidationResult::Validated(nr, _) => {
                        *self = Self::IncludedMultipleTimes(vec![r, nr]);
                    }
                    ScriptValidationResult::IncludedMultipleTimes(_) => unreachable!("multiple can be only on the left")
                }
            }
            ScriptValidationResult::IncludedMultipleTimes(multiple) => {
                match other {
                    ScriptValidationResult::GlobalVariables => unreachable!("Script can not be both global variables, and normal script at same time!"),
                    ScriptValidationResult::ScriptList(_) => unreachable!("Script can not be both script list, and normal script at same time!"),
                    ScriptValidationResult::ScriptGroups(_) => unreachable!("Script groups can not include a script"),
                    ScriptValidationResult::SpawnGroupsValidated(_) => todo!("Spawn groups can not include a script"),
                    ScriptValidationResult::NotPartOfHierarchy => unreachable!("Removing not supported"),
                    ScriptValidationResult::Validated(nr, _) => { multiple.push(nr); }
                    ScriptValidationResult::IncludedMultipleTimes(_) => unreachable!("multiple can be only on the left")
                }
            }
        }
    }
}

pub struct OtherFile {
    pub relative_path: String,
    pub content: Vec<u8>,
}

const COMMUNITY_MAP_PREFIX: &'static str = "bf1\\map\\ug_not_shipped\\";
const SCRIPT : &str = "script";
impl Map {
    pub fn fix(&mut self) {
        for (_, (table, _, _, _)) in &self.map {
            table.borrow_mut().fix(&self)
        }
        // no fix for scripts yet
    }
    pub fn from_pak_files(files: Vec<(String, Vec<u8>)>) -> Result<Self, MapError> {
        for (file, _) in &files {
            if !file.starts_with(COMMUNITY_MAP_PREFIX) {
                // Should allow reading official maps? (they are much more complex, because of multiple files, ...)
                return Err(MapError::NotACommunityMapPrefix(format!("Path '{file}' does not start with '{COMMUNITY_MAP_PREFIX}' prefix")))
            };
        }
        Self::from_files(files.into_iter().map(|(file, data)|{
            if file.starts_with(COMMUNITY_MAP_PREFIX) {
                (file[COMMUNITY_MAP_PREFIX.len()..].to_string(), data)
            } else {
                unreachable!()
            }
        }))
    }
    pub fn from_files(files: impl Iterator<Item=(String, Vec<u8>)>) -> Result<Self, MapError> {
        let GenericFiles {
            map_name, map, scripts, mut other_files
        } = parse::group_generic_files(files)?;

        let map = parse::extract_map(&map)?;
        let entity_tags = parse::extract_entity_tags(&map)?;

        // TODO needs to load turrets
        //parse::update_base_ids(&map);

        let AllScripts {
            scripts, global_variables, script_list, script_groups, spawn_groups
        } = parse::validate_scripts(scripts, &map_name, &entity_tags)?;

        // TODO script_groups `_Member` sufixed scripts
        info!("Map parsed");

        let mut files = vec![];
        for script in scripts {
            files.push(RefCell::new(FilesEnum::Script(script)))
        }
        if let Some(script) = global_variables {
            files.push(RefCell::new(FilesEnum::GlobalVariables(script)))
        }
        if let Some(script) = script_list {
            files.push(RefCell::new(FilesEnum::ScriptList(script)))
        }
        if let Some(script) = script_groups {
            files.push(RefCell::new(FilesEnum::ScriptGroups(script)))
        }
        if let Some(script) = spawn_groups {
            files.push(RefCell::new(FilesEnum::SpawnGroups(script)))
        }
        if let Some(atlac_col) = AtlasCol::new(&mut other_files){
            files.push(RefCell::new(FilesEnum::AtlasCol(atlac_col)));
        }
        if let Some(atlac_nor) = AtlasNor::new(&mut other_files){
            files.push(RefCell::new(FilesEnum::AtlasNor(atlac_nor)));
        }
        for file in other_files {
            files.push(RefCell::new(FilesEnum::OtherFiles(file)))
        }

        // TODO consider doing partial sorst "special scripts", "scripts", "other files"
        files.sort_by_key(|f| f.borrow().path());

        Ok(Self{ map, entity_tags, map_name, files, map_compression: flate2::Compression::none() })
    }

    pub fn to_pak_files(&self) -> std::io::Result<Vec<(String, Vec<u8>)>> {
        let mut files = vec![];
        let name = &self.map_name;

        let base_path = PathBuf::from_str(COMMUNITY_MAP_PREFIX).unwrap().join(name);
        let map = self.as_blob(self.map_compression)?;
        files.push((format!("{COMMUNITY_MAP_PREFIX}{name}.map"), map));
        for file in self.files.iter() {
            let file = file.borrow();
            let path = base_path.join(file.path());
            files.push((path.to_str().unwrap().to_owned(), file.as_blob()));
        }

        Ok(files)
    }

    pub fn save_pak(&self, path: &Path) -> std::io::Result<()> {
        pak::save(path, self.to_pak_files()?)
    }

    pub fn save_map_files(&mut self, path: &Path) -> std::io::Result<()> {
        let name = path.file_name().unwrap().to_str().unwrap();
        let name = name[..name.len() - 4].to_string();
        let folder_path = path.parent().unwrap().join(&name);
        self.map_name = name;
        std::fs::create_dir_all(&folder_path)?;
        for file in self.files.iter() {
            let file = file.borrow();
            let file_path = folder_path.join(file.path());
            std::fs::create_dir_all(file_path.parent().unwrap())?;
            std::fs::write(&file_path, file.as_blob())?;
        }

        std::fs::write(path, self.as_blob(self.map_compression)?)
    }

    pub fn save_bec_file(&mut self, path: &Path) -> std::io::Result<()> {
        if self.map.get(&ENTITY_PREFABS).is_none() {
            panic!("Missing prefabs");
        }
        if self.map.get(&CAMERA_POSITION_FOR_PREFABS_LOADING).is_none() {
            panic!("Missing camera position");
        }
        std::fs::write(path, self.as_blob(self.map_compression)?)
    }

    pub fn extract(&self, folder: &Path) -> std::io::Result<()> {
        std::fs::create_dir_all(&folder)?;
        for (id, (table, original, _, _)) in &self.map {
            let path = folder.join(format!("{id}"));
            let modified = table.borrow().to_bytes();
            if modified.iter().zip(original).any(|(m, o)| *m != *o) {
                info!("{id}:{:?} changed", table.borrow().name());
            }
            std::fs::write(&path, modified)?;
            let path = folder.join(format!("{id}.orig"));
            std::fs::write(&path, &original)?;
            if let Some(text) = table.borrow().as_text() {
                let path = folder.join(format!("{id}.json"));
                std::fs::write(&path, text)?;
            }
        }
        Ok(())
    }
    fn as_blob(&self, default_compression: flate2::Compression) -> std::io::Result<Vec<u8>> {
        let mut buf = BytesMut::new();
        buf.put_slice(&sr_libs::cff::SIGNATURE);
        buf.put_slice(&[0,0,0,1,0,0,0,1,0,0,0,0,0,0,0]);
        for (id, (table, _, unk1, unk2)) in self.map.iter() {
            let table = table.borrow().to_bytes();
            let decompressed_size = table.len() as u32;
            let compression = if table.is_empty() {
                flate2::Compression::default() // there is a bug that none produces unparsable output for empty input
            } else {
                default_compression
            };
            let mut compressor = flate2::write::ZlibEncoder::new(Vec::new(), compression);
            compressor.write_all(&table)?;
            let compressed_table = compressor.finish()?;
            buf.put_u32_le(*id);
            buf.put_u16_le(*unk1);
            buf.put_u32_le(compressed_table.len() as u32);
            buf.put_u16_le(*unk2);
            buf.put_u32_le(decompressed_size);
            buf.put_slice(&compressed_table);
        }
        buf.put_u32_le(crc::compute(&buf));
        Ok(buf.to_vec())
    }
}

pub const TERRAIN_TABLE_ID: u32 = 1000;
pub const OBJECTS_TABLE_ID: u32 = 1001;
pub const PLAYER_KITS_TABLE_ID: u32 = 1004;
pub const SQUADS_TABLE_ID: u32 = 1005;
pub const BUILDINGS_TABLE_ID: u32 = 1006;
pub const ATMO_ZONES_TABLE_ID: u32 = 1010;
pub const EFFECT_ZONES_TABLE_ID: u32 = 1011;
pub const TEAM_MANAGER_TABLE_ID: u32 = 1014;
pub const SCRIPT_GROUPS_TABLE_ID: u32 = 1018;
pub const LIGHTINGS_TABLE_ID: u32 = 1019;
pub const BARRIERS_TABLE_ID: u32 = 1025;
pub const BARRIER_SETS_TABLE_ID: u32 = 1026;
pub const STARTING_POINTS_TABLE_ID: u32 = 1027;
pub const POWER_SLOTS_TABLE_ID: u32 = 1029;
pub const MONUMENTS_TABLE_ID: u32 = 1030;
pub const EFFECTS_TABLE_ID: u32 = 1031;
pub const WAYPOINT_GRID_MANAGER_TABLE_ID: u32 = 1035;
pub const TEXTURE_LAYERS_TABLE_ID: u32 = 8001;
pub const CLIFF_TABLE_ID: u32 = 8002;
pub const F8003_TABLE_ID: u32 = 8003;
pub const TEAM_CONFIGURATIONS_TABLE_ID: u32 = 9000;
pub const PKIT_START_INFO_TABLE_ID: u32 = 9010;
pub const CAMERA_POSITION_FOR_PREFABS_LOADING: u32 = 16000;
pub const ENTITY_PREFABS: u32 = 16001;

impl Validate for Map {
    fn validate(&self, map: &Map) -> IssueCounter {
        let mut issues = IssueCounter::default();
        for (_, (t, _, _, _)) in self.map.iter() {
            issues += t.borrow().validate(map);
        }
        for file in self.files.iter() {
            issues += file.borrow().validate(map);
        }
        if !self.files.iter().any(|f| matches!(&*f.borrow(), FilesEnum::AtlasCol(_))) {
            issues.add_bug("'terrain\\atlas_col.dds' is missing".to_string())
        }
        if !self.files.iter().any(|f| matches!(&*f.borrow(), FilesEnum::AtlasNor(_))) {
            issues.add_bug("'terrain\\atlas_nor.dds' is missing".to_string())
        }
        issues
    }
}

impl MapTable {
    pub fn from_data(id: u32, data: Vec<u8>) -> Self {
        Self::from_data_inner(id, &data)
            .unwrap_or_else(|e|{
                match e {
                    MapError::NoMap => Self::Unknown(data),
                    _ => {
                        Self::ParsingFailed(data, id, format!("parsing {id} file failed because: {e:?}"))
                    }
                }
            })
    }
    fn from_data_inner(id: u32, data: &[u8]) -> Result<Self, MapError> {
        match id {
            TERRAIN_TABLE_ID => Ok(Self::Terrain(Terrain::from_bytes(&data)?)),
            SCRIPT_GROUPS_TABLE_ID => Ok(Self::ScriptGroups(ScriptGroups::from_bytes(&data)?, 0)),
            OBJECTS_TABLE_ID => Ok(Self::Objects(Objects::from_bytes(&data)?, 0)),
            PLAYER_KITS_TABLE_ID => Ok(Self::PlayerKits(PlayerKits::from_bytes(&data)?, 0)),
            SQUADS_TABLE_ID => Ok(Self::Squads(Squads::from_bytes(&data)?, 0)),
            BUILDINGS_TABLE_ID => Ok(Self::Buildings(Buildings::from_bytes(&data)?, 0)),
            ATMO_ZONES_TABLE_ID => Ok(Self::AtmoZones(AtmoZones::from_bytes(&data)?)),
            EFFECT_ZONES_TABLE_ID => Ok(Self::EffectZones(EffectZones::from_bytes(&data)?)),
            // LIGHTINGS_TABLE_ID =>  lighting
            BARRIERS_TABLE_ID => Ok(Self::Barriers(Barriers::from_bytes(&data)?, 0)),
            BARRIER_SETS_TABLE_ID => Ok(Self::BarrierSets(BarrierSets::from_bytes(&data)?, 0)),
            STARTING_POINTS_TABLE_ID => Ok(Self::StartingPoints(StartingPoints::from_bytes(&data)?, 0)),
            // 1027 startingPoint
            POWER_SLOTS_TABLE_ID => Ok(Self::PowerSlots(PowerSlots::from_bytes(&data)?, 0)),
            MONUMENTS_TABLE_ID => Ok(Self::Monuments(Monuments::from_bytes(&data)?, 0)),
            EFFECTS_TABLE_ID => Ok(Self::Effects(Effects::from_bytes(&data)?, 0)),
            TEAM_CONFIGURATIONS_TABLE_ID => Ok(Self::TeamConfigurations(team_config::TeamConfigurations::from_bytes(&data)?)),
            TEXTURE_LAYERS_TABLE_ID => Ok(Self::TextureLayers(TextureLayers::from_bytes(&data)?)),
            CLIFF_TABLE_ID => Ok(Self::Cliffs(Cliffs::from_bytes(&data)?)),
            F8003_TABLE_ID => Ok(Self::F8003(Data8003::from_bytes(&data)?)),
            PKIT_START_INFO_TABLE_ID => Ok(Self::PkitInfo(PkitInfo::from_bytes(&data)?)),
            CAMERA_POSITION_FOR_PREFABS_LOADING => Ok(Self::CameraPosition(CameraPosition::from_bytes(&data)?)),
            ENTITY_PREFABS => Ok(Self::Prefabs(Prefabs::from_bytes(&data)?)),
            _ => Err(MapError::NoMap)
        }
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        match self {
            MapTable::Unknown(data) => data.clone(),
            MapTable::ParsingFailed(data, _, _) => data.clone(),

            MapTable::Terrain(t) => t.to_bytes(),
            MapTable::TeamConfigurations(tc) => tc.to_bytes(),
            MapTable::TextureLayers(e) => e.to_bytes(),
            MapTable::Cliffs(e) => e.to_bytes(),
            MapTable::F8003(e) => e.to_bytes(),
            MapTable::PkitInfo(e) => e.to_bytes(),

            MapTable::AtmoZones(e) => e.to_bytes(),
            MapTable::EffectZones(e) => e.to_bytes(),
            MapTable::ScriptGroups(e, _) => e.to_bytes(),
            MapTable::Buildings(e, _) => e.to_bytes(),
            MapTable::Objects(e, _) => e.to_bytes(),
            MapTable::Squads(e, _) => e.to_bytes(),
            MapTable::Barriers(e, _) => e.to_bytes(),
            MapTable::BarrierSets(e, _) => e.to_bytes(),
            MapTable::StartingPoints(e, _) => e.to_bytes(),
            MapTable::Effects(e, _) => e.to_bytes(),
            MapTable::PowerSlots(e, _) => e.to_bytes(),
            MapTable::Monuments(e, _) => e.to_bytes(),
            MapTable::PlayerKits(e, _) => e.to_bytes(),

            MapTable::Prefabs(e) => e.to_bytes(),
            MapTable::CameraPosition(e) => e.to_bytes(),
        }
    }
    pub fn as_text(&self) -> Option<String> {
        match self {
            MapTable::Unknown(_) => None,
            MapTable::ParsingFailed(_, _, _) => None,
            MapTable::Terrain(_) => None,

            MapTable::TeamConfigurations(tc) => Some(serde_json::to_string_pretty(tc).unwrap()),
            MapTable::TextureLayers(tc) => Some(serde_json::to_string_pretty(tc).unwrap()),
            MapTable::Cliffs(tc) => Some(serde_json::to_string_pretty(tc).unwrap()),
            MapTable::F8003(tc) => Some(serde_json::to_string_pretty(tc).unwrap()),
            MapTable::PkitInfo(tc) => Some(serde_json::to_string_pretty(tc).unwrap()),

            MapTable::AtmoZones(e) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::EffectZones(e) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::ScriptGroups(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::Buildings(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::Objects(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::Squads(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::Barriers(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::BarrierSets(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::StartingPoints(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::Effects(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::PowerSlots(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::Monuments(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::PlayerKits(e, _) => Some(serde_json::to_string_pretty(e).unwrap()),

            MapTable::Prefabs(e) => Some(serde_json::to_string_pretty(e).unwrap()),
            MapTable::CameraPosition(e) => Some(serde_json::to_string_pretty(e).unwrap()),
        }
    }
    pub fn name(&self) -> Option<&'static str> {
        match self {
            MapTable::Unknown(_) => None,
            MapTable::ParsingFailed(_, _, _) => None,

            MapTable::TeamConfigurations(_) => Some("Team Configurations"),
            MapTable::Terrain(_) => Some("Terrain"),
            MapTable::TextureLayers(_) => Some("Texture Layers"),
            MapTable::Cliffs(_) => Some("Cliffs"),
            MapTable::F8003(_) => Some("8003 something"),
            MapTable::PkitInfo(_) => Some("PKit info"),

            MapTable::AtmoZones(_) => Some("AtmoZones"),
            MapTable::EffectZones(_) => Some("EffectZones"),
            MapTable::ScriptGroups(_, _) => Some("Script Groups"),
            MapTable::Buildings(_, _) => Some("Buildings"),
            MapTable::Objects(_, _) => Some("Objects"),
            MapTable::Squads(_, _) => Some("Squads"),
            MapTable::Barriers(_, _) => Some("Barriers"),
            MapTable::BarrierSets(_, _) => Some("Barrier Sets"),
            MapTable::StartingPoints(_, _) => Some("Starting Points"),
            MapTable::Effects(_, _) => Some("Effects"),
            MapTable::PowerSlots(_, _) => Some("Power Slots"),
            MapTable::Monuments(_, _) => Some("Monuments"),
            MapTable::PlayerKits(_, _) => Some("Player kits"),

            MapTable::Prefabs(_) => Some("Prefabs"),
            MapTable::CameraPosition(_) => Some("Camera position"),
        }
    }
    pub fn base_id(&self) -> usize {
        match self {
            MapTable::Unknown(_) => 0,
            MapTable::ParsingFailed(_, _, _) => 0,

            MapTable::TeamConfigurations(_) => 0,
            MapTable::Terrain(_) => 0,
            MapTable::TextureLayers(_) => 0,
            MapTable::Cliffs(_) => 0,
            MapTable::F8003(_) => 0,
            MapTable::PkitInfo(_) => 0,

            MapTable::AtmoZones(_) => 0,
            MapTable::EffectZones(_) => 0,
            MapTable::ScriptGroups(_, start) => *start,
            MapTable::Buildings(_, start) => *start,
            MapTable::Objects(_, start) => *start,
            MapTable::Squads(_, start) => *start,
            MapTable::Barriers(_, start) => *start,
            MapTable::BarrierSets(_, start) => *start,
            MapTable::StartingPoints(_, start) => *start,
            MapTable::Effects(_, start) => *start,
            MapTable::PowerSlots(_, start) => *start,
            MapTable::Monuments(_, start) => *start,
            MapTable::PlayerKits(_, start) => *start,

            MapTable::Prefabs(_) => 0,
            MapTable::CameraPosition(_) => 0,
        }
    }
}
impl Validate for MapTable {
    fn validate(&self, map: &Map) -> IssueCounter {
        match self {
            MapTable::Unknown(_) => IssueCounter::default(),
            MapTable::ParsingFailed(_, _, e) => IssueCounter::with_bug(e.clone()),

            MapTable::Terrain(t) => t.validate(map),
            MapTable::TeamConfigurations(tc) => tc.validate(map),
            MapTable::TextureLayers(e) => e.validate(map),
            MapTable::Cliffs(e) => e.validate(map),
            MapTable::F8003(e) => e.validate(map),
            MapTable::PkitInfo(e) => e.validate(map),

            MapTable::AtmoZones(e) => e.validate(map),
            MapTable::EffectZones(e) => e.validate(map),
            MapTable::ScriptGroups(e, _) => e.validate(map),
            MapTable::Buildings(e, _) => e.validate(map),
            MapTable::Objects(e, _) => e.validate(map),
            MapTable::Squads(e, _) => e.validate(map),
            MapTable::Barriers(e, _) => e.validate(map),
            MapTable::BarrierSets(e, _) => e.validate(map),
            MapTable::StartingPoints(e, _) => e.validate(map),
            MapTable::Effects(e, _) => e.validate(map),
            MapTable::PowerSlots(e, _) => e.validate(map),
            MapTable::Monuments(e, _) => e.validate(map),
            MapTable::PlayerKits(e, _) => e.validate(map),

            MapTable::Prefabs(e) => e.validate(map),
            MapTable::CameraPosition(e) => e.validate(map),
        }
    }
}

#[derive(Debug)]
pub enum SpecialScriptType {
    GlobalVars, ScriptList, ScriptGroups, SpawnGroups,
}
#[derive(Debug)]
pub enum MapError {
    NoMap,
    NotACommunityMapPrefix(String),
    MultipleMapFiles(String, String),
    DuplicateTag(String),
    DuplicateScript(SpecialScriptType),
    WrongScriptPath(String),
    MultipleNames(String, String),
    NonUtf8Content(String, std::str::Utf8Error),
    MapFileParsing(String),
}

macro_rules! show_it_mut {
    ($name:literal, $ctx:ident, $open: ident, $showable:ident) => {
        {
            egui::Window::new($name).open($open).show($ctx, |ui|{
                egui::ScrollArea::vertical().show(ui, |ui|{
                    $showable.show_mut(ui);
                });
            });
        }
    };
    ($name:literal, $ctx:ident, $open: ident, $showable:expr, $base_id:ident) => {
        {
            egui::Window::new($name).open($open).show($ctx, |ui|{
                // just to avoid unused warning
                let _ = $base_id;
                //use egui::Widget;
                //egui::DragValue::new($base_id).ui(ui);
                egui::ScrollArea::vertical().show(ui, |ui|{
                    for (_i, s) in $showable.iter_mut().enumerate() {
                        //ui.label(format!("{}", _i + *$base_id));
                        s.show_mut(ui);
                    }
                })
            });
        }
    };
}

impl MapTable {
    pub fn show_mut(&mut self, table_id: u32, open: &mut bool, _map: &Map, ctx: &egui::Context) {
        match self {
            MapTable::Unknown(unk) => {
                egui::Window::new(format!("{table_id}")).open(open).show(ctx, |ui|{
                    ui.colored_label(Color32::RED, "I have no idea what this table is, if you have an idea, please help");
                    if !unk.is_empty() {
                        egui::ScrollArea::vertical().show(ui, |ui|{
                            ui.collapsing(format!("show all {} bytes", unk.len()), |ui|{
                                ui.label(format!("{unk:02X?}"));
                            });
                            ui.collapsing(format!("show all {} bytes as text", unk.len()), |ui|{
                                ui.label(String::from_utf8_lossy(unk));
                            });
                        });
                    }
                });
            }
            MapTable::ParsingFailed(_, id, error) => {
                egui::Window::new(id.to_string()).open(open).show(ctx, |ui|{
                    ui.colored_label(ui.style().visuals.error_fg_color, error);
                });
            }
            MapTable::TeamConfigurations(cfg) =>
                show_it_mut!("Team Configurations", ctx, open, cfg),
            MapTable::Terrain(t) =>
                show_it_mut!("Terrain", ctx, open, t),
            MapTable::TextureLayers(e) =>
                show_it_mut!("Texture Layers", ctx, open, e),
            MapTable::Cliffs(e) =>
                show_it_mut!("Cliffs", ctx, open, e),
            MapTable::F8003(e) =>
                show_it_mut!("8003 something", ctx, open, e),
            MapTable::PkitInfo(e) =>
                show_it_mut!("Pkit info", ctx, open, e),
            MapTable::AtmoZones(e) =>
                show_it_mut!("AtmoZones", ctx, open, e),
            MapTable::EffectZones(e) =>
                show_it_mut!("EffectZones", ctx, open, e),
            MapTable::ScriptGroups(e, base_id) =>
                show_it_mut!("Script Groups", ctx, open, &mut e.0, base_id),
            MapTable::Buildings(e, base_id) =>
                show_it_mut!("Buildings", ctx, open, &mut e.0, base_id),
            MapTable::Objects(e, base_id) =>
                show_it_mut!("Objects", ctx, open, &mut e.0, base_id),
            MapTable::Squads(e, base_id) =>
                show_it_mut!("Squads", ctx, open, &mut e.0, base_id),
            MapTable::Barriers(e, base_id) =>
                show_it_mut!("Barriers", ctx, open, &mut e.0, base_id),
            MapTable::BarrierSets(e, base_id) =>
                show_it_mut!("Barrier sets", ctx, open, &mut e.0, base_id),
            MapTable::StartingPoints(e, base_id) =>
                show_it_mut!("Starting points", ctx, open, &mut e.0, base_id),
            MapTable::Effects(e, base_id) =>
                show_it_mut!("Effects", ctx, open, &mut e.0, base_id),
            MapTable::PowerSlots(e, base_id) =>
                show_it_mut!("Power slots", ctx, open, &mut e.0, base_id),
            MapTable::Monuments(e, base_id) =>
                show_it_mut!("Monuments", ctx, open, &mut e.0, base_id),
            MapTable::PlayerKits(e, base_id) =>
                show_it_mut!("Player kits", ctx, open, &mut e.0, base_id),

            MapTable::Prefabs(e) =>
                show_it_mut!("Prefabs", ctx, open, e),
            MapTable::CameraPosition(e) =>
                show_it_mut!("Camera Position", ctx, open, e),
        }
    }
}

impl Fix for MapTable {
    fn fix(&mut self, map: &Map) {
        match self {
            MapTable::Unknown(_) => {}
            MapTable::ParsingFailed(_, _, _) => {}

            MapTable::TeamConfigurations(tc) => tc.fix(map),
            MapTable::Terrain(t) => t.fix(map),
            MapTable::TextureLayers(e) => e.fix(map),
            MapTable::Cliffs(e) => e.fix(map),
            MapTable::F8003(e) => e.fix(map),
            MapTable::PkitInfo(e) => e.fix(map),

            MapTable::AtmoZones(e) => e.fix(map),
            MapTable::EffectZones(e) => e.fix(map),
            MapTable::ScriptGroups(e, _) => e.fix(map),
            MapTable::Buildings(e, _) => e.fix(map),
            MapTable::Objects(e, _) => e.fix(map),
            MapTable::Squads(s, _) => s.fix(map),
            MapTable::Barriers(e, _) => e.fix(map),
            MapTable::BarrierSets(e, _) => e.fix(map),
            MapTable::StartingPoints(e, _) => e.fix(map),
            MapTable::Effects(e, _) => e.fix(map),
            MapTable::PowerSlots(e, _) => e.fix(map),
            MapTable::Monuments(e, _) => e.fix(map),
            MapTable::PlayerKits(e, _) => e.fix(map),

            MapTable::Prefabs(e) => e.fix(map),
            MapTable::CameraPosition(e) => e.fix(map),
        }
    }
}

impl Validate for Script {
    fn validate(&self, _map: &Map) -> IssueCounter {
        let mut issues = IssueCounter::default();
        if self.folder == 0 || self.folder > 3 {
            issues.add_bug(format!("Script '{}' in folder {}", self.name, self.folder));
        }
        match &self.validation_result {
            ScriptValidationResult::GlobalVariables => {}
            ScriptValidationResult::ScriptList(Ok(_)) => {}
            ScriptValidationResult::ScriptGroups(Ok(_)) => {}
            ScriptValidationResult::ScriptList(Err(e)) |
            ScriptValidationResult::ScriptGroups(Err(e)) => {
                issues.add_bug(e.clone());
            }
            ScriptValidationResult::NotPartOfHierarchy => {
                issues.add_bug(format!("Script {} is not part of hierarchy", self.name));
            }
            ScriptValidationResult::Validated(Err(e), _) |
            ScriptValidationResult::Validated(Ok((_, Err(e))), _) => {
                issues.add_bug(format!("Script {} error: {e:?}", self.name));
            }
            ScriptValidationResult::SpawnGroupsValidated(Ok((_, Ok(_)))) => {}
            ScriptValidationResult::SpawnGroupsValidated(Err(e)) |
            ScriptValidationResult::SpawnGroupsValidated(Ok((_, Err(e)))) => {
                issues.add_bug(format!("Script {} error: {e:?}", self.name))
            }
            ScriptValidationResult::Validated(Ok((_, Ok(_))), _) => { }
            ScriptValidationResult::IncludedMultipleTimes(results) => {
                issues.add_warning("Script is included multiple times".to_string());
                for r in results {
                    match r {
                        Ok((_, Ok(_))) => {}
                        Ok((_, Err(e))) => {
                            issues.add_bug(format!("Script {} error: {e:?}", self.name))
                        }
                        Err(e) => {
                            issues.add_bug(format!("Script {} error: {e:?}", self.name))
                        }
                    }
                }
            }
        }
        issues
    }
}

impl Script {
    pub fn revalidate(&mut self, gv: Option<(&str, &str)>) {
        match self.validation_result {
            ScriptValidationResult::ScriptList(_) => {
                let r = script_validator::validate_script_list(&self.name, &self.content)
                    .map_err(|e| format!("{e:?}"));
                self.validation_result = ScriptValidationResult::ScriptList(r);
            }
            ScriptValidationResult::ScriptGroups(_) => {
                let r = script_validator::validate_script_groups(&self.name, &self.content)
                    .map_err(|e| format!("{e:?}"));
                self.validation_result = ScriptValidationResult::ScriptGroups(r);
            }
            ScriptValidationResult::Validated(_, et) => {
                let r = script_validator::validate(&self.name, &self.content, gv, et);
                self.validation_result = ScriptValidationResult::Validated(r, et);
            }
            ScriptValidationResult::SpawnGroupsValidated(_) => {
                let r = script_validator::validate_spawn_groups(&self.name, &self.content, gv);
                self.validation_result = ScriptValidationResult::SpawnGroupsValidated(r);
            }
            // caller's responsibility to revalidate
            ScriptValidationResult::GlobalVariables => { }
            ScriptValidationResult::NotPartOfHierarchy => { }
            ScriptValidationResult::IncludedMultipleTimes(_) => { }
        }
    }
}

#[derive(Default)]
pub struct IssueCounter {
    pub bugs: usize,
    pub warnings: usize,
    pub error_text: Vec<String>,
    pub warning_text: Vec<String>,
}

impl IssueCounter {
    pub fn add_bug(&mut self, explain: String) {
        self.bugs += 1;
        self.error_text.push(explain);
    }
    pub fn add_warning(&mut self, explain: String) {
        self.warnings += 1;
        self.warning_text.push(explain);
    }
    pub fn with_bug(explain: String) -> Self {
        let mut new = Self::default();
        new.add_bug(explain);
        new
    }
    pub fn with_warning(explain: String) -> Self {
        let mut new = Self::default();
        new.add_warning(explain);
        new
    }
}

impl Add for IssueCounter {
    type Output = Self;

    fn add(mut self, mut rhs: Self) -> Self::Output {
        self.error_text.append(&mut rhs.error_text);
        self.warning_text.append(&mut rhs.warning_text);
        Self {
            bugs: self.bugs + rhs.bugs,
            warnings: self.warnings + rhs.warnings,
            error_text: self.error_text,
            warning_text: self.warning_text,
        }
    }
}
impl AddAssign for IssueCounter {
    fn add_assign(&mut self, mut rhs: Self) {
        self.bugs += rhs.bugs;
        self.warnings += rhs.warnings;
        self.error_text.append(&mut rhs.error_text);
        self.warning_text.append(&mut rhs.warning_text);
    }
}
impl Sum for IssueCounter {
    fn sum<I: Iterator<Item = Self>>(iter: I) -> Self {
        iter.fold(IssueCounter::default(), |state, next| state + next)
    }
}

pub trait Validate {
    fn validate(&self, map: &Map) -> IssueCounter;
}
pub trait Fix {
    fn fix(&mut self, map: &Map);
}
