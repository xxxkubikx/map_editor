use std::cell::RefCell;
use std::collections::BTreeMap;
use std::ops::{Deref, DerefMut};
use crate::{TEAM_MANAGER_TABLE_ID, ATMO_ZONES_TABLE_ID, BARRIER_SETS_TABLE_ID, BARRIERS_TABLE_ID, BUILDINGS_TABLE_ID, EFFECT_ZONES_TABLE_ID, EFFECTS_TABLE_ID, MapError, MapTable, MONUMENTS_TABLE_ID, OBJECTS_TABLE_ID, OtherFile, PLAYER_KITS_TABLE_ID, POWER_SLOTS_TABLE_ID, SCRIPT, Script, SCRIPT_GROUPS_TABLE_ID, ScriptValidationResult, SpecialScriptType, SQUADS_TABLE_ID, STARTING_POINTS_TABLE_ID};
use local_encoding::Encoder;
use log::{error, info, trace, warn};
use script_validator::EntityType;
use crate::entity::{IdEntity, Param};

enum GenericFileType {
    Map,
    Lua,
    Other
}
fn generic_file_type(path: &str) -> GenericFileType {
    if path.ends_with(".map") || path.ends_with(".bec") {
        GenericFileType::Map
    } else if path.ends_with(".lua") {
        GenericFileType::Lua
    } else {
        GenericFileType::Other
    }

}

pub struct GenericFiles {
    pub map_name: String,
    pub map: Vec<u8>,
    pub scripts: Vec<Script>,
    pub other_files: Vec<OtherFile>,
}

pub fn group_generic_files(files: impl Iterator<Item=(String, Vec<u8>)>) -> Result<GenericFiles, MapError> {
    let mut map = None;
    let mut map_name = None;
    let mut scripts = vec![];
    let mut other_files = vec![];
    for (relative_path, data) in files {
        match generic_file_type(&relative_path) {
            GenericFileType::Map => {
                if map.is_some() {
                    return Err(MapError::MultipleMapFiles(map_name.unwrap(), relative_path));
                }
                map = Some(data);
                let name = relative_path[0..relative_path.len()-4].to_string();
                if map_name.is_none() {
                    map_name = Some(name);
                } else if Some(name) != map_name {
                    return Err(MapError::MultipleNames(map_name.unwrap(), relative_path));
                }
            }
            GenericFileType::Lua => {
                let prefix_pos = if let Some(p) = relative_path.find('\\') {
                    p + 1
                } else {
                    if let Some(p) = relative_path.find('/') {
                        p + 1
                    } else {
                        return Err(MapError::WrongScriptPath(relative_path));
                    }
                };
                let folder = relative_path[prefix_pos + SCRIPT.len() .. prefix_pos + SCRIPT.len()+1]
                    .parse()
                    .map_err(|_| MapError::WrongScriptPath(relative_path.to_string()))?;
                let script = match std::str::from_utf8(&data) {
                    Ok(content) => {
                        let name = relative_path[prefix_pos + SCRIPT.len()+2..].to_string();
                        Script{ name, folder, content: content.to_string(), validation_result: ScriptValidationResult::NotPartOfHierarchy }
                    }
                    Err(e) => {
                        if let Ok(content) = local_encoding::Encoding::ANSI.to_string(&data) {
                            let name = relative_path[prefix_pos + SCRIPT.len()+2..].to_string();
                            Script{ name, folder, content, validation_result: ScriptValidationResult::NotPartOfHierarchy }
                        } else {
                            return Err(MapError::NonUtf8Content(relative_path.clone(), e));
                        }
                    }
                };
                scripts.push(script)
            }
            GenericFileType::Other => {
                let prefix_pos = if let Some(p) = relative_path.find('\\') {
                    p + 1
                } else {
                    if let Some(p) = relative_path.find('/') {
                        p + 1
                    } else {
                        return Err(MapError::WrongScriptPath(relative_path.clone()));
                    }
                };
                other_files.push(OtherFile{
                    relative_path: relative_path[prefix_pos..].to_string(),
                    content: data,
                })
            }
        }
    }

    let map = map.ok_or(MapError::NoMap)?;
    Ok(GenericFiles{
        // unwrap is safe, if we have a map file, we must have name too
        map_name: map_name.unwrap(),
        map,
        scripts,
        other_files,
    })
}

pub fn extract_map(map: &[u8]) -> Result<BTreeMap<u32, (RefCell<MapTable>, Vec<u8>, u16, u16)>, MapError> {
    info!("opening map file");
    let mut bytes = sr_libs::cff::check_signature(map).unwrap();
    let mut map = BTreeMap::new();
    let cff_header_size = 16;
    while bytes.len() >= cff_header_size {
        let (input, table) = sr_libs::cff::rw_utils::parse_abstract_cff_table(bytes)
            .map_err(|e| MapError::MapFileParsing(format!("{e:?}")))?;
        let mut decompressor = flate2::read::ZlibDecoder::new(table.data);
        let mut data = Vec::new();
        use std::io::Read;
        let e = decompressor.read_to_end(&mut data);
        if e.is_err() {
            return Err(MapError::MapFileParsing(format!("{:?}", e.unwrap_err())));
        }
        if table.decompressed_size != data.len() as u32 {
            return Err(MapError::MapFileParsing(format!("Expected size: {}, real size: {}, for table: {}", table.decompressed_size, data.len(), table.id)));
        }
        let original = data.clone();
        map.insert(table.id, (RefCell::new(MapTable::from_data(table.id, data)), original, table.unknown1, table.unknown2));
        bytes = input;
    }
    trace!("Found {} tables", map.len());

    Ok(map)
}

pub fn extract_entity_tags(map: &BTreeMap<u32, (RefCell<MapTable>, Vec<u8>, u16, u16)>)
    -> Result<BTreeMap<String, (u32, EntityType)>, MapError>
{

    trace!("extracting entity tags");
    let mut entity_tags = BTreeMap::new();
    for (_, (t, _, _, _)) in map.iter() {

        match t.borrow().deref() {
            MapTable::ScriptGroups(e, _) => foreach(e.0.iter().map(|e| &e.b), EntityType::ScriptGroup, &mut entity_tags)?,
            MapTable::Objects(e, _) => foreach(e.0.iter().map(|e| &e.b.b), EntityType::Object, &mut entity_tags)?,
            MapTable::Buildings(e, _) => foreach(e.0.iter().map(|e| &e.b.b.b), EntityType::Building, &mut entity_tags)?,
            MapTable::Squads(e, _) => foreach(e.0.iter().map(|e| &e.b.b), EntityType::Squad, &mut entity_tags)?,
            MapTable::Barriers(e, _) => foreach(e.0.iter().map(|e| &e.b.b.b), EntityType::BarrierModule, &mut entity_tags)?,
            MapTable::BarrierSets(e, _) => foreach(e.0.iter().map(|e| &e.b.b.b), EntityType::BarrierSet, &mut entity_tags)?,
            MapTable::StartingPoints(e, _) => foreach(e.0.iter().map(|e| &e.b.b.b), EntityType::StartingPoint, &mut entity_tags)?,
            MapTable::Effects(e, _) => foreach(e.0.iter().map(|e| &e.b.b.b), EntityType::StartingPoint, &mut entity_tags)?,
            MapTable::PowerSlots(e, _) => foreach(e.0.iter().map(|e| &e.b.b.b), EntityType::PowerSlot, &mut entity_tags)?,
            MapTable::Monuments(e, _) => foreach(e.0.iter().map(|e| &e.b.b.b), EntityType::TokenSlot, &mut entity_tags)?,
            MapTable::PlayerKits(_, _) => {},
            MapTable::AtmoZones(_) |
            MapTable::EffectZones(_) |
            MapTable::Terrain(_) |
            MapTable::TextureLayers(_) |
            MapTable::Cliffs(_) |
            MapTable::F8003(_) |
            MapTable::PkitInfo(_) |
            MapTable::Unknown(_) |
            MapTable::ParsingFailed(_, _, _) |
            MapTable::TeamConfigurations(_) => {},
            MapTable::Prefabs(_) => {}
            MapTable::CameraPosition(_) => {}
        }
    }
    trace!("extracted {} entity tags", entity_tags.len());
    Ok(entity_tags)
}
fn foreach<'a, I: Iterator<Item = &'a IdEntity>>(entities: I, et: EntityType, entity_tags: &mut BTreeMap<String, (u32, EntityType)>)
                                                 -> Result<(), MapError>
{
    for entity in entities {
        let mut tag = None;
        let mut id = None;
        for param in entity.params.iter() {
            match param {
                Param::Tag(t) => { tag = Some(t.to_ascii_lowercase()) }
                Param::Id(i) => { id = Some(*i) }
                _ => {}
            }
        }
        if let Some(tag) = tag {
            match id {
                None => {warn!("Entity {tag} does not hane an ID");}
                Some(id) => {
                    if entity_tags.contains_key(&tag) {
                        return Err(MapError::DuplicateTag(tag))
                    } else {
                        entity_tags.insert(tag, (id, et));
                    }
                }
            }
        }
    }
    Ok(())
}

pub struct AllScripts {
    pub scripts: Vec<Script>,
    pub global_variables: Option<Script>,
    pub script_list: Option<Script>,
    pub script_groups: Option<Script>,
    pub spawn_groups: Option<Script>,
}

fn prevalidate_scripts(all_scripts: Vec<Script>, map_name: &str) -> Result<AllScripts, MapError> {
    let mut scripts: Vec<Script> = Vec::with_capacity(all_scripts.len());
    let mut global_variables = None;
    let mut script_list : Option<Script> = None;
    let mut script_groups : Option<Script> = None;
    let mut spawn_groups : Option<Script> = None;

    trace!("validating scripts");
    let global_vars_alternative_name = format!("_{map_name}_GlobalVars.lua").to_ascii_lowercase();
    let script_list_alternative_name = format!("_{map_name}_scriptList.lua").to_ascii_lowercase();
    let script_groups_alternative_name = format!("_{map_name}_scriptgroups.lua").to_ascii_lowercase();
    let spawn_groups_alternative_name = format!("_{map_name}_spawngroups.lua").to_ascii_lowercase();
    for script in all_scripts {
        if script.name.eq_ignore_ascii_case("_GlobalVars.lua")
            || script.name.eq_ignore_ascii_case(&global_vars_alternative_name) {
            if global_variables.is_some() {
                return Err(MapError::DuplicateScript(SpecialScriptType::GlobalVars))
            }
            global_variables = Some(Script{ validation_result: ScriptValidationResult::GlobalVariables, .. script });
        } else if script.name.eq_ignore_ascii_case("_scriptList.lua")
            || script.name.eq_ignore_ascii_case(&script_list_alternative_name) {
            if script_list.is_some() {
                return Err(MapError::DuplicateScript(SpecialScriptType::ScriptList))
            }
            let r = script_validator::validate_script_list(&script.name, &script.content)
                .map_err(|e| format!("{e:?}"));
            script_list = Some(Script{ validation_result: ScriptValidationResult::ScriptList(r), .. script });
        } else if script.name.eq_ignore_ascii_case("_scriptgroups.lua")
            || script.name.eq_ignore_ascii_case(&script_groups_alternative_name) {
            if script_groups.is_some() {
                return Err(MapError::DuplicateScript(SpecialScriptType::ScriptGroups))
            }
            let r = script_validator::validate_script_groups(&script.name, &script.content)
                .map_err(|e| format!("{e:?}"));
            script_groups = Some(Script{ validation_result: ScriptValidationResult::ScriptGroups(r), .. script });
        } else if script.name.eq_ignore_ascii_case("_spawngroups.lua")
            || script.name.eq_ignore_ascii_case(&spawn_groups_alternative_name) {
            if spawn_groups.is_some() {
                return Err(MapError::DuplicateScript(SpecialScriptType::SpawnGroups))
            }
            spawn_groups = Some(script);
        } else {
            scripts.push(script)
        }
    }
    Ok(AllScripts{
        scripts,
        global_variables,
        script_list,
        script_groups,
        spawn_groups,
    })
}

pub fn validate_scripts(all_scripts: Vec<Script>, map_name: &str, entity_tags: &BTreeMap<String, (u32, EntityType)>)
    -> Result<AllScripts, MapError>
{
    let AllScripts {
        mut scripts, global_variables, mut script_list, script_groups, spawn_groups
    } = prevalidate_scripts(all_scripts, &map_name)?;

    let gv = global_variables.as_ref().map(|s| (s.name.as_str(), s.content.as_str()));
    for script in &mut scripts {
        if let Some(et) = entity_tags.get(&script.name[0 .. script.name.len() - 4].to_ascii_lowercase()).map(|(_, et)| *et) {
            trace!("validate: {}", script.name);
            let validation_result = script_validator::validate(&script.name, &script.content, gv, et);
            trace!("script {} for entity type {:?} with result: {}", script.name, et, script_validator::summary(&validation_result));
            script.validation_result.add(ScriptValidationResult::Validated(validation_result, et));
        } else if script.name.eq_ignore_ascii_case("_main.lua")
            || script.name.eq_ignore_ascii_case(&format!("_{map_name}.lua")) {
            trace!("validate: {}", script.name);
            let validation_result = script_validator::validate(&script.name, &script.content, gv, EntityType::World);
            trace!("{} with result: {}", script.name, script_validator::summary(&validation_result));
            script.validation_result.add(ScriptValidationResult::Validated(validation_result, EntityType::World));
        } else if (&script.name[0 .. script.name.len() - 4]).eq_ignore_ascii_case(&map_name) {
            trace!("validate: {}", script.name);
            let validation_result = script_validator::validate(&script.name, &script.content, gv, EntityType::World);
            trace!("{} with result: {}", script.name, script_validator::summary(&validation_result));
            script.validation_result.add(ScriptValidationResult::Validated(validation_result, EntityType::World));
        } else if let Some(script_groups) = &script_groups {
            match &script_groups.validation_result {
                ScriptValidationResult::ScriptGroups(Ok(script_groups)) => {
                    if let Some((_, _group)) = script_groups.iter().find(|(key, _)| (&script.name[0 .. script.name.len() - 4]).eq_ignore_ascii_case(key)) {
                        trace!("validate: {}", script.name);
                        let validation_result = script_validator::validate(&script.name, &script.content, gv, EntityType::World);
                        trace!("{} with result: {}", script.name, script_validator::summary(&validation_result));
                        script.validation_result.add(ScriptValidationResult::Validated(validation_result, EntityType::World));
                    }
                }
                _ => {}
            }
        }
    }
    if let Some(sl) = &mut script_list {
        if let ScriptValidationResult::ScriptList(Ok(list)) = &sl.validation_result {
            for script in &mut scripts {
                if list.iter().any(|le| script.name.eq_ignore_ascii_case(le)) {
                    trace!("validate: {}", script.name);
                    let validation_result = script_validator::validate(&script.name, &script.content, gv, EntityType::World);
                    trace!("(script list) {} with result: {}", script.name, script_validator::summary(&validation_result));
                    script.validation_result.add(ScriptValidationResult::Validated(validation_result, EntityType::World));
                } else {
                    // TODO add error (to UI) if script does not exist
                    warn!("(script list) {} does not exist", script.name);
                }
            }
        }
    }
    let spawn_groups = spawn_groups.map(|spawn_groups|{
        trace!("validate: {}", spawn_groups.name);
        let r = script_validator::validate_spawn_groups(&spawn_groups.name, &spawn_groups.content, gv);
        if let Ok((_, Ok(list))) = &r {
            for script in &mut scripts {
                if list.iter().any(|le| script.name[0 .. script.name.len()-4].eq_ignore_ascii_case(le)) {
                    trace!("validate: {}", script.name);
                    let validation_result = script_validator::validate_spawn_group(&spawn_groups.content, &script.name, &script.content, gv, EntityType::World);
                    trace!("(spawn group) {} with result: {}", script.name, script_validator::summary(&validation_result));
                    script.validation_result.add(ScriptValidationResult::Validated(validation_result, EntityType::World));
                } else {
                    // TODO add error (to UI) if script does not exist
                    warn!("(spawn group) {} does not exist", script.name);
                }
            }
        }
        Script{ validation_result: ScriptValidationResult::SpawnGroupsValidated(r), .. spawn_groups }
    });
    Ok(AllScripts{
        scripts,
        global_variables,
        script_list,
        script_groups,
        spawn_groups,
    })
}

/// Does not work correctly yet
#[allow(dead_code)]
pub fn update_base_ids(map: &BTreeMap<u32, (RefCell<MapTable>, Vec<u8>, u16, u16)>) {
    trace!("precompute entity ids");
    fn update_base_id(base_id: &mut usize, table: &u32, map: &BTreeMap<u32, (RefCell<MapTable>, Vec<u8>, u16, u16)>) {
        macro_rules! update_base_id {
                ($e:ident, $start:ident, $base_id:ident) => {
                    {
                        *$start = *$base_id;
                        *$base_id += $e.0.len();
                    }
                };
            }
        if let Some((t, _, _, _)) = map.get(table) {
            match t.borrow_mut().deref_mut() {
                MapTable::Objects(e, start) => update_base_id!(e, start, base_id),
                MapTable::Buildings(e, start) => update_base_id!(e, start, base_id),
                MapTable::Squads(e, start) => update_base_id!(e, start, base_id),
                MapTable::Barriers(e, start) => update_base_id!(e, start, base_id),
                MapTable::BarrierSets(e, start) => update_base_id!(e, start, base_id),
                MapTable::PowerSlots(e, start) => update_base_id!(e, start, base_id),
                MapTable::Monuments(e, start) => update_base_id!(e, start, base_id),
                MapTable::PlayerKits(e, start) => update_base_id!(e, start, base_id),
                MapTable::StartingPoints(e, start) => update_base_id!(e, start, base_id),
                MapTable::Effects(e, start) => update_base_id!(e, start, base_id),
                MapTable::ScriptGroups(e, start) => update_base_id!(e, start, base_id),
                MapTable::AtmoZones(_) => {
                    *base_id += 1;
                }
                MapTable::Terrain(_) |
                MapTable::Unknown(_) |
                MapTable::ParsingFailed(_, _, _) |
                MapTable::EffectZones(_) |
                MapTable::TeamConfigurations(_) => {
                    error!("IDs are most likely incorrect! table: {table} base_id: {base_id}");
                    if 1014 == *table || 1011 == *table {
                        *base_id += 1;
                    }
                },
                _ => unimplemented!("{table}")
            }
        }
    }

    let mut base_id = 1;
    const TABLE_LOAD_ORDER : &[u32] = &[
        TEAM_MANAGER_TABLE_ID,
        ATMO_ZONES_TABLE_ID,
        EFFECT_ZONES_TABLE_ID,
        1013,
        SQUADS_TABLE_ID,
        OBJECTS_TABLE_ID,
        BARRIER_SETS_TABLE_ID,
        BARRIERS_TABLE_ID,
        BUILDINGS_TABLE_ID,
        EFFECTS_TABLE_ID,
        1007,
        PLAYER_KITS_TABLE_ID,
        1009,
        SCRIPT_GROUPS_TABLE_ID,
        1024,
        1002,
        STARTING_POINTS_TABLE_ID,
        1028,
        POWER_SLOTS_TABLE_ID,
        MONUMENTS_TABLE_ID,
        1033,
        1034,
        1032,
        1017,
    ];
    for table in TABLE_LOAD_ORDER {
        update_base_id(&mut base_id, table, map);
    }
}// */
